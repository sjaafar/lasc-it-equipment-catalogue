﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LASC_ShoppingCart2
{
    public partial class checkout : Base
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["CustomLogin"] == null)
            {
                if (Session["UserLoggedIn"] != null)
                {
                    if (Convert.ToBoolean(Session["UserLoggedIn"]) != true)
                        Response.Redirect("~/Login.aspx");
                }
                else
                    Response.Redirect("~/Login.aspx");

            }
            //tax
            TaxRate.Text = String.Concat("<input id='SalesTax' type='hidden' name='amount' value='", GetSalesTax(), "'>");
            lbDate.Text = DateTime.Now.Date.ToShortDateString();
            //Start Cost Center
            try
            {
                // johnny 7/26/2017
                int userid = 0;
                if (Session["UserID"] != null)
                    userid = Convert.ToInt32(Session["UserId"]);
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
            string SQL = "select ss.IssueeID, ss.IssueeCode, ss.IssueeName from issuees ss,WebReportingUsersIssueeFilter  where is_UnitCode = 0 and IssueeTypeID > 1 and WebReportingUsersIssueeFilter.userid=" + userid + " and  Deleted = 0 and len(IssueeCode) = 6 and WebReportingUsersIssueeFilter.issueeid=ss.issueeid ORDER BY IssueeName ";
            SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
            DataTable dtOutput = new DataTable();
            da.Fill(dtOutput);
            rpCostCenter.DataSource = dtOutput;
            rpCostCenter.DataBind();
             }
            catch (Exception ex)
            {

            }
            //End Cost Center

            //Start Finction Area 
            try
            {
                //int UserId =(int)Session["UserId"];
               // Response.Write("Send to debug output " + UserId);
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                string SQL = "SELECT FUNCTIONAL_AREA, [DESCRIPTION] FROM TBL_FUNCTIONAL_AREA ORDER BY DESCRIPTION";
                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dtOutput = new DataTable();
                da.Fill(dtOutput);
                rpFunctional_Area.DataSource = dtOutput;
                rpFunctional_Area.DataBind();
            }
            catch (Exception ex)
            {

            }
            //End Finction Area 



            //Start Unit Code
            //try
            //{
            //    SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
            //    string SQL = "select IssueeID, IssueeCode, IssueeName from issuees where is_UnitCode = 1 and IssueeTypeID > 1 and Deleted = 0  ORDER BY IssueeName ";
            //    SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
            //    DataTable dtOutput = new DataTable();
            //    da.Fill(dtOutput);
            //    rpUniteCode.DataSource = dtOutput;
            //    rpUniteCode.DataBind();
            //}
            //catch (Exception ex)
            //{

            //}
            //End Unit Code

            // Start IssueOrdersUDText7 lable
            //try
            //{
            //    SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
            //    string SQL = "select ObjectLabel from Configurations where ObjectName = 'IssueOrdersUDText7' ";
            //    SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
            //    DataTable dtOutput = new DataTable();
            //    da.Fill(dtOutput);

            //    IssueOrdersUDText7.Text = (string)dtOutput.Rows[0]["ObjectLabel"]; 
            //}
            //catch (Exception ex)
            //{

            //}
           
            // End IssueOrdersUDText7 lable


            // Start get persons info 
            //try
            //{
            //    SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
            //    string SQL = "select ObjectLabel from Configurations where ObjectName = 'IssueOrdersUDText7' ";
            //    SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
            //    DataTable dtOutput = new DataTable();
            //    da.Fill(dtOutput);

            //    IssueOrdersUDText7.Text = (string)dtOutput.Rows[0]["ObjectLabel"]; 
            //}
            //catch (Exception ex)
            //{

            //}

            // End  get persons info 


        }


        protected Decimal GetSalesTax()
        {
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);

            string SQL = "Select StringValue From PassportMiscValues Where ObjectName = 'TAXPercent'";
            SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
            DataTable dtOutput = new DataTable();
            da.Fill(dtOutput);

            Decimal taxRate;

            try
            {
                taxRate = Convert.ToDecimal(dtOutput.Rows[0][0].ToString()) / 100;
            }
            catch
            {
                taxRate = 0;
            }
            return taxRate;
        }
    }
}