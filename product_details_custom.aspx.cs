﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LASC_ShoppingCart2
{
    public partial class product_details_custom : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["CustomLogin"] == null)
            {
                if (Session["UserLoggedIn"] != null)
                {
                    if (Convert.ToBoolean(Session["UserLoggedIn"]) != true)
                        Response.Redirect("~/Login.aspx");
                }
                else
                    Response.Redirect("~/Login.aspx");
            }



            //Start Path 
            try
            {
                string ModelID = Request.QueryString["id"];
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                string SQL = " ";

                SQL = SQL + " select M.ModelName, FURNITURE_TYPE, TF.FURNITURE_TYPE_ID  ,C.CategoryID , C.CategoryCode from Models M ";
                SQL = SQL + " join Categories C on C.CategoryID=M.CategoryID ";
                SQL = SQL + " join TBL_MODEL_EXTENDED TM on TM.Model_ID = M.ModelID  ";
                SQL = SQL + "  join TBL_FURNITURE_TYPE TF on TM.FURNITURE_TYPE_ID = TF.FURNITURE_TYPE_ID  ";
                SQL = SQL + "  where  M.ModelID =" + ModelID;

                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dtOutput = new DataTable();
                da.Fill(dtOutput);

                StringBuilder sb = new StringBuilder();

                page_pathserver.InnerHtml = "<a href=\"default.aspx\">Home</a> &#8674; <a href=\"subcategories.aspx?id=" + dtOutput.Rows[0]["CategoryID"] + "\" >" + dtOutput.Rows[0]["CategoryCode"] + "</a> &#8674; <a href=\"categories.aspx?id=" + dtOutput.Rows[0]["CategoryID"] + "&fid=" + dtOutput.Rows[0]["FURNITURE_TYPE_ID"] + " \" >" + dtOutput.Rows[0]["FURNITURE_TYPE"] + "</a>  &#8674; " + dtOutput.Rows[0]["ModelName"];
            }
            catch (Exception ex)
            {

            }
            //End Path

           

        }
    }
}