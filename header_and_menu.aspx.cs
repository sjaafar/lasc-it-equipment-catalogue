﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LASC_ShoppingCart2
{
    public partial class header_and_menu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             

            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
            string SQL = "select CategoryID, CategoryCode from Categories where Category='asset'";
            SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
            DataTable dtOutput = new DataTable();
            da.Fill(dtOutput);

            StringBuilder sb = new StringBuilder();
            foreach (DataRow drOutput in dtOutput.Rows)
            {
                sb.Append("<li><a href=\"#\" > " + UppercaseFirst(drOutput[1].ToString()) + " </a>");
            }

            menu_item.InnerHtml = sb.ToString();

        }


        //Transfer first case to upper
        static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            string lower =  s.ToLower();
            // Return char and concat substring.
            return char.ToUpper(lower[0]) + lower.Substring(1);
        }
    }

}

