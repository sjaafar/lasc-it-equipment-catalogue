//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Services;
using System.Data.Services.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Transactions;
using System.Web;
using System.Web.Script.Serialization;


namespace LASC_ShoppingCart2
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WcfDataService1
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
            // config.SetEntitySetAccessRule("MyEntityset", EntitySetRights.AllRead);
            // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V2;
        }

        public string Email_to = "resources@lacourt.org";
        //public string Email_to = "tadra@asapsystems.com";
        //Search field
        #region SearchProduct
        [OperationContract]
        [
        WebInvoke
        (
        Method = "GET",
        UriTemplate = "SearchProduct?Input={Input}",
        BodyStyle = WebMessageBodyStyle.Bare,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )
        ]
        public PageProductInfo SearchProduct(string Input)
        {
            #region Declaration And Initialization Section.
            //get info from ajax call
            PageInfo oPageInfo = new PageInfo();

            //return values
            PageProductInfo Result = new PageProductInfo();
            List<product> oReturnValue = new List<product>();

            JavaScriptSerializer jscript = new JavaScriptSerializer();
            #endregion

            try
            {
                oPageInfo = jscript.Deserialize<PageInfo>(Input);



                int index = (int)oPageInfo.IndexPage;
                int fromIndex = (index * 25) + 1;
                int toIndex = fromIndex + 24;

                string SearchInput = (string)oPageInfo.search_input;
                string SearchQuery = " ";
                string[] words = SearchInput.Split(' ');
                int ij = 1;
                foreach (string word in words)
                {
                    SearchQuery = SearchQuery + "(FT.FURNITURE_TYPE Like '%" + word + "%' or M.UDTextModel1 Like '%" + word + "%' or M.ModelCode Like '%" + word + "%' or  C.ConditionCode Like '%" + word + "%' )";

                    if (ij < words.Length)
                    {
                        SearchQuery = SearchQuery + " AND ";
                    }
                    ij++;
                }
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                //get Images From Database 

                //string SQL = "select ModelID, ModelCode, ModelName from Models where ItemTypeID=5  and ModelName Like '%'+'" + SearchInput + "'+'%' ";
                string SQL = "SELECT * FROM ( SELECT M.ModelIDAbstract, M.ModelID, M.ModelCode, TC.COLOR, I.ItemsUDValue6, I.ItemsUDValue5,"
                    + " TME.MATERIALS, M.ModelName,M.CostMostRecent, C.ConditionCode,AT.AssetTypeCode, CA.CategoryCode,"
                    + " M.UDTextModel1 as Model, I.SerialNumber, FT.FURNITURE_TYPE ,ROW_NUMBER() "
                    + " OVER (ORDER BY M.ModelIDAbstract ASC) As x FROM Models M "
                    + " JOIN Items I on I.ModelID = M.ModelID "
                    + " LEFT JOIN AssetTypes AT on AT.AssetTypeID = M.AssetTypeID"
                    + " LEFT JOIN Categories CA on CA.CategoryID = M.CategoryID"
                    + " LEFT JOIN Conditions C ON C.ConditionID = I.ConditionID "
                    + " LEFT JOIN TBL_MODEL_EXTENDED TME ON TME.Model_ID = M.ModelIDAbstract"
                    + " Left JOIN TBL_MATCHED_MODEL TMM on TMM.MAIN_MODEL_ID = M.ModelID "
                    + " LEFT JOIN TBL_COLOR TC ON TC.COLOR_ID = TME.COLOR_ID"
                    + " LEFT JOIN TBL_FURNITURE_TYPE FT ON FT.FURNITURE_TYPE_ID = TME.FURNITURE_TYPE_ID"
                    + " LEFT JOIN Models AM ON AM.ModelID = M.ModelIDAbstract "
                    + " WHERE M.Deleted=0  and I.Deleted= 0 and M.ItemTypeID = 1  and M.AssetTypeID <> 7 and M.ModelsUDValueCode2 = 'SurPlus' and ( " + SearchQuery + " )) AS Data WHERE x BETWEEN " + fromIndex + " AND " + (toIndex);

                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                int nbr_line_query = (int)dt.Rows.Count;

                SQL = "select   count(M.ModelID) FROM Models M " +
                    " JOIN Items I on I.ModelID = M.ModelID  " +
                    " LEFT JOIN Conditions C ON C.ConditionID = I.ConditionID " +
                    " LEFT JOIN TBL_MODEL_EXTENDED TME ON TME.Model_ID = M.ModelIDAbstract " +
                    " Left JOIN TBL_MATCHED_MODEL TMM on TMM.MAIN_MODEL_ID = M.ModelID " +
                    " LEFT JOIN TBL_COLOR TC ON TC.COLOR_ID = TME.COLOR_ID " +
                    " LEFT JOIN TBL_FURNITURE_TYPE FT ON FT.FURNITURE_TYPE_ID = TME.FURNITURE_TYPE_ID" +
                    " WHERE M.Deleted=0 and I.Deleted= 0 and M.ItemTypeID = 1  and M.AssetTypeID <> 7 and M.ModelsUDValueCode2 = 'SurPlus' and ( " + SearchQuery + " ) ";



                SqlDataAdapter da2 = new SqlDataAdapter(SQL, sqlConn);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);


                int nbr_row_total = (int)dt2.Rows[0][0];

                for (int i = 0; i < 25; i++)
                {
                    if (nbr_row_total == i)
                        break;

                    if (nbr_line_query == i)
                        break;

                    product p = new product();
                    if (dt.Rows[i]["ModelID"] != null)
                        p.id = (int)dt.Rows[i]["ModelID"];
                    else break;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["ModelCode"].ToString()))
                        p.ModelCode = (string)dt.Rows[i]["ModelCode"];
                    else p.ModelCode = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["AssetTypeCode"].ToString()))
                        p.assetType = (string)dt.Rows[i]["AssetTypeCode"];
                    else p.assetType = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["CategoryCode"].ToString()))
                        p.CategoryCode = (string)dt.Rows[i]["CategoryCode"];
                    else p.CategoryCode = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["FURNITURE_TYPE"].ToString()))
                        p.FURNITURE_TYPE = (string)dt.Rows[i]["FURNITURE_TYPE"];
                    else p.FURNITURE_TYPE = string.Empty;
                    if (!string.IsNullOrEmpty(dt.Rows[i]["Model"].ToString()))
                        p.Model = (string)dt.Rows[i]["Model"];
                    else p.Model = string.Empty;
                    if (!string.IsNullOrEmpty(dt.Rows[i]["SerialNumber"].ToString()))
                        p.SerialNum = (string)dt.Rows[i]["SerialNumber"];
                    else p.SerialNum = string.Empty;
                    if (!string.IsNullOrEmpty(dt.Rows[i]["ConditionCode"].ToString()))
                        p.ConditionCode = (string)dt.Rows[i]["ConditionCode"];
                    else p.ConditionCode = string.Empty;





                    if (dt.Rows[i]["ModelIDAbstract"] != null)
                        p.ModelIDAbstract = (int)dt.Rows[i]["ModelIDAbstract"];
                    else
                        p.ModelIDAbstract = 0;



                    if (!string.IsNullOrEmpty(dt.Rows[i]["ItemsUDValue6"].ToString()))
                        p.COLOR = dt.Rows[i]["ItemsUDValue6"].ToString();
                    else p.COLOR = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["ItemsUDValue5"].ToString()))
                        p.MATERIALS = dt.Rows[i]["ItemsUDValue5"].ToString();
                    else p.MATERIALS = string.Empty;



                    //get image from Image Table 
                    string SQL3 = "select (select TOP 1 ModelImage from Images where MOdelID = " + p.id + ") as ModelLImage"
                            + ", (select TOP 1 ModelImage from Images where MOdelID = " + p.ModelIDAbstract + ") as abstModelImage";



                    SqlDataAdapter reponse3 = new SqlDataAdapter(SQL3, sqlConn);
                    DataTable ImagesForProduct = new DataTable();
                    reponse3.Fill(ImagesForProduct);
                    //if (ImagesForProduct.Rows.Count == 0)
                    //{
                    //    SQL3 = "select Top 1 ModelImage from Images where ModelID=" + p.ModelIDAbstract;
                    //    reponse3 = new SqlDataAdapter(SQL3, sqlConn);
                    //    reponse3.Fill(ImagesForProduct);

                    //}

                    int nbr_rows = ImagesForProduct.Rows.Count;

                    if (nbr_rows != 0)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(ImagesForProduct.Rows[0][0].ToString()))
                                p.ProductImage = Convert.ToBase64String((byte[])ImagesForProduct.Rows[0][0]);
                            else if (!string.IsNullOrEmpty(ImagesForProduct.Rows[0][1].ToString()))
                                p.ProductImage = Convert.ToBase64String((byte[])ImagesForProduct.Rows[0][1]);
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    else
                        p.ProductImage = "none";


                    //get Quantity
                    /*
                    string SQL4 = "select count(M.ModelID) FROM Models M  where  ModelsUDValueCode2 like 'Surplus' and M.deleted = 0  AND  ItemTypeId=1 and M.ModelID =" + p.id;
                    SqlDataAdapter reponse4 = new SqlDataAdapter(SQL4, sqlConn);
                    DataTable qdt = new DataTable();
                    reponse4.Fill(qdt);
                    */
                    p.Quantity = 1;
                    oReturnValue.Add(p);

                }

                Result.ListProduct = oReturnValue;
                Result.nbrPages = nbr_row_total / 25;
            }

            catch (Exception ex)
            {
                string m = ex.Message;
            }
            return Result;
        }


        #endregion
        //when you select item get info needed about this item and save it in local storage
        #region GetOneProductInfo
        [OperationContract]
        [
        WebInvoke
        (
        Method = "GET",
        UriTemplate = "GetOneProductInfo?Input={Input}",
        BodyStyle = WebMessageBodyStyle.Bare,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )
        ]
        public product GetOneProductInfo(string Input)
        {
            product p = new product();

            try
            {
                string item_id = Input;
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                //string SQL = "SELECT ModelIDAbstract, M.ModelID, ModelCode, TC.COLOR,I.ItemsUDValue6, I.ItemsUDValue5, TME.MATERIALS, ModelName, ModelImage, " +
                //            "CostMostRecent, C.ConditionCode "+
                //            "FROM Models M "+
                //            "JOIN Items I on I.ModelID = M.ModelID "+
                //            "LEFT JOIN Conditions C ON C.ConditionID = I.ConditionID "+
                //            "LEFT JOIN TBL_MODEL_EXTENDED TME ON TME.Model_ID = M.ModelIDAbstract "+
                //            "Left JOIN TBL_MATCHED_MODEL TMM on TMM.MAIN_MODEL_ID = M.ModelID "+
                //            "LEFT JOIN (select Top(1) ModelImage, ModelID FROM Images  "+
                //            "where ModelID = (select ModelIDAbstract from Models M2 where  M2.ModelID = "+ item_id +" ))  "+
                //            "AS Im ON M.ModelIDAbstract = Im.ModelID " +
                //            "LEFT JOIN TBL_COLOR TC ON TC.COLOR_ID = TME.COLOR_ID "+
                //            "where M.ModelID = " + item_id;     
                string SQL = "SELECT ModelIDAbstract, M.ModelID, ModelCode, TC.COLOR, I.ItemsUDValue6, I.ItemsUDValue5, TME.MATERIALS, ModelName, "
                                    + " ISNULL(AF.ImageID, MF.ImageID) AS ImageID, CostMostRecent, C.ConditionCode,FURNITURE_TYPE AS [Subcategory],UDTextModel1 AS [Model] " +
                            "FROM Models M  " +
                            "JOIN Items I on I.ModelID = M.ModelID  " +
                            "LEFT JOIN Conditions C ON C.ConditionID = I.ConditionID  " +
                            "LEFT JOIN TBL_MODEL_EXTENDED TME ON TME.Model_ID = M.ModelIDAbstract " +
                            " LEFT JOIN TBL_FURNITURE_TYPE FT ON FT.FURNITURE_TYPE_ID = TME.FURNITURE_TYPE_ID " +
                            "Left JOIN TBL_MATCHED_MODEL TMM on TMM.MAIN_MODEL_ID = M.ModelID  " +
                            "LEFT JOIN (SELECT MIN(ImageID) as ImageID, ModelID FROM Images Group by ModelID) AS AF ON AF.ModelID = M.ModelID " +
                            "LEFT JOIN (SELECT MIN(ImageID) as ImageID, ModelID FROM Images Group by ModelID) AS MF ON MF.ModelID = M.ModelIDAbstract " +
                            "LEFT JOIN TBL_COLOR TC ON TC.COLOR_ID = TME.COLOR_ID  " +
                            "where M.ModelID =" + item_id;
                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows[0]["ModelID"] != null)
                    p.id = (int)dt.Rows[0]["ModelID"];

                if (dt.Rows[0]["ModelIDAbstract"] != null)
                    p.ModelIDAbstract = (int)dt.Rows[0]["ModelIDAbstract"];

                if (dt.Rows[0]["ItemsUDValue6"] != null)
                    p.COLOR = dt.Rows[0]["ItemsUDValue6"].ToString();

                if (dt.Rows[0]["ItemsUDValue5"] != null)
                    p.MATERIALS = dt.Rows[0]["ItemsUDValue5"].ToString();

                if (!string.IsNullOrEmpty(dt.Rows[0]["ModelName"].ToString()))
                    p.ModelName = (string)dt.Rows[0]["ModelName"];
                else p.ModelNumber = string.Empty;
                if (!string.IsNullOrEmpty(dt.Rows[0]["ModelCode"].ToString()))
                    p.ModelNumber = (string)dt.Rows[0]["ModelCode"];
                else p.ModelNumber = string.Empty;

                if (!string.IsNullOrEmpty(dt.Rows[0]["ConditionCode"].ToString()))
                    p.ConditionCode = (string)dt.Rows[0]["ConditionCode"];
                else p.ConditionCode = string.Empty;


                if (!string.IsNullOrEmpty(dt.Rows[0]["CostMostRecent"].ToString()))
                    p.CostMostRecent = Convert.ToDecimal(dt.Rows[0]["CostMostRecent"]);
                else p.CostMostRecent = 0;

                if (dt.Rows[0]["ImageID"] != null)
                    p.ProductImage = Convert.ToString(dt.Rows[0]["ImageID"]);
                else p.ProductImage = "0";

                if (!string.IsNullOrEmpty(dt.Rows[0]["Subcategory"].ToString()))
                    p.Subcategory = (string)dt.Rows[0]["Subcategory"];
                else p.Subcategory = string.Empty;

                if (!string.IsNullOrEmpty(dt.Rows[0]["Model"].ToString()))
                    p.Model = (string)dt.Rows[0]["Model"];
                else p.Model = string.Empty;


                //Model Name Abstract 

                string SQL2 = "select ModelName from Models where ModelID=(select ModelIDAbstract from Models where ModelID=" + item_id + ")";
                SqlDataAdapter da2 = new SqlDataAdapter(SQL2, sqlConn);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);

                if (!string.IsNullOrEmpty(dt2.Rows[0]["ModelName"].ToString()))
                    p.ModelName = (string)dt2.Rows[0]["ModelName"];
                else p.ModelName = string.Empty;


                string sql3 = "UPDATE Models SET ModelsUDValueCode2 = 'InCart' WHERE ModelID = " + item_id + " AND ModelsUDValueCode2='Surplus'";
                SqlCommand SQL_CMD = new SqlCommand(sql3, sqlConn);

                List<int> ItemInCart = new List<int>();
                if (System.Web.HttpContext.Current.Session["ItemInCart"] != null)
                {
                    ItemInCart = (List<int>)System.Web.HttpContext.Current.Session["ItemInCart"];
                }
                ItemInCart.Add(Convert.ToInt32(item_id));
                System.Web.HttpContext.Current.Session["ItemInCart"] = ItemInCart;
                SQL_CMD.CommandType = CommandType.Text;
                SQL_CMD.Parameters.Clear();
                SQL_CMD.Connection.Open();
                SQL_CMD.ExecuteNonQuery();
                SQL_CMD.Connection.Close();
            }

            catch (Exception ex)
            {

            }

            return p;
        }


        #endregion

        //Pop up list menu
        #region Getasstes_needed
        [OperationContract]
        [
        WebInvoke
        (
        Method = "GET",
        UriTemplate = "Getasstes_needed?Input={Input}",
        BodyStyle = WebMessageBodyStyle.Bare,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )
        ]
        public PageProductInfo Getasstes_needed(string Input)
        {
            #region Declaration And Initialization Section.
            //get info from ajax call
            PageInfo oPageInfo = new PageInfo();

            //return values
            PageProductInfo Result = new PageProductInfo();
            List<product> oReturnValue = new List<product>();
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            #endregion

            try
            {
                oPageInfo = jscript.Deserialize<PageInfo>(Input);


                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                //get Images From Database 
                //here ModelID Id is CategorieId
                int index = (int)oPageInfo.IndexPage;
                int ModelID = (int)oPageInfo.CategorieId;
                int fromIndex = (index * 8) + 1;
                int toIndex = fromIndex + 7;



                string SQL = "SELECT  * FROM   ( " +
                             "select M.ModelID, ModelCode, ModelName, CostMostRecent,ConditionCode,  ROW_NUMBER() OVER (ORDER BY M.ModelID) As x " +
                             " from Models M JOIN Items I ON I.ModelID = M.Modelid " +
                              "LEFT JOIN Conditions C on C.ConditionID = I.ConditionID " +

                              "where   ModelsUDValueCode2 like 'Surplus' AND  M.deleted = 0 AND ModelIDAbstract=" + ModelID + " ) AS tbl " +
                             "WHERE  tbl.x BETWEEN " + fromIndex + " AND " + toIndex + " ORDER BY  ConditionCode DESC";

                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                int nbr_line_query = (int)dt.Rows.Count;

                //get Numbr of row in database 
                string SQL2 = "select   count(ModelID)  from Models  where  ModelsUDValueCode2 like 'Surplus' AND  deleted = 0 AND  ModelIDAbstract=" + ModelID;
                //string SQL2 = "SELECT count(*) FROM ModelsViewWithImagesIN";
                SqlDataAdapter reponse = new SqlDataAdapter(SQL2, sqlConn);
                DataTable nbt = new DataTable();
                reponse.Fill(nbt);
                int nbr_row_total = (int)nbt.Rows[0][0];

                for (int i = 0; i < 8; i++)
                {
                    if (nbr_row_total == i)
                        break;

                    if (nbr_line_query == i)
                        break;

                    product p = new product();
                    if (dt.Rows[i]["ModelID"] != null)
                        p.id = (int)dt.Rows[i]["ModelID"];
                    else break;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["ModelCode"].ToString()))
                        p.ModelNumber = (string)dt.Rows[i]["ModelCode"];
                    else p.ModelNumber = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["ModelName"].ToString()))
                        p.ModelName = (string)dt.Rows[i]["ModelName"];
                    else p.ModelName = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["ConditionCode"].ToString()))
                        p.ConditionCode = (string)dt.Rows[i]["ConditionCode"];
                    else p.ConditionCode = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["CostMostRecent"].ToString()))
                        p.CostMostRecent = Convert.ToDecimal(dt.Rows[i]["CostMostRecent"]);
                    else p.CostMostRecent = 0;

                    //get image from Image Table 
                    // string SQL3 = "select TOP 1 ModelImage From Images  where  ModelID=" + ModelID;
                    //SqlDataAdapter reponse3 = new SqlDataAdapter(SQL3, sqlConn);
                    //DataTable idt = new DataTable();
                    //reponse3.Fill(idt);
                    //int nbr_rows = idt.Rows.Count;

                    //if (nbr_rows != 0)
                    //{
                    //    try{
                    //    if (!string.IsNullOrEmpty(idt.Rows[0][0].ToString()))
                    //        p.ProductImage = Convert.ToBase64String((byte[])idt.Rows[0][0]);
                    //    }
                    //    catch (Exception ex)
                    //    {

                    //    }

                    //}
                    //else
                    p.ProductImage = "none";


                    oReturnValue.Add(p);
                }

                Result.ListProduct = oReturnValue;
                Result.nbrPages = nbr_row_total / 8;
            }

            catch (Exception ex)
            {

            }
            return Result;
        }

        private bool IsNullOrEmpty(string p)
        {
            throw new NotImplementedException();
        }

        #endregion

        //Model info in product_details.aspx page 
        #region GetProductDetails
        [OperationContract]
        [
        WebInvoke
        (
        Method = "GET",
        UriTemplate = "GetProductDetails?Input={Input}",
        BodyStyle = WebMessageBodyStyle.Bare,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )
        ]
        public product_Details GetProductDetails(string Input)
        {
            #region Declaration And Initialization Section.
            //get info from ajax call
            //PageInfo oPageInfo = new PageInfo();

            //return values
            product_Details Result = new product_Details();
            List<String> ListImages = new List<String>();
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            #endregion

            try
            {
                // oPageInfo = jscript.Deserialize<PageInfo>(Input);
                String ModelID = Input;

                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);

                //Fill lsite images

                //End Fill lsite images

                //get Quantity
                // string SQL2 = "select count(ModelID) as NumberItems from Models where ItemTypeId=1 and ModelIDAbstract =" + ModelID;
                //SqlDataAdapter da2 = new SqlDataAdapter(SQL2, sqlConn);
                //DataTable dt2 = new DataTable();
                //da2.Fill(dt2);
                //Result.Quantity =(int)dt2.Rows[0]["NumberItems"];
                //End get Quantity

                //all info in right box
                string SQL3 = "select M.ModelName,M.Color as Features, M.Length as SpecialFeatures, M.Size as Dimensions, " +
                "M.ModelIDAbstract, I.ItemsUDValue6, I.ItemsUDValue5, M.ModelIDAbstract, C.ConditionCode, M.ModelCode AS MainModelCode,FURNITURE_TYPE, " +
                "ISNULL(M.Color,TC.COLOR) AS Color, ME.FEATURES, ME.MATERIALS, ME.ADDITIONAL_INFO, " +
                "ME.SPECIAL_FEATURES, ME.SPECIFICATIONS,MM.ModelCode AS MatchedModelCode, MM.ModelName AS MatchedModelName, " +
                "MM.ModelID AS MatchedModelModelID, AM.ModelCode AS AbstractModelCode, AT.AssetTypeCode AS AssetType, " +
                "CAT.CategoryCode, M.UDTextModel1, I.SerialNumber, I.CPU, I.CPUSpeed, I.HardDriveSize, I.IPAddress, I.OS, I.BIOS, I.RAM, I.VideoCard, I.MAC, " +
                "M.Resolution, M.ScreenSize, M.SoftwareVersion, M.SoftwareLicenses " +
                "FROM Models M " +
                "JOIN Items I on I.ModelID = M.ModelID " +
                "LEFT JOIN Models AM ON AM.ModelID = M.ModelIDAbstract " +
                "LEFT JOIN AssetTypes AT on AT.AssetTypeID = M.AssetTypeID " +
                "LEFT JOIN Conditions C ON C.ConditionID = I.ConditionID " +
                "LEFT JOIN TBL_MODEL_EXTENDED ME ON ME.MODEL_ID = M.ModelIDAbstract " +
                "LEFT JOIN TBL_FURNITURE_TYPE FT ON FT.FURNITURE_TYPE_ID = ME.FURNITURE_TYPE_ID " +
                "LEFT JOIN TBL_COLOR TC ON TC.COLOR_ID = ME.COLOR_ID " +
                "Left JOIN TBL_MATCHED_MODEL TMM on TMM.MAIN_MODEL_ID = M.ModelIDAbstract " +
                "LEFT JOIN Models MM ON MM.ModelIDAbstract = TMM.MODEL_ID " +
                "LEFT JOIN Categories CAT on CAT.CategoryID = M.CategoryID " +
                "WHERE M.ModelID = " + ModelID;



                SqlDataAdapter da3 = new SqlDataAdapter(SQL3, sqlConn);
                DataTable dt3 = new DataTable();
                da3.Fill(dt3);


                string SQL = "Select  ISNULL(I.ImageID,IABM.ImageID) as ModelImage From Models M"
                               + " LEFT JOIN Images I on M.ModelID = I.ModelID"
                               + " LEFT JOIN Images IABM on IABM.ModelID = M.ModelIDAbstract"
                               + " where  M.ModelID=" + ModelID;
                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                //if (dt.Rows.Count == 0)
                //{
                //    string SQL5 = "select  ImageID as ModelImage From Images  where  ModelID=" + dt3.Rows[0]["ModelIDAbstract"];
                //    SqlDataAdapter reponse4 = new SqlDataAdapter(SQL5, sqlConn);
                //    reponse4.Fill(dt);

                //}

                int nbr_rows = dt.Rows.Count;


                for (int i = 0; i < nbr_rows; i++)
                {
                    try
                    {

                        if (!string.IsNullOrEmpty(dt.Rows[i][0].ToString()))
                            ListImages.Add(Convert.ToString(dt.Rows[i][0]));
                    }
                    catch (Exception ex)
                    {

                    }

                }

                Result.ProductImage = ListImages;

                if (!string.IsNullOrEmpty(dt3.Rows[0]["Resolution"].ToString()))
                    Result.resolution = (string)dt3.Rows[0]["Resolution"];

                else Result.resolution = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["ScreenSize"].ToString()))
                    Result.ScreenSize = (string)dt3.Rows[0]["ScreenSize"];

                else Result.ScreenSize = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["SoftwareVersion"].ToString()))
                    Result.SoftwareVersion = (string)dt3.Rows[0]["SoftwareVersion"];

                else Result.SoftwareVersion = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["SoftwareLicenses"].ToString()))
                    Result.SoftwareLicenses = (string)dt3.Rows[0]["SoftwareLicenses"];

                else Result.SoftwareLicenses = "";




                if (!string.IsNullOrEmpty(dt3.Rows[0]["CPU"].ToString()))
                    Result.CPU = (string)dt3.Rows[0]["CPU"];

                else Result.CPU = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["CPUSpeed"].ToString()))
                    Result.CPUspeed = (string)dt3.Rows[0]["CPUSpeed"];

                else Result.CPUspeed = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["HardDriveSize"].ToString()))
                    Result.HDSize = (string)dt3.Rows[0]["HardDriveSize"];

                else Result.HDSize = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["IPAddress"].ToString()))
                    Result.IP = (string)dt3.Rows[0]["IPAddress"];

                else Result.IP = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["BIOS"].ToString()))
                    Result.BIOS = (string)dt3.Rows[0]["BIOS"];

                else Result.BIOS = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["RAM"].ToString()))

                    Result.RAM = (string)dt3.Rows[0]["RAM"];

                else Result.RAM = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["VideoCard"].ToString()))
                    Result.VideoCard = (string)dt3.Rows[0]["VideoCard"];

                else Result.VideoCard = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["MAC"].ToString()))
                    Result.MAC = (string)dt3.Rows[0]["MAC"];

                else Result.MAC = "";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["ModelName"].ToString()))
                    Result.ModelName = (string)dt3.Rows[0]["ModelName"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["AbstractModelCode"].ToString()))
                    Result.ModelCode = (string)dt3.Rows[0]["AbstractModelCode"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["AssetType"].ToString()))
                    Result.assetType = (string)dt3.Rows[0]["AssetType"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["CategoryCode"].ToString()))
                    Result.CategoryCode = (string)dt3.Rows[0]["CategoryCode"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["UDTextModel1"].ToString()))
                    Result.Model = (string)dt3.Rows[0]["UDTextModel1"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["SerialNumber"].ToString()))
                    Result.SerialNum = (string)dt3.Rows[0]["SerialNumber"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["MainModelCode"].ToString()))
                    Result.MainModelCode = (string)dt3.Rows[0]["MainModelCode"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["FURNITURE_TYPE"].ToString()))
                    Result.FURNITURE_TYPE = (string)dt3.Rows[0]["FURNITURE_TYPE"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["ItemsUDValue6"].ToString()))
                    Result.COLOR = (string)dt3.Rows[0]["ItemsUDValue6"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["Features"].ToString()))
                    Result.FEATURES = "<li>" + ((string)dt3.Rows[0]["Features"]).Replace(System.Environment.NewLine, "</li><li>") + "</li>";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["ItemsUDValue5"].ToString()))
                    Result.MATERIALS = (string)dt3.Rows[0]["ItemsUDValue5"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["ADDITIONAL_INFO"].ToString()))
                    Result.ADDITIONAL_INFO = (string)dt3.Rows[0]["ADDITIONAL_INFO"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["SpecialFeatures"].ToString()))
                    Result.SPECIAL_FEATURES = "<li>" + ((string)dt3.Rows[0]["SpecialFeatures"]).Replace(System.Environment.NewLine, "</li><li>") + "</li>";

                if (!string.IsNullOrEmpty(dt3.Rows[0]["Dimensions"].ToString()))
                    Result.SPECIFICATIONS = (string)dt3.Rows[0]["Dimensions"];

                if (!string.IsNullOrEmpty(dt3.Rows[0]["ConditionCode"].ToString()))
                    Result.Conditions = (string)dt3.Rows[0]["ConditionCode"];
                else Result.Conditions = string.Empty;


                List<MatchedModel> ML = new List<MatchedModel>();


                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    MatchedModel M = new MatchedModel();
                    if (!string.IsNullOrEmpty(dt3.Rows[i]["MatchedModelCode"].ToString()))
                        M.MatchedModelCode = (string)dt3.Rows[i]["MatchedModelCode"];

                    if (!string.IsNullOrEmpty(dt3.Rows[i]["MatchedModelModelID"].ToString()))
                        M.MatchedModelModelID = (int)dt3.Rows[i]["MatchedModelModelID"];

                    if (!string.IsNullOrEmpty(dt3.Rows[i]["MatchedModelName"].ToString()))
                        M.MatchedModelName = (string)dt3.Rows[i]["MatchedModelName"];

                    try
                    {
                        if (!string.IsNullOrEmpty(dt3.Rows[0][0].ToString()))
                            M.ModelImage = Convert.ToBase64String((byte[])dt3.Rows[i]["ModelImage"]);
                    }
                    catch (Exception ex)
                    {

                    }


                    ML.Add(M);
                }

                Result.MatchedModel = ML;
                // End Right Box

            }

            catch (Exception ex)
            {

            }



            return Result;
        }



        #endregion

        //List of Models filtred By ID Categories
        #region GetProducts_needed
        [OperationContract]
        [
        WebInvoke
        (
        Method = "GET",
        UriTemplate = "GetProducts_needed?Input={Input}",
        BodyStyle = WebMessageBodyStyle.Bare,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )
        ]
        public PageProductInfo GetProducts_needed(string Input)
        {
            #region Declaration And Initialization Section.
            //get info from ajax call
            PageInfo oPageInfo = new PageInfo();

            //return values
            PageProductInfo Result = new PageProductInfo();
            List<product> oReturnValue = new List<product>();
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            #endregion

            try
            {
                oPageInfo = jscript.Deserialize<PageInfo>(Input);


                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                //get Images From Database 
                int index = (int)oPageInfo.IndexPage;

                int fromIndex = (index * 25) + 1;
                int toIndex = fromIndex + 24;

                //int index = (int)oPageInfo.IndexPage * 8;
                int CategorieID = (int)oPageInfo.CategorieId;
                int furnitureID = (int)oPageInfo.FurnitureId;
                //int fromIndex = (index * 8) + 1;
                // int toIndex = fromIndex + 7;

                // string SQL = "SELECT  * FROM   ( select ModelID, ModelCode, ModelName,  ROW_NUMBER() OVER (ORDER BY ModelID) As x from Models where ItemTypeID=5  and CategoryID=" + CategorieID + " ) AS tbl WHERE  tbl.x BETWEEN " + index + " AND " + (index + 8);

                string SQL = "SELECT * FROM ( "
                             + " SELECT ModelIDAbstract, I.ItemsUDValue6, I.ItemsUDValue5, M.ModelID, ModelCode, TC.COLOR, TME.MATERIALS, ModelName,UDTextModel1 AS [Model],FURNITURE_TYPE AS [Subcategory],"
                             + " CostMostRecent, C.ConditionCode,ISNULL(AF.ImageID, MF.ImageID) AS ImageID,ROW_NUMBER() "
                             + " OVER (ORDER BY ModelIDAbstract ASC) As x "
                             + " FROM Models M"
                             + " JOIN Items I on I.ModelID = M.ModelID"
                             + " LEFT JOIN Conditions C ON C.ConditionID = I.ConditionID  "
                             + " LEFT JOIN TBL_MODEL_EXTENDED TME ON TME.Model_ID = M.ModelIDAbstract "
                             + " LEFT JOIN TBL_FURNITURE_TYPE FT ON FT.FURNITURE_TYPE_ID = TME.FURNITURE_TYPE_ID "
                             + " Left JOIN TBL_MATCHED_MODEL TMM on TMM.MAIN_MODEL_ID = M.ModelID "
                             + " LEFT JOIN TBL_COLOR TC ON TC.COLOR_ID = TME.COLOR_ID "
                             + " LEFT JOIN (SELECT MIN(ImageID) as ImageID, ModelID FROM Images Group by ModelID) AS AF ON AF.ModelID = M.ModelID"
                             + " LEFT JOIN (SELECT MIN(ImageID) as ImageID, ModelID FROM Images Group by ModelID) AS MF ON MF.ModelID = M.ModelIDAbstract"
                             + " WHERE M.Deleted=0 and I.Deleted= 0 and M.CategoryID =" + CategorieID + " AND TME.FURNITURE_TYPE_ID = " + furnitureID + " and M.ModelsUDValueCode2 = 'Surplus' "
                             + ")  AS Data WHERE x BETWEEN " + fromIndex + " AND " + toIndex;

                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                int nbr_line_query = (int)dt.Rows.Count;


                // modified by johnny 3/31/2017 adding conditon where deleted = 0 ;
                string SQL3 = "SELECT Count(ModelID) FROM ( SELECT ModelIDAbstract, M.ModelID, ModelCode, TC.COLOR, TME.MATERIALS, ModelName," +
                            "CostMostRecent, C.ConditionCode,ROW_NUMBER() OVER (ORDER BY ModelIDAbstract ASC) As x " +
                            "FROM Models M " +
                            "JOIN Items I on I.ModelID = M.ModelID " +
                            "LEFT JOIN Conditions C ON C.ConditionID = I.ConditionID " +
                            "LEFT JOIN TBL_MODEL_EXTENDED TME ON TME.Model_ID = M.ModelIDAbstract " +
                            "Left JOIN TBL_MATCHED_MODEL TMM on TMM.MAIN_MODEL_ID = M.ModelID " +
                            "LEFT JOIN TBL_COLOR TC ON TC.COLOR_ID = TME.COLOR_ID " +
                            "WHERE M.Deleted=0 and I.Deleted= 0 and M.CategoryID = " + CategorieID + " AND TME.FURNITURE_TYPE_ID = " + furnitureID + " and M.ModelsUDValueCode2 = 'SurPlus') " +
                            " AS Data ";
                SqlDataAdapter reponse = new SqlDataAdapter(SQL3, sqlConn);
                DataTable nbt = new DataTable();
                reponse.Fill(nbt);
                int nbr_row_total = (int)nbt.Rows[0][0];

                //get Numbr of row in database 

                for (int i = 0; i < 25; i++)
                {

                    if (nbr_row_total == i)
                        break;
                    if (nbr_line_query == i)
                        break;

                    product p = new product();

                    if (dt.Rows[i]["ModelID"] != null)
                        p.id = (int)dt.Rows[i]["ModelID"];
                    else break;

                    if (dt.Rows[i]["ModelIDAbstract"] != null)
                        p.ModelIDAbstract = (int)dt.Rows[i]["ModelIDAbstract"];
                    else break;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["ItemsUDValue6"].ToString()))
                        p.COLOR = (string)dt.Rows[i]["ItemsUDValue6"];
                    else p.COLOR = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["ItemsUDValue5"].ToString()))
                        p.MATERIALS = (string)dt.Rows[i]["ItemsUDValue5"];
                    else p.MATERIALS = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["ModelName"].ToString()))
                        p.ModelName = (string)dt.Rows[i]["ModelName"];
                    else p.ModelName = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["ModelCode"].ToString()))
                        p.ModelNumber = (string)dt.Rows[i]["ModelCode"];
                    else p.ModelNumber = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["ConditionCode"].ToString()))
                        p.ConditionCode = (string)dt.Rows[i]["ConditionCode"];
                    else p.ConditionCode = string.Empty;

                    if (dt.Rows[i]["ImageID"] != null)
                        p.ProductImage = Convert.ToString(dt.Rows[i]["ImageID"]);
                    else p.ProductImage = "0";

                    if (!string.IsNullOrEmpty(dt.Rows[i]["Subcategory"].ToString()))
                        p.Subcategory = (string)dt.Rows[i]["Subcategory"];
                    else p.Subcategory = string.Empty;

                    if (!string.IsNullOrEmpty(dt.Rows[i]["Model"].ToString()))
                        p.Model = (string)dt.Rows[i]["Model"];
                    else p.Model = string.Empty;

                    oReturnValue.Add(p);
                }

                Result.ListProduct = oReturnValue;
                Result.nbrPages = nbr_row_total / 25;
            }
            catch (Exception ex)
            {
                string T = ex.ToString();
            }
            return Result;
        }


        #endregion
        #region GetOrder_neededGeneral

        [OperationContract]
        [
        WebInvoke
        (
        Method = "GET",
        UriTemplate = "Getorder_neededGeneral?Input={Input}",
        BodyStyle = WebMessageBodyStyle.Bare,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )
        ]
        public PageorderInfo Getorder_neededGeneral(string Input)
        {
            #region Declaration And Initialization Section.
            PageorderInfo Results = new PageorderInfo();
            try
            {
               Pageorderrequest Pageorderrequest1 = new Pageorderrequest();
                List<Order> Order = new List<Order>();
                JavaScriptSerializer jscript = new JavaScriptSerializer();
                Pageorderrequest1 = jscript.Deserialize<Pageorderrequest>(Input);
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                //get Images From Database 
                int index = (int)Pageorderrequest1.index;
                string customerid = Pageorderrequest1.CustomLogin;
                string flag = Pageorderrequest1.flag;
                int fromIndex = (index * 25) + 1;
                int toIndex = fromIndex + 24;
                int soStatus = 0;
                if (flag == "open")
                    soStatus = 0;
                else
                    soStatus = 1;
                string SQL = "select CategoryID, CategoryCode,CAtegory from Categories "
                    + " where Category='asset' AND CategoryID IN (SELECT CategoryID from Models where ItemTypeID = 1"
                    + " AND (AssetTypeID = 1 OR AssetTypeID = 2 OR AssetTypeID = 3 OR AssetTypeID = 5 OR AssetTypeID = 6 OR AssetTypeID = 18 OR AssetTypeID = 19 OR AssetTypeID = 20 )AND ModelsUDValueCode2 = 'Surplus')"
                    + " order by CategoryCode asc";
                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dtOutput = new DataTable();
                da.Fill(dtOutput);
                string category = "";
                for (int i = 0; i < dtOutput.Rows.Count; i++)
                {
                    if (i == dtOutput.Rows.Count-1)
                    {
                        category += dtOutput.Rows[i]["CategoryID"];
                    }
                    else{
                        category += dtOutput.Rows[i]["CategoryID"] + ",";
                    }
                }
                string SQl = "";
              
           
                  SQl = " select * from ( "
                  + " select  IS2.IssueOrderID as orderid ,IS2.IssueOrderCode as orderNumber,IS2.orderDate as orderDate,AD.AddressName as AddressName,ISU.issueename as issueename,ISU.issueecode as issueecode,IS2.subtotal as TotalPrice,ROW_NUMBER() OVER ( order by IS2.IssueOrderID ASC ) AS x "
                  + "from   issueorders as IS2 "
                  + "left join  Addresses AD on  AD.AddressID = IS2.Shiptoaddressid left join issueorderline IOL on IOL.IssueOrderID = IS2.IssueOrderID left join Models M on M.modelid = IOL.Modelid left join categories CA on CA.categoryid = M.categoryid left join Issuees ISU on ISU.issueeid = IS2.issueeid "
                  + "where IS2.Addedby='"+customerid+"'and IS2.soclosed="+soStatus+""
                  + "and CA.CategoryId in (" + category + ") "
                  + " group by IS2.IssueOrderID  ,IS2.IssueOrderCode ,IS2.orderDate ,AD.AddressName ,IS2.subtotal,ISU.issueename,ISU.issueecode )"
                  + "as data where x between " + fromIndex + "  and " + toIndex + "";
                 


                SqlDataAdapter da1 = new SqlDataAdapter(SQl, sqlConn);
                DataTable dt = new DataTable();
                da1.Fill(dt);
                int nbr_line_query = (int)dt.Rows.Count;
                string SQlall = " select * from ( "
                  + " select  IS2.IssueOrderID as orderid ,IS2.IssueOrderCode as orderNumber,IS2.orderDate as orderDate,AD.AddressName as AddressName,IS2.subtotal as TotalPrice,ROW_NUMBER() OVER ( order by IS2.IssueOrderID ASC ) AS x "
                  + "from   issueorders as IS2 "
                  + " left join  Addresses AD on  AD.AddressID = IS2.shipToAddressID left join issueorderline IOL on IOL.IssueOrderID = IS2.IssueOrderID left join Models M on M.modelid = IOL.Modelid left join categories CA on CA.categoryid = M.categoryid where IS2.Addedby='" + customerid + "' "
                  + "and CA.categoryID in(" + category + ") "
                  + "group by IS2.IssueOrderID  ,IS2.IssueOrderCode ,IS2.orderDate ,AD.AddressName ,IS2.subtotal ) "
                  + "as data ";
                SqlDataAdapter da2 = new SqlDataAdapter(SQlall, sqlConn);
                DataTable dt1 = new DataTable();
                da2.Fill(dt1);

                int nbr_row_total = (int)dt1.Rows.Count;
                   for (int i = 0; i < 25; i++)
                {

                    if (nbr_row_total == i)
                        break;
                    if (nbr_line_query == i)
                        break;

                    Order p = new Order();
                    if (dt.Rows[i]["orderid"].ToString() != null && dt.Rows[i]["orderid"].ToString() != "")
                        p.OrderId = Convert.ToInt32(dt.Rows[i]["orderid"].ToString());
                    if (dt.Rows[i]["orderNumber"].ToString() != null && dt.Rows[i]["orderNumber"].ToString()!="")
                        p.orderNumber = dt.Rows[i]["orderNumber"].ToString();

                    if (dt.Rows[i]["orderDate"].ToString() != null && dt.Rows[i]["orderDate"].ToString() != "")
                        p.OrderDate = Convert.ToDateTime(dt.Rows[i]["orderDate"].ToString());

                    if (dt.Rows[i]["AddressName"].ToString() != null && dt.Rows[i]["AddressName"].ToString() != "")
                        p.AddressToShip = dt.Rows[i]["AddressName"].ToString();

                    if (dt.Rows[i]["TotalPrice"].ToString() != null && dt.Rows[i]["TotalPrice"].ToString() != "")
                        p.TotalPrice = Convert.ToDouble(dt.Rows[i]["TotalPrice"].ToString());
                       if(dt.Rows[i]["issueename"].ToString()!=null && dt.Rows[i]["issueename"].ToString()!="")
                       p.issuename=dt.Rows[i]["issueename"].ToString();
                       if (dt.Rows[i]["issueecode"].ToString() != null && dt.Rows[i]["issueecode"].ToString() != "")
                           p.issuecode = dt.Rows[i]["issueecode"].ToString();

                    Order.Add(p);
                } // end for
                Results.ListOrder = Order;
                Results.nbrPages = nbr_row_total / 25;
            


            #endregion

            }
            catch (Exception e1)
            {
                string message = e1.Message;
            }

            return Results;
        }
        #endregion
        // johnny 6/13/12 list of order flitering by customer id and order id 
        #region GetOrder_needed
        [OperationContract]
        [
        WebInvoke
        (
        Method = "GET",
        UriTemplate = "GeTorder_needed_?Input={Input}",
        BodyStyle = WebMessageBodyStyle.Bare,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )
        ]
        public PageorderInfo GeTorder_needed_(string Input)
        {
                             //TBL_MODEL_EXTENDED TME ON TME.Model_ID = M.ModelIDAbstract "
                            // + " LEFT JOIN TBL_FURNITURE_TYPE FT ON FT.FURNITURE_TYPE_ID = TME.FURNITURE_TYPE_ID "
            #region Declaration And Initialization Section.
            PageorderInfo Results1 = new PageorderInfo();
            try
            {
                Pageorderrequest1 Pageorderrequest2 = new Pageorderrequest1();
                
                List<Order> Order = new List<Order>();
                JavaScriptSerializer jscript1 = new JavaScriptSerializer();
                 Pageorderrequest2  = jscript1.Deserialize<Pageorderrequest1>(Input);
            #endregion

                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                //get Images From Database 
                int index1 = Pageorderrequest2.index;
                string customerid1 = Pageorderrequest2.CustomLogin;
                int orderid1 = Pageorderrequest2.orderid;

                int fromIndex1 = (index1 * 25) + 1;
                int toIndex1 = fromIndex1 + 24;

                string SQL = "Select * from ( "
                + "Select IS2.IssueOrderID ,IS2.IssueOrderCode as orderNumber, M.modelcode as modelcode,M.ModelIDAbstract,FT.FURNITURE_TYPE as subcategory, M.modelname as modelname, M.costmostrecent as price, ROW_NUMBER() OVER ( order by IS2.IssueOrderCode) as x  from issueorderline as ISL,models as M, issueOrders as IS2, TBL_MODEL_EXTENDED as TME, TBL_FURNITURE_TYPE as FT  "
                + "where ISL.AddedBy = '" + customerid1 + "' and IS2.IssueOrderID =" + orderid1+ " and IS2.IssueOrderID = ISL.IssueOrderID and M.Modelid = ISL.Modelid and FT.FURNITURE_TYPE_ID = TME.FURNITURE_TYPE_ID and TME.Model_ID = M.ModelIDAbstract  group by IS2.IssueOrderID ,IS2.IssueOrderCode,M.modelcode,M.modelname, M.costmostrecent,M.ModelIDAbstract,FT.FURNITURE_TYPE ) as data where x between " + fromIndex1 + " and " + toIndex1 + "";
                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                int nbr_line_query = (int)dt.Rows.Count;

                string SQLall = " Select count (*) from"
                     + "( select IS2.IssueOrderid as iod,  IS2.IssueOrderCode as orderNumber , M.modelcode as modelcode,M.modelname as modelname, M.costMostRecent as price, ROW_NUMBER() OVER ( order by IS2.IssueOrderCode asc) as x  from issueorderline as ISL,models as M , "
                    + " issueOrders as IS2"
                   + " where  ISL.AddedBy ='" + customerid1 + "' and IS2.IssueOrderID =" + orderid1 + " and IS2.IssueOrderID = ISL.IssueOrderID and M.Modelid = ISL.Modelid  group by IS2.IssueOrderid,  IS2.IssueOrderCode , M.modelcode ,M.modelname , M.costMostRecent)  as data";
                SqlDataAdapter reponse = new SqlDataAdapter(SQLall, sqlConn);
                DataTable nbt = new DataTable();
                reponse.Fill(nbt);
                int nbr_row_total = (int)nbt.Rows[0][0];

                //get Numbr of row in database 

                for (int i = 0; i < 25; i++)
                {

                    if (nbr_row_total == i)
                        break;
                    if (nbr_line_query == i)
                        break;

                    Order p = new Order();

                    p.OrderId = orderid1;
                    p.OrderDate = DateTime.UtcNow;

                    if (dt.Rows[i]["orderNumber"].ToString() != null && dt.Rows[i]["orderNumber"].ToString()!="")
                        p.orderNumber = dt.Rows[i]["orderNumber"].ToString();

                    if (dt.Rows[i]["modelcode"].ToString() != null && dt.Rows[i]["modelcode"].ToString() != "")
                        p.ModelNumber = dt.Rows[i]["modelcode"].ToString();

                    if (dt.Rows[i]["subcategory"].ToString() != null && dt.Rows[i]["modelname"].ToString() != "")
                        p.ModelName = dt.Rows[i]["subcategory"].ToString();
                   
                    if (dt.Rows[i]["price"].ToString() != null && dt.Rows[i]["price"].ToString()!="")
                        p.price = Convert.ToDouble(dt.Rows[i]["price"].ToString());

                    Order.Add(p);


                }
                Results1.ListOrder = Order;
                Results1.nbrPages = nbr_row_total / 25;
            }
            catch (Exception e)
            {
               string e1 = e.Message;
            }

            return Results1;
        }

        //List of Models filtred By ID Categories and Furniture Id
        #region GetSubCategories_needed
        [OperationContract]
        [
        WebInvoke
        (
        Method = "GET",
        UriTemplate = "GetSubCategories_needed?Input={Input}",
        BodyStyle = WebMessageBodyStyle.Bare,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )
        ]
        public PageProductInfo GetSubCategories_needed(string Input)
        {
            #region Declaration And Initialization Section.
            //get info from ajax call
            PageInfo oPageInfo = new PageInfo();

            //return values
            PageProductInfo Result = new PageProductInfo();
            List<product> oReturnValue = new List<product>();
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            #endregion

            try
            {
                oPageInfo = jscript.Deserialize<PageInfo>(Input);


                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                //get Images From Database 


                int CategorieID = (int)oPageInfo.CategorieId;


                // string SQL = "SELECT  * FROM   ( select ModelID, ModelCode, ModelName,  ROW_NUMBER() OVER (ORDER BY ModelID) As x from Models where ItemTypeID=5  and CategoryID=" + CategorieID + " ) AS tbl WHERE  tbl.x BETWEEN " + index + " AND " + (index + 8);

                //string SQL = "select Distinct M.ModelIDAbstract, FURNITURE_TYPE,TF.FURNITURE_TYPE_ID";
                //SQL = SQL + " from TBL_FURNITURE_TYPE TF ";
                //SQL = SQL + " left join TBL_MODEL_EXTENDED TM on TM.FURNITURE_TYPE_ID = TF.FURNITURE_TYPE_ID ";
                //SQL = SQL + "  left join Models M  on M.ModelIDAbstract=TM.Model_ID ";
                //SQL = SQL + "  left join Categories C on C.CategoryID=M.CategoryID ";
                //SQL = SQL + "  where C.categoryID = " + CategorieID + " and TF.FURNITURE_TYPE_ID=" ;

                string SQL = "select  distinct FURNITURE_TYPE, TF.FURNITURE_TYPE_ID ";
                SQL = SQL + " from TBL_FURNITURE_TYPE TF ";
                SQL = SQL + " left join TBL_MODEL_EXTENDED TM on TM.FURNITURE_TYPE_ID = TF.FURNITURE_TYPE_ID ";
                SQL = SQL + " left join Models M  on M.ModelIDAbstract=TM.Model_ID ";
                SQL = SQL + " left join Categories C on C.CategoryID=M.CategoryID ";
                SQL = SQL + " where C.categoryID =" + CategorieID;
                SQL = SQL + " and  M.ModelsUDValueCode2 like 'Surplus' AND  M.deleted = 0   AND";
                SQL = SQL + "  ItemTypeId=1 and ModelIDAbstract = M.ModelIDAbstract order by FURNITURE_TYPE desc ";

                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dt = new DataTable();
                da.Fill(dt);

                int nbr_row_total = (int)dt.Rows.Count;

                for (int i = 0; i < nbr_row_total; i++)
                {


                    product p = new product();
                    if (dt.Rows[i]["FURNITURE_TYPE_ID"] != null)
                        p.id = (int)dt.Rows[i]["FURNITURE_TYPE_ID"];
                    else break;


                    if (!string.IsNullOrEmpty(dt.Rows[i]["FURNITURE_TYPE"].ToString()))
                        p.ModelName = (string)dt.Rows[i]["FURNITURE_TYPE"];
                    else p.ModelName = string.Empty;

                    p.COLOR = string.Empty;
                    p.MATERIALS = string.Empty;
                    p.ModelNumber = string.Empty;



                    //get image from Image Table 
                    string SQL3 = "";

                    //SQL3 = SQL3 + " select TOP 1 imageID as ModelImage From Images  where  ModelID=( ";
                    //SQL3=SQL3+" select  TOP 1  M.ModelIDAbstract from Models M ";
                    //SQL3=SQL3+"  JOIN Items I ON I.ModelID = M.ModelID   ";
                    // SQL3=SQL3+" LEFT JOIN TBL_MODEL_EXTENDED TM on TM.Model_ID = M.ModelIDAbstract   ";
                    //SQL3=SQL3+"  LEFT JOIN TBL_FURNITURE_TYPE TF  on TF.FURNITURE_TYPE_ID= TM.FURNITURE_TYPE_ID ";
                    //SQL3 = SQL3 + "   Where Quantity>0 and I.deleted = 0 AND ItemTypeID=1 AND ModelsUDValueCode2 like 'Surplus' AND  M.deleted = 0 and TF.FURNITURE_TYPE_ID=" + p.id + " and CategoryID=" + CategorieID + " )";

                    //SQL3 = " SELECT TOP 1 ABSM.Imageid AS ModelImage  From Images I " +
                    //        "JOIN(SELECT M.ModelID from Models M " +
                    //        "LEFT JOIN TBL_MODEL_EXTENDED TM on TM.Model_ID = M.ModelIDAbstract " +
                    //        "LEFT JOIN TBL_FURNITURE_TYPE TF  on TF.FURNITURE_TYPE_ID= TM.FURNITURE_TYPE_ID " +
                    //        "where ItemTypeID=1 AND ModelsUDValueCode2 like 'Donation' " +
                    //        "AND  M.deleted = 0 and TF.FURNITURE_TYPE_ID=" + p.id + " and CategoryID=" + CategorieID + ") AS F ON F.ModelID = I.ModelID " +
                    //        "LEFT JOIN ( SELECT TOP 1 Imageid, ModelIDAbstract From Images I " +
                    //        "JOIN(SELECT M.ModelIDAbstract from Models M " +
                    //        "LEFT JOIN TBL_MODEL_EXTENDED TM on TM.Model_ID = M.ModelIDAbstract    " +
                    //        "LEFT JOIN TBL_FURNITURE_TYPE TF  on TF.FURNITURE_TYPE_ID= TM.FURNITURE_TYPE_ID " +
                    //        "where ItemTypeID=1 AND ModelsUDValueCode2 like 'Surplus' " +
                    //        "AND  M.deleted = 0 and TF.FURNITURE_TYPE_ID=" + p.id + " and CategoryID=" + CategorieID + ") AS F ON F.ModelIDAbstract = I.ModelID) " +
                    //        "AS ABSM ON ABSM.ModelIDAbstract = I.ModelID";



                    SQL3 = " SELECT TOP 1 I.ImageID  from Models M"
                            + " JOIN TBL_MODEL_EXTENDED TME ON TME.Model_ID = M.ModelID"
                            + " JOIN TBL_FURNITURE_TYPE TFT ON TFT.FURNITURE_TYPE_ID = TME.FURNITURE_TYPE_ID"
                            + " JOIN Images I ON I.ModelID = M.ModelID"
                            + " JOIN Models A ON A.ModelIDAbstract = M.ModelID"
                            + " WHERE M.CategoryID = " + CategorieID + " AND M.Deleted=0 and A.Deleted= 0 and  TFT.FURNITURE_TYPE_ID = " + p.id + " and M.Itemtypeid = 5"
                            + " AND A.ModelsUDValueCode2 like 'Surplus'";

                    SqlDataAdapter reponse3 = new SqlDataAdapter(SQL3, sqlConn);
                    DataTable idt = new DataTable();
                    reponse3.Fill(idt);
                    int nbr_rows = idt.Rows.Count;

                    if (nbr_rows != 0)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(idt.Rows[0][0].ToString()))
                                p.ProductImage = Convert.ToString(idt.Rows[0][0]);
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    else
                        p.ProductImage = "none";



                    oReturnValue.Add(p);

                }

                Result.ListProduct = oReturnValue;

            }

            catch (Exception ex)
            {

            }
            return Result;
        }


        #endregion

        //Login 

        #region IsAuthenticated_User
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "IsAuthenticated_User?Input={Input}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public User IsAuthenticated_User(string Input)
        {
            #region Declaration And Initialization Section.

            User _Params_IsAuthenticated = new User();
            JavaScriptSerializer jscript = new JavaScriptSerializer();

            #endregion
            #region Body Section.
            try
            {

                _Params_IsAuthenticated = jscript.Deserialize<User>(Input);

                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);

                SqlCommand sqlCmd = new SqlCommand("Select * from Users U left join TBL_WEB_REPORT_MODULE_FILTER TW on TW.UserID=U.UserID where UserLogOn = @Username and CAST(U.UserPassword AS varbinary(255)) = CAST((CAST (@Password as nvarchar(255))) AS varbinary(255)) AND Deleted <> 1 And TW.ASSET=1 AND CTS_IT_Equipment=1", sqlConn);
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Parameters.Add(new SqlParameter("@Username", _Params_IsAuthenticated.UserLogOn));
                sqlCmd.Parameters.Add(new SqlParameter("@Password", _Params_IsAuthenticated.UserPassword));
                sqlConn.Open();
                SqlDataReader rdr = sqlCmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (rdr.Read())
                {
                    int UserId = rdr.GetInt32(rdr.GetOrdinal("UserId"));
                    int? SiteID = null;
                    try
                    {
                        SiteID = rdr.GetInt32(rdr.GetOrdinal("SiteID"));
                    }
                    catch
                    {
                    }
                    string UserLogOn = string.Empty;
                    UserLogOn = rdr.GetString(rdr.GetOrdinal("UserLogOn"));

                    _Params_IsAuthenticated.UserId = UserId;

                    System.Web.HttpContext.Current.Session.Add("UserLoggedIn", true);
                    System.Web.HttpContext.Current.Session.Add("UserId", UserId);
                    System.Web.HttpContext.Current.Session.Add("UserLogOn", UserLogOn);
                    System.Web.HttpContext.Current.Session.Add("SiteID", SiteID);


                }



            }
            catch (Exception ex)
            {

            }
            #endregion
            #region Return Section
            return _Params_IsAuthenticated;
            #endregion
        }
        #endregion

        #region IsLogedIn
        [OperationContract]
        [
        WebInvoke
        (
        Method = "GET",
        UriTemplate = "IsLogedIn?Input={Input}",
        BodyStyle = WebMessageBodyStyle.Bare,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )
        ]
        public Boolean IsLogedIn(string Input)
        {
            Boolean Result = false;

            if (System.Web.HttpContext.Current.Session["UserLoggedIn"] != null)
            {
                Result = true;//loged In
            }
            else
            {
                Result = false;//loged In }


            }
            return Result;
        }

        #endregion

        //CHeckout page 
        #region GetWBSElement
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetWBSElement?Input={Input}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public List<DropDownInfo> GetWBSElement(string Input)
        {
            #region Declaration And Initialization Section.

            List<DropDownInfo> L = new List<DropDownInfo>();

            #endregion
            #region Body Section.
            try
            {

                String IssueeID = (string)Input;

                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);

                string SQL = "SELECT '' AS WBS_ELEMENT, '' AS [DESCRIPTION], '' as FUNCTIONAL_AREA UNION SELECT WBS_ELEMENT, [DESCRIPTION],FUNCTIONAL_AREA FROM TBL_WBS_ELEMENT TWE " +
                             "JOIN Issuees I on I.IssueeCode = TWE.COST_CENTER " +
                             "WHERE I.IssueeID =  " + IssueeID +
                             " ORDER BY WBS_ELEMENT";
                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dtOutput = new DataTable();
                da.Fill(dtOutput);

                int nbr_row_total = (int)dtOutput.Rows.Count;
                for (int i = 0; i < nbr_row_total; i++)
                {
                    DropDownInfo fa = new DropDownInfo();
                    fa.RelatedItem = (string)dtOutput.Rows[i]["FUNCTIONAL_AREA"];
                    fa.stringID = (string)dtOutput.Rows[i]["WBS_ELEMENT"];
                    fa.Name = (string)dtOutput.Rows[i]["DESCRIPTION"];

                    L.Add(fa);
                }


            }
            catch (Exception ex)
            {

            }
            #endregion
            #region Return Section
            return L;
            #endregion
        }
        #endregion

        #region GetShipAddressName
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetShipAddressName?Input={Input}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public List<DropDownInfo> GetShipAddressName(string Input)
        {
            #region Declaration And Initialization Section.

            List<DropDownInfo> L = new List<DropDownInfo>();

            #endregion
            #region Body Section.
            try
            {

                String IssueeID = (string)Input;

                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);

                string SQL = "SELECT AddressID, AddressName " +
                             "  FROM Addresses " +
                             " WHERE AddressID IN (Select AddressID From IssueeShipToAddresses Where IssueeID = " + IssueeID + ") ORDER BY AddressName";
                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dtOutput = new DataTable();
                da.Fill(dtOutput);

                int nbr_row_total = (int)dtOutput.Rows.Count;
                for (int i = 0; i < nbr_row_total; i++)
                {
                    DropDownInfo fa = new DropDownInfo();
                    fa.ID = (int)dtOutput.Rows[i]["AddressID"];
                    fa.Name = (String)dtOutput.Rows[i]["AddressName"];

                    L.Add(fa);
                }



            }
            catch (Exception ex)
            {

            }
            #endregion
            #region Return Section
            return L;
            #endregion
        }
        #endregion

        #region CheckRecipientEmployee
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CheckRecipientEmployee?Input={Input}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public RecipientPersons CheckRecipientEmployee(string Input)
        {
            #region Declaration And Initialization Section.

            RecipientPersons p = new RecipientPersons();

            JavaScriptSerializer jscript = new JavaScriptSerializer();

            #endregion
            #region Body Section.
            try
            {

                p = jscript.Deserialize<RecipientPersons>(Input);

                //String EmpCode = (string)Input;

                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);

                string SQL = "select PersonID FROM personsview where PersonsPersonCode='" + p.PersonCode + "'  AND PersonsLastName='" + p.PersonsLastName + "'";
                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dtOutput = new DataTable();
                da.Fill(dtOutput);

                int nbr_row_total = (int)dtOutput.Rows.Count;
                if (nbr_row_total > 0)
                {
                    p.PersonID = (int)dtOutput.Rows[0]["PersonID"];
                    // p.PersonCode = (string)dtOutput.Rows[0]["PersonsPersonCode"];
                    // p.PersonsName = (string)dtOutput.Rows[0]["PersonsName"];
                    p.PersonsFound = 1;
                }
                else
                {
                    p.PersonsFound = 0;
                }

            }
            catch (Exception ex)
            {

            }
            #endregion
            #region Return Section
            return p;
            #endregion
        }
        #endregion

        #region GetEmailUser
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetEmailUser?Input={Input}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public RecipientPersons GetEmailUser(string Input)
        {
            #region Declaration And Initialization Section.

            RecipientPersons p = new RecipientPersons();

            #endregion
            #region Body Section.
            try
            {

                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);

                string SQL = "Select p.Email from persons p " +
                 "   left join users u on u.PersonsID=p.PersonID " +
                  "  where u.UserID=" + System.Web.HttpContext.Current.Session["UserId"] + " AND P.Deleted=0";

                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dtOutput = new DataTable();
                da.Fill(dtOutput);

                int nbr_row_total = (int)dtOutput.Rows.Count;
                if (nbr_row_total > 0)
                {
                    if (!string.IsNullOrEmpty(dtOutput.Rows[0]["Email"].ToString()))
                    {
                        p.PersonsEmail = (string)dtOutput.Rows[0]["Email"];
                        p.PersonsFound = 1;
                    }
                    else
                    {
                        p.PersonsEmail = string.Empty;
                        p.PersonsFound = 0;
                    }


                }


            }
            catch (Exception ex)
            {

            }
            #endregion
            #region Return Section
            return p;
            #endregion
        }
        #endregion

        #region SendEmailOrder
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "SendEmailOrder?Input={Input}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public string SendEmailOrder(string Input)
        {
            #region Declaration And Initialization Section.

            SendEmail s = new SendEmail();
            JavaScriptSerializer jscript = new JavaScriptSerializer();
            #endregion
            #region Body Section.
            try
            {
                s = jscript.Deserialize<SendEmail>(Input);

                SendNotification("CTS Equipment Catalogue Order Confirmation", s.BodyEmail.Replace("{number}", "#"), Email_to + "," + s.Email);//AVega@LASuperiorCourt.org
            }
            catch (Exception ex)
            {

            }
            #endregion
            #region Return Section
            return "Send";
            #endregion
        }
        #endregion

        //End Checkout oage

        #region RemoveItemFromCart
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RemoveItemFromCart?Input={Input}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public string RemoveItemFromCart(string Input)
        {
            try
            {

                String ItemID = Input;
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);

                SqlCommand SQL_CMD = new SqlCommand("", sqlConn);
                SQL_CMD.Connection.Open();


                string sql3 = "UPDATE Models SET ModelsUDValueCode2 = 'Surplus' WHERE ModelID = @ModelID AND ModelsUDValueCode2='InCart'";

                SQL_CMD.CommandText = sql3;
                SQL_CMD.CommandType = CommandType.Text;
                SQL_CMD.Parameters.Clear();
                SQL_CMD.Parameters.Add(new SqlParameter("ModelID", ItemID));
                SQL_CMD.ExecuteNonQuery();
                SQL_CMD.Connection.Close();
            }
            catch (Exception ex)
            {

            }
            #region Return Section
            return "succcess";
            #endregion
        }
        #endregion

        #region EmptyServerSession
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EmptyServerSession", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public string EmptyServerSession()
        {
            try
            {


                /*
                 String ModelIDS = "";
                List<int> ItemsInCart = (List<int>)System.Web.HttpContext.Current.Session["ItemInCart"];
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);

                for (int i = 0; i < ItemsInCart.Count; i++)
                {

                ModelIDS+= ItemsInCart[i]+",";



            }
                ModelIDS = ModelIDS.TrimEnd(',');

                string sql3 = "UPDATE Models SET ModelsUDValueCode2 = 'SurPlust' WHERE ModelID in (" +ModelIDS+ ")";
                    SqlCommand SQL_CMD = new SqlCommand(sql3, sqlConn);
                 */

                System.Web.HttpContext.Current.Session.Abandon();

            }
            catch (Exception ex)
            {

            }
            #region Return Section
            return "Session Empty";
            #endregion
        }
        #endregion

        //Submit Button In Checkout Page
        #region InsertToDataBase
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "InsertToDataBase", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public SubmitFormReturn InsertToDataBase(string Input)
        {
            SubmitFormReturn Result = new SubmitFormReturn();
            Result.ErrorString = string.Empty;

            #region Declaration And Initialization Section.
            bool isTaxable = true;
            JavaScriptSerializer jscript = new JavaScriptSerializer();

            // loop through and add sected Asset ModelID to SelectedItems hash table
            SubmitFormDetails p = jscript.Deserialize<SubmitFormDetails>(Input);

            string UnAvailableModels = string.Empty;
            string chosenModels = string.Empty;

            string personsId = string.Empty;//
            string DeliverTo = string.Empty;//

            List<string> oReturnItemsIDNotAvailable = new List<string>();
            Hashtable SelectedItems = new Hashtable();
            for (int i = 0; i < p.Items.Count; i++)
            {
                itemsLine s = p.Items[i];

                if (string.IsNullOrEmpty(chosenModels))
                    chosenModels = s.ItemsID.ToString();
                else
                    chosenModels += "," + s.ItemsID.ToString();


                if (!SelectedItems.Contains(s))
                {
                    SelectedItems.Add(i, s);

                }
            }

            int IssueeID = p.IssueeID;
            int? addressShipToID = p.ShipAddressID;

            decimal Subtotal = 0;
            decimal Total = 0;
            string IssueOrderCode = GetNextSONum();
            string WBS = string.Empty;
            if (!string.IsNullOrEmpty(p.WBSElement))
                WBS = p.WBSElement;


            string FAV = string.Empty;
            if (!string.IsNullOrEmpty(p.FunctionArea))
                FAV = p.FunctionArea;



            string WBSDesc = string.Empty;

            string FA = string.Empty;
            string FADesc = string.Empty;
            string Notes = p.SalesOrderNote;
            string PurchaseOrder = string.Empty;

            string PhoneNumber = string.Empty;
            if (!string.IsNullOrEmpty(p.PhoneNumber))
                PhoneNumber = p.PhoneNumber;

            string RequestorName = string.Empty;
            if (!string.IsNullOrEmpty(p.RequestorName))
                RequestorName = p.RequestorName;

            string RoomNumber = string.Empty;
            if (!string.IsNullOrEmpty(p.RoomNumber))
                RoomNumber = p.RoomNumber;

            string DeliveryDate = string.Empty;//IssueOrdersUDText6
            if (!string.IsNullOrEmpty(p.DeliveryDate))
                DeliveryDate = p.DeliveryDate;

            string IssueOrdersUDText7 = string.Empty;//IssueOrdersUDText6
            if (!string.IsNullOrEmpty(p.DeliveryDate))
                IssueOrdersUDText7 = p.IssueOrdersUDText7;


            #endregion

            bool OkToSubmit = true;
            using (TransactionScope scope = new TransactionScope())
            {
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                SqlCommand sqlCmd = null;
                int IssueOrderID = 0;
                #region revalidate Items

                string validationQ = " SELECT ModelCode, M.ModelID FROM Models M  "
                                + " LEFT JOIN IssueOrderLine OL ON M.MOdelID = OL.ModelID "
                                + " WHERE M.ModelsUDValueCode2 = 'Operations' AND M.ModelID IN (" + chosenModels + ")";

                sqlCmd = new SqlCommand(validationQ, sqlConn);
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Parameters.Clear();
                SqlDataAdapter vald = new SqlDataAdapter(sqlCmd);
                DataTable valdt = new DataTable();
                vald.Fill(valdt);
                if (valdt.Rows.Count > 0)
                {
                    foreach (DataRow r in valdt.Rows)
                    {
                        if (string.IsNullOrEmpty(UnAvailableModels))
                        {
                            UnAvailableModels = r["ModelCode"].ToString();
                            oReturnItemsIDNotAvailable.Add(r["ModelID"].ToString());
                        }
                        else
                        {
                            UnAvailableModels = UnAvailableModels + ", " + r["ModelCode"].ToString();
                            oReturnItemsIDNotAvailable.Add(r["ModelID"].ToString());
                        }
                    }
                    UnAvailableModels = "The following items are not available " + UnAvailableModels;
                    OkToSubmit = false;
                    Result.ErrorString = UnAvailableModels;
                    Result.ItemsIDNotAvailable = oReturnItemsIDNotAvailable;
                }

                #endregion

                if (OkToSubmit)
                {
                    #region Add IssueOrder

                    int? SiteID = null;

                    if (!string.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["SiteID"])))
                        SiteID = Convert.ToInt32(System.Web.HttpContext.Current.Session["SiteID"]);

                    if (!string.IsNullOrEmpty(WBS))
                        WBSDesc = GetWBSDescription(WBS, ConfigurationManager.AppSettings["STR_CONN"]);

                    if (!string.IsNullOrEmpty(FAV))
                    {
                        string FADescact = GetFADescription(FAV, ConfigurationManager.AppSettings["STR_CONN"]);
                        FA = GetFA_UDID(FAV, ConfigurationManager.AppSettings["STR_CONN"]);
                        if (!string.IsNullOrEmpty(FADescact))
                        {
                            FADesc = GetFADescription_UDID(FADescact, ConfigurationManager.AppSettings["STR_CONN"]);
                        }
                    }




                    #region SQL Querry
                    string IssueOrderQ = "INSERT INTO IssueOrders"
                  + " ([SiteID],[IssueeID],[ShipToAddressID],[OrderDate],[IssueOrderCode],[Subtotal],[Shipping],"
                  + " [Discount],[OtherAdjustment],[Tax],[Total],[Notes],[UDNumericIssueOrder1],"
                  + " [UDNumericIssueOrder2],[UDNumericIssueOrder3],[UDNumericIssueOrder4],[UDNumericIssueOrder5],"
                  + " [UDNumericIssueOrder6],[UDNumericIssueOrder7],[UDNumericIssueOrder8],[UDNumericIssueOrder9],"
                  + " [UDNumericIssueOrder10],[UDTextIssueOrder1],[UDTextIssueOrder2],[UDTextIssueOrder3],[UDTextIssueOrder4],"
                  + " [UDTextIssueOrder5],[UDTextIssueOrder6],[UDTextIssueOrder7],[UDTextIssueOrder8],[UDTextIssueOrder9],"
                  + " [UDTextIssueOrder10],UDValueIDIssueOrder9,UDValueIDIssueOrder10,AddedBy,UpdatedBy, AssetOrder, ModelBasedOrder)"
                  + " VALUES "
                  + " (@SiteID,@IssueeID,@ShipToAddressID,@OrderDate,@IssueOrderCode,@Subtotal,@Shipping,"
                  + " @Discount,@OtherAdjustment,@Tax,@Total,@Notes,@UDNumericIssueOrder1,"
                  + " @UDNumericIssueOrder2,@UDNumericIssueOrder3,@UDNumericIssueOrder4,@UDNumericIssueOrder5,"
                  + " @UDNumericIssueOrder6,@UDNumericIssueOrder7,@UDNumericIssueOrder8,@UDNumericIssueOrder9,"
                  + " @UDNumericIssueOrder10,@UDTextIssueOrder1,@UDTextIssueOrder2,@UDTextIssueOrder3,@UDTextIssueOrder4,"
                  + " @UDTextIssueOrder5,@UDTextIssueOrder6,@UDTextIssueOrder7,@UDTextIssueOrder8,@UDTextIssueOrder9,"
                  + " @UDTextIssueOrder10,@UDValueIDIssueOrder9,@UDValueIDIssueOrder10,@AddedBy,@UpdatedBy,1,0)";

                    #endregion

                    sqlCmd = new SqlCommand(IssueOrderQ, sqlConn);
                    sqlCmd.CommandType = CommandType.Text;
                    sqlCmd.Parameters.Clear();
                    sqlCmd.CommandTimeout = 60;

                    #region Parameters

                    sqlCmd.Parameters.Add(new SqlParameter("@SiteID", SqlDbType.Int));
                    if (SiteID.HasValue)
                        sqlCmd.Parameters["@SiteID"].Value = SiteID.Value;
                    else
                        sqlCmd.Parameters["@SiteID"].Value = DBNull.Value;


                    sqlCmd.Parameters.Add(new SqlParameter("@ShipToAddressID", SqlDbType.Int));
                    if (addressShipToID.HasValue)
                        sqlCmd.Parameters["@ShipToAddressID"].Value = addressShipToID.Value;
                    else
                        sqlCmd.Parameters["@ShipToAddressID"].Value = DBNull.Value;

                    sqlCmd.Parameters.Add(new SqlParameter("@IssueOrderCode", SqlDbType.NVarChar));
                    sqlCmd.Parameters["@IssueOrderCode"].Value = IssueOrderCode;

                    sqlCmd.Parameters.Add(new SqlParameter("@IssueeID", SqlDbType.Int));
                    sqlCmd.Parameters["@IssueeID"].Value = IssueeID;

                    TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");

                    DateTime newPSTDateTime = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo);

                    sqlCmd.Parameters.Add(new SqlParameter("@OrderDate", SqlDbType.DateTime));
                    sqlCmd.Parameters["@OrderDate"].Value = newPSTDateTime;//Convert.ToDateTime(lblOrderDateVal.Text);


                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@Subtotal", SqlDbType.Decimal));
                        sqlCmd.Parameters["@Subtotal"].Value = Subtotal;
                    }
                    catch
                    {
                        sqlCmd.Parameters["@Subtotal"].Value = DBNull.Value;
                    }

                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@Shipping", SqlDbType.Decimal));
                        //sqlCmd.Parameters["@Shipping"].Value = Convert.ToDecimal(TextBox2.Text);
                        sqlCmd.Parameters["@Shipping"].Value = DBNull.Value;
                    }
                    catch
                    {
                        sqlCmd.Parameters["@Shipping"].Value = DBNull.Value;
                    }

                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@Discount", SqlDbType.Decimal));
                        //sqlCmd.Parameters["@Discount"].Value = Convert.ToDecimal(TextBox3.Text);
                        sqlCmd.Parameters["@Discount"].Value = DBNull.Value;
                    }
                    catch
                    {
                        sqlCmd.Parameters["@Discount"].Value = DBNull.Value;
                    }

                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@OtherAdjustment", SqlDbType.Decimal));
                        //sqlCmd.Parameters["@OtherAdjustment"].Value = Convert.ToDecimal(TextBox4.Text);
                        sqlCmd.Parameters["@OtherAdjustment"].Value = DBNull.Value;
                    }
                    catch
                    {
                        sqlCmd.Parameters["@OtherAdjustment"].Value = DBNull.Value;
                    }

                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@Tax", SqlDbType.Decimal));
                        //sqlCmd.Parameters["@Tax"].Value = Convert.ToDecimal(TextBox5.Text);
                        sqlCmd.Parameters["@Tax"].Value = DBNull.Value;
                    }
                    catch
                    {
                        sqlCmd.Parameters["@Tax"].Value = DBNull.Value;
                    }
                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@Total", SqlDbType.Decimal));
                        sqlCmd.Parameters["@Total"].Value = Convert.ToDecimal(Total);
                    }
                    catch
                    {
                        sqlCmd.Parameters["@Total"].Value = DBNull.Value;
                    }




                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@Notes", SqlDbType.NVarChar));
                        sqlCmd.Parameters["@Notes"].Value = Notes;
                    }
                    catch
                    {
                        sqlCmd.Parameters["@Notes"].Value = DBNull.Value;
                    }




                    // Text UD fields
                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@UDTextIssueOrder1", SqlDbType.NVarChar));
                        sqlCmd.Parameters["@UDTextIssueOrder1"].Value = PurchaseOrder;
                    }
                    catch
                    {
                        sqlCmd.Parameters["@UDTextIssueOrder1"].Value = DBNull.Value;
                    }
                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@UDTextIssueOrder2", SqlDbType.NVarChar));
                        sqlCmd.Parameters["@UDTextIssueOrder2"].Value = Convert.ToString(PhoneNumber);
                    }
                    catch
                    {
                        sqlCmd.Parameters["@UDTextIssueOrder2"].Value = DBNull.Value;
                    }
                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@UDTextIssueOrder3", SqlDbType.NVarChar));
                        sqlCmd.Parameters["@UDTextIssueOrder3"].Value = RequestorName;
                    }
                    catch
                    {
                        sqlCmd.Parameters["@UDTextIssueOrder3"].Value = DBNull.Value;
                    }
                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@UDTextIssueOrder4", SqlDbType.NVarChar));
                        sqlCmd.Parameters["@UDTextIssueOrder4"].Value = RoomNumber;
                    }
                    catch
                    {
                        sqlCmd.Parameters["@UDTextIssueOrder4"].Value = DBNull.Value;
                    }


                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@UDTextIssueOrder6", SqlDbType.NVarChar));
                        sqlCmd.Parameters["@UDTextIssueOrder6"].Value = DeliveryDate;
                    }
                    catch
                    {
                        sqlCmd.Parameters["@UDTextIssueOrder6"].Value = DBNull.Value;
                    }

                    sqlCmd.Parameters.Add(new SqlParameter("@UDNumericIssueOrder1", SqlDbType.Decimal));
                    sqlCmd.Parameters.Add(new SqlParameter("@UDNumericIssueOrder2", SqlDbType.Decimal));
                    sqlCmd.Parameters.Add(new SqlParameter("@UDNumericIssueOrder3", SqlDbType.Decimal));
                    sqlCmd.Parameters.Add(new SqlParameter("@UDNumericIssueOrder4", SqlDbType.Decimal));
                    sqlCmd.Parameters.Add(new SqlParameter("@UDNumericIssueOrder5", SqlDbType.Decimal));
                    sqlCmd.Parameters.Add(new SqlParameter("@UDNumericIssueOrder6", SqlDbType.Decimal));
                    sqlCmd.Parameters.Add(new SqlParameter("@UDNumericIssueOrder7", SqlDbType.Decimal));
                    sqlCmd.Parameters.Add(new SqlParameter("@UDNumericIssueOrder8", SqlDbType.Decimal));
                    sqlCmd.Parameters.Add(new SqlParameter("@UDNumericIssueOrder9", SqlDbType.Decimal));
                    sqlCmd.Parameters.Add(new SqlParameter("@UDNumericIssueOrder10", SqlDbType.Decimal));

                    sqlCmd.Parameters["@UDNumericIssueOrder1"].Value = DBNull.Value;
                    sqlCmd.Parameters["@UDNumericIssueOrder2"].Value = DBNull.Value;
                    sqlCmd.Parameters["@UDNumericIssueOrder3"].Value = DBNull.Value;
                    sqlCmd.Parameters["@UDNumericIssueOrder4"].Value = DBNull.Value;
                    sqlCmd.Parameters["@UDNumericIssueOrder5"].Value = DBNull.Value;
                    sqlCmd.Parameters["@UDNumericIssueOrder6"].Value = DBNull.Value;
                    sqlCmd.Parameters["@UDNumericIssueOrder7"].Value = DBNull.Value;
                    sqlCmd.Parameters["@UDNumericIssueOrder8"].Value = DBNull.Value;
                    sqlCmd.Parameters["@UDNumericIssueOrder9"].Value = DBNull.Value;
                    sqlCmd.Parameters["@UDNumericIssueOrder10"].Value = DBNull.Value;



                    sqlCmd.Parameters.Add(new SqlParameter("@UDTextIssueOrder5", SqlDbType.NVarChar));
                    sqlCmd.Parameters["@UDTextIssueOrder5"].Value = DBNull.Value;
                    sqlCmd.Parameters.Add(new SqlParameter("@UDTextIssueOrder7", SqlDbType.NVarChar));
                    sqlCmd.Parameters["@UDTextIssueOrder7"].Value = IssueOrdersUDText7;
                    sqlCmd.Parameters.Add(new SqlParameter("@UDTextIssueOrder8", SqlDbType.NVarChar));
                    sqlCmd.Parameters["@UDTextIssueOrder8"].Value = DBNull.Value;
                    sqlCmd.Parameters.Add(new SqlParameter("@UDTextIssueOrder9", SqlDbType.NVarChar));
                    sqlCmd.Parameters["@UDTextIssueOrder9"].Value = DBNull.Value;
                    sqlCmd.Parameters.Add(new SqlParameter("@UDTextIssueOrder10", SqlDbType.NVarChar));
                    sqlCmd.Parameters["@UDTextIssueOrder10"].Value = DBNull.Value;



                    try
                    {

                        sqlCmd.Parameters.Add(new SqlParameter("@UDValueIDIssueOrder9", SqlDbType.Int));
                        sqlCmd.Parameters["@UDValueIDIssueOrder9"].Value = Convert.ToInt32(FA);
                    }
                    catch
                    {
                        sqlCmd.Parameters["@UDValueIDIssueOrder9"].Value = DBNull.Value;
                    }
                    try
                    {
                        sqlCmd.Parameters.Add(new SqlParameter("@UDValueIDIssueOrder10", SqlDbType.Int));
                        sqlCmd.Parameters["@UDValueIDIssueOrder10"].Value = Convert.ToInt32(FADesc);
                    }
                    catch
                    {
                        sqlCmd.Parameters["@UDValueIDIssueOrder10"].Value = DBNull.Value;
                    }

                    sqlCmd.Parameters.Add(new SqlParameter("@AddedBy", SqlDbType.NVarChar));
                    sqlCmd.Parameters["@AddedBy"].Value = System.Web.HttpContext.Current.Session["UserLogOn"].ToString();
                    sqlCmd.Parameters.Add(new SqlParameter("@UpdatedBy", SqlDbType.NVarChar));
                    sqlCmd.Parameters["@UpdatedBy"].Value = System.Web.HttpContext.Current.Session["UserLogOn"].ToString();
                    #endregion

                    try
                    {
                        sqlCmd.Connection.Open();
                        sqlCmd.ExecuteNonQuery();
                        sqlCmd.Connection.Close();
                    }
                    catch (Exception e)
                    {
                        OkToSubmit = false;
                        sqlCmd.Connection.Close();
                        //SendNotification("Urgent LASC Catalogue BUG, please escalate to Carlos or Greg. Error Insert Into   IssueOrders", "Error Insert Into  IssueOrders  IssueOrderCode:" + IssueOrderCode + " UserId: " + System.Web.HttpContext.Current.Session["UserId"] + ", IssueOrderCode: " + IssueOrderCode + ",  Error Was:" + e.Message, "support@asapsystems.com");

                        //Second try : try again inserting it 
                        try
                        {
                            System.Threading.Thread.Sleep(1000);
                            sqlCmd.Connection.Open();
                            sqlCmd.ExecuteNonQuery();
                            sqlCmd.Connection.Close();

                            OkToSubmit = true;
                        }
                        catch (Exception e2)
                        {
                            OkToSubmit = false;
                            sqlCmd.Connection.Close();
                            //SendNotification("Urgent LASC Catalogue BUG, please escalate to Carlos or Greg. Error Insert Into   IssueOrders", "In Second try : Error Insert Into  IssueOrders  IssueOrderCode:" + IssueOrderCode + " UserId: " + System.Web.HttpContext.Current.Session["UserId"] + ", IssueOrderCode: " + IssueOrderCode + ",  Error Was:" + e2.Message, "support@asapsystems.com");

                            //3rd try : try again inserting it 
                            try
                            {
                                System.Threading.Thread.Sleep(1000);
                                sqlCmd.Connection.Open();
                                sqlCmd.ExecuteNonQuery();
                                sqlCmd.Connection.Close();

                                OkToSubmit = true;
                            }
                            catch (Exception e3)
                            {
                                OkToSubmit = false;
                                sqlCmd.Connection.Close();
                                //SendNotification("Urgent LASC Catalogue BUG, please escalate to Carlos or Greg. Error Insert Into   IssueOrders", "In 3rd try : Error Insert Into  IssueOrders  IssueOrderCode:" + IssueOrderCode + " UserId: " + System.Web.HttpContext.Current.Session["UserId"] + ", IssueOrderCode: " + IssueOrderCode + ",  Error Was:" + e3.Message, "support@asapsystems.com");


                                Result.ErrorString = "request timed out please check your connection or try again later";
                            }

                        }


                    }


                    #endregion

                    #region Add LineItems
                    if (OkToSubmit)
                    {
                        string GetIssueOrderIDQ = "SELECT IssueOrderID FROM IssueOrders WHERE IssueOrderCode = @IssueOrderCode";

                        sqlCmd = new SqlCommand(GetIssueOrderIDQ, sqlConn);
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.Clear();
                        sqlCmd.Parameters.Add(new SqlParameter("@IssueOrderCode", SqlDbType.NVarChar));
                        sqlCmd.Parameters["@IssueOrderCode"].Value = IssueOrderCode;

                        SqlDataAdapter d = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        d.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            Result.IssueOrderCode = IssueOrderCode;
                            IssueOrderID = Convert.ToInt32(dt.Rows[0][0].ToString());

                            // selected items Key ModelID, value Qty ordered|Price

                            int lineNum = 1;
                            // loop through chosen items and add into issueOrderline table 
                            for (int i = 0; i < SelectedItems.Count; i++)
                            {
                                string IssueOrderLineQ = "INSERT INTO [IssueOrderLine]"
                                    + " ([IssueOrderID],[LineNumber],[ModelID],[UnitPrice],[Units],[UOMID],"
                                    + " [ExtendedPrice],[Taxable],[IssuedQuantity],[OnBackorder],IssueOrderLineUDTxt1,"
                                    + " [AddedBy],[UpdatedBy])"
                                    + " Select "
                                    + IssueOrderID.ToString() + "," + lineNum.ToString() + ",ModelID,ISNULL(CostMostRecent,0),1,1,"
                                    + "ISNULL(CostMostRecent,0),1,0,1,@IssueOrderLineUDTxt1,'"
                                    + System.Web.HttpContext.Current.Session["UserLogOn"].ToString() + "','" + System.Web.HttpContext.Current.Session["UserLogOn"].ToString()
                                    + "' FROM Models WHERE ModelID = " + ((itemsLine)SelectedItems[i]).ItemsID.ToString()
                                    + " update models set ModelsUDValueCode2='ONORDER' where ModelID =" + ((itemsLine)SelectedItems[i]).ItemsID.ToString()
                                    + " update items set PersonID=@PersonID where ModelID =" + ((itemsLine)SelectedItems[i]).ItemsID.ToString();
                                ;
                                try
                                {
                                    sqlCmd = new SqlCommand(IssueOrderLineQ, sqlConn);
                                    sqlCmd.CommandType = CommandType.Text;
                                    sqlCmd.Parameters.Add(new SqlParameter("@PersonID", DBNull.Value));
                                    sqlCmd.Parameters.Add(new SqlParameter("@IssueOrderLineUDTxt1", DBNull.Value));

                                    sqlCmd.Connection.Open();
                                    sqlCmd.ExecuteNonQuery();
                                    sqlCmd.Connection.Close();
                                }
                                catch (Exception exi)
                                {
                                    OkToSubmit = false;
                                    //SendNotification("Urgent LASC Catalogue BUG, please escalate to Carlos or Greg. Error Insert into IssueOrderLine", "Error when inserting to IssueOrderLine ItemId:" + ((itemsLine)SelectedItems[i]).ItemsID.ToString() + " UserId: " + System.Web.HttpContext.Current.Session["UserId"] + ", IssueOrderCode: " + IssueOrderCode + ", Error was:" + exi.Message, "support@asapsystems.com");
                                    sqlCmd.Connection.Close();

                                    //Second try : try again inserting it 
                                    try
                                    {
                                        System.Threading.Thread.Sleep(1000);
                                        sqlCmd.Connection.Open();
                                        sqlCmd.ExecuteNonQuery();
                                        sqlCmd.Connection.Close();

                                        OkToSubmit = true;
                                    }
                                    catch (Exception e2)
                                    {
                                        OkToSubmit = false;
                                        sqlCmd.Connection.Close();
                                        //SendNotification("Urgent LASC Catalogue BUG, please escalate to Carlos or Greg. Error Insert into IssueOrderLine", "Second try Error when inserting to IssueOrderLine ItemId:" + ((itemsLine)SelectedItems[i]).ItemsID.ToString() + " UserId: " + System.Web.HttpContext.Current.Session["UserId"] + ", IssueOrderCode: " + IssueOrderCode + ", Error was:" + e2.Message, "support@asapsystems.com");
                                        //3rd try : try again inserting it 
                                        try
                                        {
                                            System.Threading.Thread.Sleep(1000);
                                            sqlCmd.Connection.Open();
                                            sqlCmd.ExecuteNonQuery();
                                            sqlCmd.Connection.Close();

                                            OkToSubmit = true;
                                        }
                                        catch (Exception e3)
                                        {
                                            OkToSubmit = false;
                                            sqlCmd.Connection.Close();
                                            //SendNotification("Urgent LASC Catalogue BUG, please escalate to Carlos or Greg. Error Insert into IssueOrderLine", "In 3rd try : Error when inserting to IssueOrderLine ItemId:" + ((itemsLine)SelectedItems[i]).ItemsID.ToString() + " UserId: " + System.Web.HttpContext.Current.Session["UserId"] + ", IssueOrderCode: " + IssueOrderCode + ", Error was:" + e3.Message, "support@asapsystems.com");

                                            Result.ErrorString = "request timed out please check your connection or try again later";
                                        }

                                    }
                                }

                                lineNum++;
                            }
                        }
                    }
                    #endregion

                    #region Update Total info
                    if (OkToSubmit)
                    {

                        string Q = "UPDATE IssueOrders set Subtotal = X.Subtotal, Total = (X.Subtotal + X.TAX), Tax = X.TAX, Shipping = 0, Discount = 0, OtherAdjustment = 0"
                                    + " FROM IssueOrders I "
                                    + " JOIN "
                                    + " (select " + IssueOrderID.ToString() + " AS IssueOrderID, ISNULL(Subtotal,0) AS Subtotal, (ISNULL(Taxed,0)*ISNULL(tax,0)) AS TAX"
                                    + " FROM"
                                    + " (select sum(ExtendedPrice) as Subtotal,"
                                    + " (select sum(ExtendedPrice) from IssueOrderLIne where Taxable = 0 AND issueOrderID = " + IssueOrderID.ToString() + " )as UnTaxed, "
                                    + " (select sum(ExtendedPrice) from IssueOrderLIne where Taxable = 1 AND issueOrderID = " + IssueOrderID.ToString() + ") as taxed,"
                                    + " (select (Convert(money,ISNULL(StringValue,0))/100) as taxRate from PassportMiscValues where ObjectName = 'SOTAXPercent') as tax"
                                    + " from IssueOrderLIne where issueOrderID = " + IssueOrderID.ToString() + " ) AS b) as X ON X.IssueOrderID = I.IssueOrderID";

                        sqlCmd = new SqlCommand(Q, sqlConn);
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.Parameters.Clear();

                        try
                        {
                            sqlCmd.Connection.Open();
                            sqlCmd.ExecuteNonQuery();
                            sqlCmd.Connection.Close();
                        }
                        catch (Exception exl)
                        {
                            OkToSubmit = false;
                            sqlCmd.Connection.Close();
                            //SendNotification("Urgent LASC Catalogue BUG, please escalate to Carlos or Greg. Error UPdate  IssueOrders", "Error when UPdate  IssueOrders  issueOrderID:" + IssueOrderID.ToString() + " UserId: " + System.Web.HttpContext.Current.Session["UserId"] + ", IssueOrderCode: " + IssueOrderCode + ", Error Was:" + exl.Message, "support@asapsystems.com");

                            //Second try : try again inserting it 
                            try
                            {
                                System.Threading.Thread.Sleep(1000);
                                sqlCmd.Connection.Open();
                                sqlCmd.ExecuteNonQuery();
                                sqlCmd.Connection.Close();

                                OkToSubmit = true;
                            }
                            catch (Exception e2)
                            {
                                OkToSubmit = false;
                                sqlCmd.Connection.Close();
                                //SendNotification("Urgent LASC Catalogue BUG, please escalate to Carlos or Greg. Error UPdate  IssueOrders", "Error when UPdate  IssueOrders  issueOrderID:" + IssueOrderID.ToString() + " UserId: " + System.Web.HttpContext.Current.Session["UserId"] + ", IssueOrderCode: " + IssueOrderCode + ", Error Was:" + e2.Message, "support@asapsystems.com");

                                //3rd try : try again inserting it 
                                try
                                {
                                    System.Threading.Thread.Sleep(1000);
                                    sqlCmd.Connection.Open();
                                    sqlCmd.ExecuteNonQuery();
                                    sqlCmd.Connection.Close();

                                    OkToSubmit = true;
                                }
                                catch (Exception e3)
                                {
                                    OkToSubmit = false;
                                    sqlCmd.Connection.Close();
                                    //SendNotification("Urgent LASC Catalogue BUG, please escalate to Carlos or Greg. Error UPdate  IssueOrders", "Error when UPdate  IssueOrders  issueOrderID:" + IssueOrderID.ToString() + " UserId: " + System.Web.HttpContext.Current.Session["UserId"] + ", IssueOrderCode: " + IssueOrderCode + ", Error Was:" + e3.Message, "support@asapsystems.com");


                                    OkToSubmit = false;
                                    Result.ErrorString = "request timed out please check your connection or try again later";
                                }

                            }
                        }
                        finally
                        {
                            sqlCmd.Connection.Close();
                        }
                    }

                    #endregion
                    if (OkToSubmit)
                        scope.Complete();
                    else
                        scope.Dispose();
                }
                else
                    scope.Dispose();
            }

            if (OkToSubmit)
            {
                string email_text = "UserId " + System.Web.HttpContext.Current.Session["UserId"] + ", Your order number is: " + Result.IssueOrderCode;
                email_text = email_text + " Items: " + chosenModels;

                SendNotification("LASC order", email_text, "tadra@asapsystems.com");
            }
            else
            {
                //SendNotification("Urgent LASC Catalogue BUG, please escalate to Carlos or Greg. Item Not availabel ", "UserId: " + System.Web.HttpContext.Current.Session["UserId"] + ", IssueOrderCode: " + IssueOrderCode + ", Item Not availabel :" + Result.ErrorString, "support@asapsystems.com");
            }

            return Result;
        }


        protected string GetNextSONum()
        {
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
            SqlCommand sqlCmd = null;
            sqlCmd = new SqlCommand("SELECT * FROM BarcodeGenerators WHERE ConfigurationID IN (SELECT ConfigurationID FROM Configurations"
                + " WHERE ObjectName = 'IssueOrdersIssueOrderCodeAS')", sqlConn);
            sqlCmd.CommandType = CommandType.Text;
            SqlDataAdapter d = new SqlDataAdapter(sqlCmd);
            DataTable dt = new DataTable();
            d.Fill(dt);
            string PreText = dt.Rows[0]["Prefix"].ToString();
            string Suffix = dt.Rows[0]["Suffix"].ToString();
            int IncrementingNumber = Convert.ToInt32(dt.Rows[0]["IncrementingNumber"]);
            int Format = Convert.ToInt32(dt.Rows[0]["Format"]);
            bool getCode = true;
            string Code = string.Empty;
            while (getCode)
            {
                Code = "IT-" + PreText + IncrementingNumber.ToString().PadLeft(
                            Format, '0') + Suffix;
                if (!DoesSOExist(Code))
                {
                    if (IncrementIncrementingNumber(IncrementingNumber, "IssueOrdersIssueOrderCodeAS"))
                    {
                        getCode = false;
                    }

                }
                else
                {
                    IncrementingNumber++;
                }

            }
            return Code;
        }

        protected bool IncrementIncrementingNumber(int IncrementingNumber, string Configuration)
        {
            bool success = true;
            IncrementingNumber++;
            SqlCommand sqlCmd = null;
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
            try
            {
                sqlCmd = new SqlCommand("Update BarcodeGenerators SET IncrementingNumber = " + IncrementingNumber.ToString() + " WHERE ConfigurationID IN (SELECT ConfigurationID FROM Configurations"
                    + " WHERE ObjectName = '" + Configuration + "')", sqlConn);
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Connection.Open();
                sqlCmd.ExecuteNonQuery();
                sqlCmd.Connection.Close();
            }
            catch
            {
                sqlCmd.Connection.Close();
                success = false;
            }
            finally
            {
                sqlCmd.Connection.Close();
            }
            return success;
        }


        protected bool DoesSOExist(string IssueOrderCode)
        {
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
            SqlCommand sqlCmd = null;
            sqlCmd = new SqlCommand("SELECT * FROM IssueOrders WHERE IssueOrderCode = @IssueOrderCode AND Deleted <> 1", sqlConn);
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.Parameters.Add(new SqlParameter("@IssueOrderCode", SqlDbType.NVarChar));
            sqlCmd.Parameters["@IssueOrderCode"].Value = IssueOrderCode;

            SqlDataAdapter d = new SqlDataAdapter(sqlCmd);
            DataTable dt = new DataTable();
            d.Fill(dt);
            return dt.Rows.Count > 0;
        }

        public static string GetWBSFuncArea(string WBS_ELEMENT, string strconstring)
        {
            string res = string.Empty;

            SqlConnection objcon;
            SqlCommand objcom;
            SqlDataReader objreader;
            string SqlQry = "SELECT TOP(1)[FUNCTIONAL_AREA] FROM TBL_WBS_ELEMENT WHERE WBS_ELEMENT=@WBS_ELEMENT";
            objcon = new SqlConnection(strconstring);
            objcon.Open();
            objcom = new SqlCommand(SqlQry, objcon);
            objcom.Parameters.Add(new SqlParameter("WBS_ELEMENT", WBS_ELEMENT));
            objreader = objcom.ExecuteReader();
            if (objreader.Read())
            {
                res = objreader["FUNCTIONAL_AREA"].ToString();
            }
            objreader.Dispose();
            objcon.Close();
            objcom.Dispose();//Dispose the Command Object
            objcon.Dispose();//Dispose the Connection Object

            return res;
        }

        public static string GetWBSDescription(string WBS_ELEMENT, string strconstring)
        {
            string res = string.Empty;

            SqlConnection objcon;
            SqlCommand objcom;
            SqlDataReader objreader;
            string SqlQry = "SELECT TOP(1)[DESCRIPTION] FROM TBL_WBS_ELEMENT WHERE WBS_ELEMENT=@WBS_ELEMENT";
            objcon = new SqlConnection(strconstring);
            objcon.Open();
            objcom = new SqlCommand(SqlQry, objcon);
            objcom.Parameters.Add(new SqlParameter("WBS_ELEMENT", WBS_ELEMENT));
            objreader = objcom.ExecuteReader();
            if (objreader.Read())
            {
                res = objreader["DESCRIPTION"].ToString();
            }
            objreader.Dispose();
            objcon.Close();
            objcom.Dispose();//Dispose the Command Object
            objcon.Dispose();//Dispose the Connection Object

            return res;
        }

        public static string GetFADescription(string FUNCTIONAL_AREA, string strconstring)
        {
            string res = string.Empty;

            SqlConnection objcon;
            SqlCommand objcom;
            SqlDataReader objreader;
            string SqlQry = "SELECT TOP(1)[DESCRIPTION] FROM TBL_FUNCTIONAL_AREA WHERE FUNCTIONAL_AREA=@FUNCTIONAL_AREA";
            objcon = new SqlConnection(strconstring);
            objcon.Open();
            objcom = new SqlCommand(SqlQry, objcon);
            objcom.Parameters.Add(new SqlParameter("FUNCTIONAL_AREA", FUNCTIONAL_AREA));
            objreader = objcom.ExecuteReader();
            if (objreader.Read())
            {
                res = objreader["DESCRIPTION"].ToString();
            }
            objreader.Dispose();
            objcon.Close();
            objcom.Dispose();//Dispose the Command Object
            objcon.Dispose();//Dispose the Connection Object

            return res;
        }

        public static string GetCostCenterDescription(string IssueeID, string strconstring)
        {
            string res = string.Empty;

            SqlConnection objcon;
            SqlCommand objcom;
            SqlDataReader objreader;
            string SqlQry = "SELECT TOP(1)[IssueeName] FROM Issuees WHERE IssueeID=@IssueeID";
            objcon = new SqlConnection(strconstring);
            objcon.Open();
            objcom = new SqlCommand(SqlQry, objcon);
            objcom.Parameters.Add(new SqlParameter("IssueeID", IssueeID));
            objreader = objcom.ExecuteReader();
            if (objreader.Read())
            {
                res = objreader["IssueeName"].ToString();
            }
            objreader.Dispose();
            objcon.Close();
            objcom.Dispose();//Dispose the Command Object
            objcon.Dispose();//Dispose the Connection Object

            return res;
        }

        public static string GetFA_UDID(string FUNCTIONAL_AREA, string strconstring)
        {
            string res = string.Empty;

            SqlConnection objcon;
            SqlCommand objcom;
            SqlDataReader objreader;
            string SqlQry = "SELECT TOP(1)UDValueID FROM UDValues WHERE UDValueCode=@FUNCTIONAL_AREA AND TableName = 'IssueOrders' AND FieldName = 'UDValueIDIssueOrder9'";
            objcon = new SqlConnection(strconstring);
            objcon.Open();
            objcom = new SqlCommand(SqlQry, objcon);
            objcom.Parameters.Add(new SqlParameter("FUNCTIONAL_AREA", FUNCTIONAL_AREA));
            objreader = objcom.ExecuteReader();
            if (objreader.Read())
            {
                res = objreader["UDValueID"].ToString();
            }
            objreader.Dispose();
            objcon.Close();
            objcom.Dispose();//Dispose the Command Object
            objcon.Dispose();//Dispose the Connection Object
            return res;

        }

        public static string GetFADescription_UDID(string FUNCTIONAL_AREA_DESC, string strconstring)
        {
            string res = string.Empty;

            SqlConnection objcon;
            SqlCommand objcom;
            SqlDataReader objreader;
            string SqlQry = "SELECT TOP(1)UDValueID FROM UDValues WHERE UDValueCode=@FUNCTIONAL_AREA_DESC AND TableName = 'IssueOrders' AND FieldName = 'UDValueIDIssueOrder10'";
            objcon = new SqlConnection(strconstring);
            objcon.Open();
            objcom = new SqlCommand(SqlQry, objcon);
            objcom.Parameters.Add(new SqlParameter("FUNCTIONAL_AREA_DESC", FUNCTIONAL_AREA_DESC));
            objreader = objcom.ExecuteReader();
            if (objreader.Read())
            {
                res = objreader["UDValueID"].ToString();
            }
            objreader.Dispose();
            objcon.Close();
            objcom.Dispose();//Dispose the Command Object
            objcon.Dispose();//Dispose the Connection Object
            return res;

        }

        #endregion




        //SendFormEmail 
        #region SendFormEmail
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "SendFormEmail?Input={Input}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public register SendFormEmail(string Input)
        {
            #region Declaration And Initialization Section.

            register _Params_Form = new register();
            JavaScriptSerializer jscript = new JavaScriptSerializer();

            #endregion
            #region Body Section.
            try
            {

                _Params_Form = jscript.Deserialize<register>(Input);


                string BodyText = "Register form Info:<br />";

                BodyText = BodyText + "Name: " + _Params_Form.name + "<br />";
                BodyText = BodyText + "Last Name: " + _Params_Form.last_name + "<br />";
                BodyText = BodyText + "Company: " + _Params_Form.company + "<br />";
                BodyText = BodyText + "Password: " + _Params_Form.password + "<br />";

                SendNotification("Register Form Info", BodyText, Email_to);//AVega@LASuperiorCourt.org
            }
            catch (Exception ex)
            {

            }
            #endregion
            #region Return Section
            return _Params_Form;
            #endregion
        }
        #endregion


        //Send email function 
        private void SendNotification(string SubjectLine, string BodyText, string EmailAddress)
        {
            if (EmailAddress != "")
            {
                string PersonEmail = EmailAddress;
                System.Net.Mail.SmtpClient smtp = default(System.Net.Mail.SmtpClient);
                System.Net.Mail.MailMessage item = default(System.Net.Mail.MailMessage);
                item = new System.Net.Mail.MailMessage("noreply@asapsystems.com", EmailAddress);
                string body = "";
                //Create SMTP Client
                smtp = new System.Net.Mail.SmtpClient();
                smtp.Host = "outlook.office365.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential("noreply@asapsystems.com", "Welcome123@");
                body = BodyText;

                item.Subject = SubjectLine;
                item.IsBodyHtml = true;
                item.Body = body.ToString();
                smtp.Send(item);
            }
        }




    }



}


#region product_Details
public partial class product_Details
{

    public string ModelName { get; set; }
    public string MainModelCode { get; set; }
    public string AbstractID { get; set; }
    public List<string> ProductImage { get; set; }
    public int Quantity { get; set; }

    //right bos info
    public string FURNITURE_TYPE { get; set; }
    public string COLOR { get; set; }
    public string FEATURES { get; set; }
    public string MATERIALS { get; set; }
    public string ADDITIONAL_INFO { get; set; }
    public string SPECIAL_FEATURES { get; set; }
    public string SPECIFICATIONS { get; set; }
    public string ModelCode { get; set; }
    public string assetType { get; set; }
    public List<MatchedModel> MatchedModel { get; set; }

    public string Conditions { get; set; }

    public string CategoryCode { get; set; }

    public string Model { get; set; }

    public string SerialNum { get; set; }

    public string MAC { get; set; }

    public string VideoCard { get; set; }

    public string RAM { get; set; }

    public string BIOS { get; set; }

    public string IP { get; set; }

    public string HDSize { get; set; }

    public string CPUspeed { get; set; }

    public string CPU { get; set; }

    public string resolution { get; set; }

    public string ScreenSize { get; set; }

    public string SoftwareVersion { get; set; }

    public string SoftwareLicenses { get; set; }
}
#endregion

#region Mateched Items

public partial class MatchedModel
{
    public string MatchedModelCode { get; set; }
    public string MatchedModelName { get; set; }
    public int MatchedModelModelID { get; set; }
    public string ModelImage { get; set; }
}

#endregion

#region Conditions

public partial class Conditions
{

    public int OneStar { get; set; }
    public int TwoStar { get; set; }
    public int ThreeStar { get; set; }
    public int FourStar { get; set; }
}

#endregion



#region Product
public partial class product
{
    public int id { get; set; }
    public string ModelNumber { get; set; }
    public String ModelName { get; set; }
    public int Quantity { get; set; }
    public String ProductImage { get; set; }
    public Decimal CostMostRecent { get; set; }
    public string ConditionCode { get; set; }
    public int ModelIDAbstract { get; set; }
    public string COLOR { get; set; }
    public string MATERIALS { get; set; }
    public string Subcategory { get; set; }
    public string ModelCode { get; set; }
    public string assetType { get; set; }
    public string CategoryCode { get; set; }
    public string FURNITURE_TYPE { get; set; }
    public string Model { get; set; }
    public string SerialNum { get; set; }
}
#endregion
#region order 
// johnny 6/13/2017
public partial class Order
{

    public int OrderId { get; set; }
    public string  orderNumber { get; set; }
    public string ModelNumber { get; set;}
    public string ModelName { get; set; }
    public double price { get; set; }
    public DateTime OrderDate {get;set;}
    public double TotalPrice {get;set;}
    public string AddressToShip {get;set;}
    public string issuecode { get; set; }
    public string issuename { get; set; }
}
#endregion
#region Page Products Info
public partial class PageProductInfo
{
    public int nbrPages { get; set; }
    public List<product> ListProduct { get; set; }

}
#endregion
// johnny 6/13/2017
#region Page orders Info
public partial class PageorderInfo
{
    public int nbrPages { get; set; }
    public List<Order> ListOrder { get; set; }
}


#endregion
#region Page orders Info request
public partial class Pageorderrequest
{
    public int index { get; set; }
    public string CustomLogin { get; set; }
    public string flag { get; set; }
}
public partial class Pageorderrequest1
{
    public int index { get; set; }
    public string CustomLogin { get; set; }
    public int orderid {get;set;}
}

#endregion

#region Page Info
public partial class PageInfo
{
    public int IndexPage { get; set; }
    public int CategorieId { get; set; }
    public int FurnitureId { get; set; }
    public string search_input { get; set; }

}
#endregion





#region User
public partial class User
{


    public int UserId { get; set; }
    public string UserLogOn { get; set; }
    public String UserPassword { get; set; }

}
#endregion


#region DropDownInfo
public partial class DropDownInfo
{
    public int ID { get; set; }
    public string stringID { get; set; }
    public string Name { get; set; }
    public string RelatedItem { get; set; }


}
#endregion

#region SubmitFormDetails
public partial class SubmitFormDetails
{
    public int IssueeID { get; set; }
    public string FunctionArea { get; set; }
    public string WBSElement { get; set; }
    public string RoomNumber { get; set; }
    public int? ShipAddressID { get; set; }
    public string DeliveryDate { get; set; }
    public string PhoneNumber { get; set; }
    public string RequestorName { get; set; }
    public string SalesOrderNote { get; set; }
    public List<itemsLine> Items { get; set; }
    public string IssueOrdersUDText7 { get; set; }
}

public partial class itemsLine
{
    public int ItemsID { get; set; }
    public int PersonsID { get; set; }
    public string DeliverTo { get; set; }
}

#endregion

#region SubmitFormReturn
public partial class SubmitFormReturn
{
    public string IssueOrderCode { get; set; }
    public string ErrorString { get; set; }
    public List<string> ItemsIDNotAvailable { get; set; }
}
#endregion


#region Register
public partial class register
{


    public string name { get; set; }
    public string last_name { get; set; }
    public string company { get; set; }
    public string email { get; set; }
    public string password { get; set; }

}
#endregion



#region RecipientPersons
public partial class RecipientPersons
{
    public int PersonID { get; set; }
    public string PersonCode { get; set; }
    public string PersonsFirstName { get; set; }
    public string PersonsLastName { get; set; }
    //public string PersonsName { get; set; }
    public string PersonsEmail { get; set; }
    public int PersonsFound { get; set; }
}
#endregion


#region SendEmail
public partial class SendEmail
{
    public string Email { get; set; }
    public string BodyEmail { get; set; }
}
#endregion


        #endregion
