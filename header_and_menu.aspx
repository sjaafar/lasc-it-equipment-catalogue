﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="header_and_menu.aspx.cs" Inherits="LASC_ShoppingCart2.header_and_menu" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link rel="stylesheet" href="./css/styles.css" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
      <!-- -----------------------------------------Start Header-------------------------------- -->
    <div id="header_section">

        <div id="logo_and_name">
            <div id="logo">
                <img src="img/logo.png" /></div>
            <div>
                <h1>Los Angeles Superior Court</h1>
                <h2>Furniture Catalogue</h2>
            </div>

        </div>

    </div>
    <!-- -----------------------------------------End  Header-------------------------------- -->

    <!-- -----------------------------------------Start Sub Header Menu-------------------------------- -->
    <div id="sub_header" class="full_width">

        <div class="page_width">
            <div id="top_menu">
                <ul  runat="server" id="menu_item">
                    
                </ul>
            </div>
            <div id="search_feild">Search
                <input type="text" name="search" /><span>GO</span></div>

        </div>

    </div>
    <!-- -----------------------------------------End  Sub Header Menu--------------------------------- -->
    <div class="divider_blue"></div>

    
    <!-- -----------------------------------------End Logo section and Top MEnu -------------------------------- -->
</body>
</html>
