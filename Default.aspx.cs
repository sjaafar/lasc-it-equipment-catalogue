﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

namespace LASC_ShoppingCart2
{
    public partial class Default : Base
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["CustomLogin"] == null)
            {
                if (Session["UserLoggedIn"] != null)
                {
                    if (Convert.ToBoolean(Session["UserLoggedIn"]) != true)
                        Response.Redirect("~/Login.aspx");
                }
                else
                    Response.Redirect("~/Login.aspx");
            }
            //Categories HOme Page
             try
            {
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                string SQL = "select CategoryID, CategoryCode,CAtegory from Categories "
                    + " where Category='asset' AND CategoryID IN (SELECT CategoryID from Models where ItemTypeID = 1" 
                    + " AND (AssetTypeID = 1 OR AssetTypeID = 2 OR AssetTypeID = 3 OR AssetTypeID = 5 OR AssetTypeID = 6 OR AssetTypeID = 18 OR AssetTypeID = 19 OR AssetTypeID = 20 )AND ModelsUDValueCode2 = 'Surplus')"
                    + " order by CategoryCode asc";

                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dtOutput = new DataTable();
                da.Fill(dtOutput);
                rpcategories.DataSource = dtOutput;
                rpcategories.DataBind();
            }
            catch (Exception ex)
            {

            }

        }

        public string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            string lower = s.ToLower();
            // Return char and concat substring.
            return char.ToUpper(lower[0]) + lower.Substring(1);
        }
        
    }
}