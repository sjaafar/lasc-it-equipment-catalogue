﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LASC_ShoppingCart2
{
    public partial class CustomLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["UserLoggedIn"] = null;
            Session["CustomLogin"] = true;
            Response.Redirect("~/Default.aspx");
        }
    }
}