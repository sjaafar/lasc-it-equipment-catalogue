﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/LASC.Master" CodeFile="checkout.aspx.cs" Inherits="LASC_ShoppingCart2.checkout" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
<div class="result_msg" style="display: none;"></div>
    <div class="popup_error_msg" style="display: none;">

        <%-----------------------------------------------------------------%>
        <div id="pop_up_error_box" >
        <div id="poped_up_error_content" >
             <div class="error_content_div">
                 <div class="text_error">
                     <div class="error_img"><img src="img/error_logo.png" title="error" /></div>
                     <div class="text_next_img"><H3 style="margin: 0;color:#282483;">THERE WAS AN ERROR WHILE PROCESSING YOUR ORDER!</H3>
                         <p  style="margin: 0;">THESE ITEMS ARE NO LONGER AVAILABLE AND WILL BE REMOVED FROM YOUR CART:</p>
                     </div>
                 </div>
                 <div class="error_list">
                     <ul class="error_ul_list">
                          
                     </ul>

                 </div>
             </div>
            <div class="ok_error_button has_background_button" style="width: 197px;height: 36px;background-position: 50% 50%;margin: auto;">OK</div>
                 </div>

                    </div>
        <%-----------------------------------------------------------------------%>

    </div>

    <div id="checkout_form">
        <h1 class="header_title_blue header_title_checkout_blue" id="header_title_checkout" >Asset Details</h1>



        <%-------------------------------------------------%>
        <div style="width: 100%;">

            <input type="button" value="Toggel View" id="view_button" style="display: none;" />
            <div id="item_selected_table" style="margin-top: 5px; margin-bottom: 25px;width: 1038px;
">

                <asp:Literal runat="server" ID="TaxRate"></asp:Literal>
                <table style="width: 100%;" id="item_selected_table" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="10%">Product</td>
                        <td width="15%">Subcategory</td>
                        <td width="25%">Model</td>
                        <!--<td width="25%">Description</td>-->
                        <td width="10%">Asset #</td>
                        <td width="5%">Condition</td>
                        <td style="min-width: 106px;">Check to Include
                            <br />
                            Uncheck to Remove
                        </td>

                        
                    </tr>

                </table>
                <div class="total_items">
                    <h3>Total Items </h3>
                    <input type="text" readonly value="0" id="total_items_input" /></div>
                <div class="div_update_button_with_img_loader">
                <div class="has_background_button" id="update_button">UPDATE</div>
                <div id="image_loader_update" style="display: none;"><img src="img/ajax_loader.gif"></div>
                </div>
            </div>


            <%-------------------------------------------------%>
        </div>


        <%---------------------Table Infoe----------------------------%>
        <div style="padding-top: 105px;">

                <h1 class="header_title_checkout_blue" id="h1" style="clear:both;" >Requisition Information</h1>

            <table class="toptablecart" style="width: 980px;">
                <tr>
                    <td>Order Date:
                    </td>
                    <td>
                        <asp:Label ID="lbDate" runat="server"></asp:Label>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Sales Order #:
                    </td>
                    <td>Will be generated upon submitting the order.
                    </td>
                    <td></td>
                </tr>


                <tr>
                    <td style="width: 195px;">Contact:</td>
                    <td valign="top">
                        <input type="text" class="txtinput" id="requestor_name" /></td>
                </tr>
                <tr>
                    <td  >Phone Number:</td>
                    <td ><input type="text" class="txtinput" id="phone_number" /></td>
                    <td></td>
                </tr>
                <%--<tr>
                    <td>Room Number:</td>
                    <td valign="top">
                        <input type="text" class="txtinput" id="room_number" /></td>
                    <td></td>
                </tr>--%>
                <%--<tr>
                <td  >Delivery Date:</td>
                <td valign="top"><input type="text" class="txtinput" id="delivery_date" /></td>
               
            </tr>--%>



                <%-----------------------------------------Cost Center--------------------------%>
                <tr>
                    <td>Cost Center:
                    </td>
                    <td>

                        <asp:Repeater ID="rpCostCenter" runat="server">
                            <HeaderTemplate>

                                <select class="dropctr addsapces" id="CostCenter" onchange="update_other_drop_down()">
                                    <option></option>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <option iname='<%# Eval("IssueeName").ToString()  %>' isssuecode="<%# Eval("IssueeCode").ToString()  %>" id='<%# Eval("IssueeID").ToString()  %>'><%# Eval("IssueeName").ToString()  %> (<%# Eval("IssueeCode").ToString()  %>)</option>
                            </ItemTemplate>
                            <FooterTemplate>
                                </select>
                            </FooterTemplate>
                        </asp:Repeater>


                        <input disabled="disabled" id="issuee_name" class="aspNetDisabled" />
                    </td>
                    <td>
                        <div class="clear_cost_center clear_button">Clear Cost Center</div>

                    </td>
                </tr>
                <%----------------------------------------End-Cost Center--------------------------%>
                <%-----------------------------------------Function Area --------------------------%>
                <tr>
                    <td>Functional Area:
                    </td>
                    <td>
                        <asp:Repeater ID="rpFunctional_Area" runat="server">
                            <HeaderTemplate>

                                <select class="dropctr addsapces" id="Functional_Area" onchange="$('#functional_area_name').val($('#Functional_Area').find(':selected').attr('id'))">
                                    <option></option>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <option fname='<%# Eval("DESCRIPTION").ToString()  %>' id='<%# Eval("FUNCTIONAL_AREA").ToString()  %>'><%# Eval("DESCRIPTION").ToString()  %> (<%# Eval("FUNCTIONAL_AREA").ToString()  %>)</option>
                            </ItemTemplate>
                            <FooterTemplate>
                                </select>
                            </FooterTemplate>
                        </asp:Repeater>
                        <input disabled="disabled" class="aspNetDisabled" id="functional_area_name" />
                    </td>
                    <td></td>
                </tr>
                <%-----------------------------------------End Function Area --------------------------%>

                <%-----------------------------------------Start WBS_Element--------------------------%>
                <%-- <tr>
                        <td>WBS Element: 
                        </td>
                        <td>
                            <select class="dropctr addsapcesWbs" id="WBS_Element"  >
                            </select>

                            <input disabled="disabled" class="aspNetDisabled" id="wbs_element_name" />
                        </td>
                          <td></td>
                    </tr>--%>
                <%-----------------------------------------End WBS_Element --------------------------%>

                <%-----------------------------------------Unit Code --------------------------%>
                <%--<tr>
                        <td>Unit Code: 
                        </td>
                        <td>
                            
                             <asp:Repeater ID="rpUniteCode" runat="server">
                                <HeaderTemplate>

                                    <select class="dropctr addsapces" id="unit_code"  onchange="update_unit_code_drop_down()">
                                        <option></option>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <option  iname='<%# Eval("IssueeName").ToString()  %>' issueecode="<%# Eval("IssueeCode").ToString()  %>" id='<%# Eval("IssueeID").ToString()  %>'><%# Eval("IssueeName").ToString()  %> +<%# Eval("IssueeCode").ToString()  %></option>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </select>
                                </FooterTemplate>
                            </asp:Repeater>


                            <input disabled="disabled" class="aspNetDisabled" id="unit_code_name" />
                           
                        </td>
                          <td> <div class="clear_unite_code clear_button">Clear Unit Code</div></td>
                    </tr>--%>

                <%-----------------------------------------End Unit Code--------------------------%>
                
                <tr>
                    <td>Deliver To Address Name: 
                    </td>
                    <td>
                        <select class="dropctr" id="ship_address_name">
                        </select>
                    </td>
                    <td></td>
                </tr>
                <tr>

                    <td>Special Instructions: 

                    </td>
                    <td>
                        <input class="txtinput"  id="sales_order_note" />
                    </td>
                    <td></td>
                </tr>
                <%-- <tr>

                        <td><asp:label id="IssueOrdersUDText7" runat="server" />:  </td>
                        <td>
                            <input class="txtinput" style="width: 431px;" id="IssueOrdersUDText7" />
                        </td>
                          <td></td>
                    </tr>--%>
            </table>
        </div>


        <%-------------------------End Table info------------------------%>






        <div style="clear: both; padding-top: 25px; padding-bottom: 25px;">
            <div class="has_background_button" id="submit_order"  onclick="submit_order()">Submit Order</div>
            
        </div>


    </div>


    <script type="text/javascript">

        var persons_code_c;
        var first_name_c;
        var last_name_c;
        var deliver_to_c;

        $(document).ready(function () {

            $('#image_loader').hide();
            $('#content').fadeIn();

            Build_checkout_table_detlais();


            //Update Button
            $('#update_button').click(function () {
                $('#item_selected_table  tr:not(:first)').each(function () {
                        if (!$(this).find(".add_asset_button").hasClass('add_asset_checked'))
                        { $(this).remove(); }
                });
             
            });


            $('#view_button').click(function () {
                $('#item_selected_table').slideToggle();
            });

            $(document).on('click', '.remove_item', function () {
                location.reload();
            });


            $('.clear_cost_center').click(function () {
                //if ($("#unit_code").val() != "") return;

                $(".dropctr").val("");
                $('.aspNetDisabled').val("");
                $('.dropctr').removeAttr("disabled");
                $('.dropctr').css("backgroundColor", "white");
            });

            $('.clear_unite_code').click(function () {
                //if ($("#CostCenter").val() != "") return;

                $(".dropctr").val("");
                $('.aspNetDisabled').val("");
                $('.dropctr').removeAttr("disabled");
                $('.dropctr').css("backgroundColor", "white");
            });

            $(".dropctr").change(function () {
                $(".dropctr").css("border", "1px solid #ccc");
            });

             
            $('.txtinput').keydown(function () {
                $(this).css("border", "1px solid #ccc");
            });

            $('.class_feild').on("keyup change", function () {
                $(this).css("border", "1px solid rgb(120,120,120)");
            });

            //Wbs changed
            $('#WBS_Element').on('change', function () {

                $('#wbs_element_name').val($('#WBS_Element').find(':selected').attr('value'));

                var related_item_id_wbs_fa = $('#WBS_Element').find(':selected').attr('related_item');

                $("#Functional_Area option[id='" + related_item_id_wbs_fa + "']").attr("selected", "selected");

                $('#functional_area_name').val($('#Functional_Area').find(':selected').attr('id'));

                $('#Functional_Area').attr("disabled", "disabled");
            });

            //Select drop Down
            var spacesToAdd = 3;
            var biggestLength = 0;
            $(".addsapces option").each(function () {
                var len = $(this).text().length;
                if (len > biggestLength) {
                    biggestLength = len;
                }
            });
            var padLength = biggestLength + spacesToAdd;
            $(".addsapces option").each(function () {
                var parts = $(this).text().split('+');
                var strLength = parts[0].length;
                for (var x = 0; x < (padLength - strLength) ; x++) {
                    parts[0] = parts[0] + ' ';
                }
                if (parts[1] || parts[1] == "") {
                    $(this).text(parts[0].replace(/ /g, '\u00a0') + '' + parts[1]).text;
                }

            });
            //End Select Drop Down

            //add button
            $('.add_asset_button').click(function () {

                if ($(this).hasClass('add_asset_checked')) {
                    $(this).removeClass('add_asset_checked');
                    RemoveItemFromSessionStorage($(this).attr('item_id'));
                    // var items = JSON.parse(sessionStorage.getItem('item_selected'));
                    var items = JSON.parse(sessionStorage.getItem('item_selected'));
                    $('#total_items_input').val(items.length);
                    if (items.length == 0) {
                        $('#header_title_checkout').html("NO Item Selected");
                        $('#submit_order').attr("disabled", "disabled");
                    }
                    else {
                        $('#header_title_checkout').html("Checkout");
                        $('#submit_order').removeAttr("disabled");
                    }


                }
                else {

                    $(this).addClass('add_asset_checked');
                    AddAsset_item($(this).attr('item_id'));
                }


            });

            //Check BOx Yes NO 
            $('.check_box_checkout').click(function () {

                var value_checked = $(this).attr("val");
                $('.check_box_checkout').removeClass("check_box_checkout_checked");
                $(this).addClass("check_box_checkout_checked");

                if (value_checked == "no") {
                    $('#IssueOrdersUDText7').attr("disabled", "disabled");
                    $('#IssueOrdersUDText7').val("");
                }
                else {
                    $('#IssueOrdersUDText7').removeAttr("disabled");
                }
            });
            //End Check BOx Yes And No 

            //Copy Paste features
            $('.copy_button').click(function () {

                persons_code_c = $(this).closest('tr').find('.rec_emp_feild').val();
                //first_name_c = $(this).closest('tr').find('.r_first_name').val();
                last_name_c = $(this).closest('tr').find('.r_last_name').val();
                deliver_to_c = $(this).closest('tr').find('.deliver_to').val();

                $('.past_button').css("opacity", "1");
                $(this).closest('tr').find('.past_button').css("opacity","0.4");

            });

            $('.past_button').click(function () {

                $(this).closest('tr').find('.rec_emp_feild').val(persons_code_c);
                //$(this).closest('tr').find('.r_first_name').val(first_name_c);
                $(this).closest('tr').find('.r_last_name').val(last_name_c);
                $(this).closest('tr').find('.deliver_to').val(deliver_to_c);
                 
                $(this).closest('tr').find('.class_feild').css("border", "1px solid rgb(120,120,120)");
                //check database 
                if (persons_code_c != "" && first_name_c != "" && last_name_c != "")
                    CheckRecipientEmployee(this, persons_code_c, last_name_c);
            });

            $('.rec_emp_feild, .r_first_name, .r_last_name').on("change", function () {
                var persons_code = $(this).closest('tr').find('.rec_emp_feild').val();
                //var first_name = $(this).closest('tr').find('.r_first_name').val();
                var last_name = $(this).closest('tr').find('.r_last_name').val();

                if (persons_code != "" && last_name != "")
                     CheckRecipientEmployee(this, persons_code, last_name);
            });
             

            $('.class_feild').on("keyup change", function () {
                CheckIfFeildTrue(this);
            });

            //End Copy Paste features

            //Only Number Input 
            $('.only_number').keypress(function (e) {
                var a = [];
                var k = e.which;

                for (i = 65; i < 123; i++) {
                    if (i != 92)
                        a.push(i);
                }
                a.push(36);
                a.push(38);
                a.push(33);
                a.push(34);
                a.push(39);
                a.push(61);
                a.push(63);
                a.push(42);
                a.push(44);
                a.push(58);
                a.push(59);
                a.push(64);
                a.push(126);

                if ((a.indexOf(k) >= 0))
                    e.preventDefault();


            });

        });

        //drop down updat

        function Build_checkout_table_detlais() {
            var SubTotal = 0;


            $('#item_selected_table').find("tr:gt(0)").remove();

            var items = JSON.parse(sessionStorage.getItem('item_selected'));

            if (items.length == 0) {

                $('.header_title_blue').html("Sorry NO Item Selected");
            }

            for (var i = 0 ; i < items.length; i++) {

                SubTotal = parseFloat(SubTotal) + parseFloat(items[i].CostMostRecent);

                //Condition Start
                var CondVl = items[i].ConditionCode;
                var condition_stars = "";
                if (CondVl == '1' || CondVl == '2' || CondVl == '3' || CondVl == '4') {

                    if (CondVl == '1')
                        condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";

                    if (CondVl == '2')
                        condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";

                    if (CondVl == '3')
                        condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class\"></div></div>";

                    if (CondVl == '4')
                        condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class star_on\"></div></div>";
                }
                else
                    condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class\"></div> <div class=\"star_class\"></div> <div class=\"star_class\"></div><div class=\"star_class\"></div></div>";
                //Conditions End
                if (items[i].ProductImage != "none") {
                    var imageForProduct = '<img src="data:image/png;base64,' + items[i].ProductImage + '"  >';
                } else {
                    var imageForProduct = 'Image Not Available';
                }
                var structure = "";
                var structure = structure + "<tr class=\"item_tr\">";
                var structure = structure + "<td class=\"GoToProduct\"><a href=\"product_details.aspx?id=" + items[i].id + "\"><div class='categories_items_image '>" + imageForProduct + "</div></a></td>";
                var structure = structure + "<td class=\"GoToProduct\"> " + items[i].Subcategory + " </td>";
                var structure = structure + "<td class=\"GoToProduct\"> " + items[i].Model + " </td>";
                var structure = structure + " <td class=\"GoToProduct\">" + items[i].ModelNumber + "</td>";
                var structure = structure + " <td class=\"GoToProduct\">" + condition_stars + "</td>";
                if (CheckIfItemInSessionStorage(items[i].id))
                    structure = structure + "<td>  <div item_id=\"" + items[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
                else
                    structure = structure + "<td>  <div item_id=\"" + items[i].id + "\" class=\"add_asset_button\"></div> </td>";
                var structure = structure + "    </tr>";

                    $('#item_selected_table tr:last').after(structure);

            }//end for


            //Total Items Value
            $('#total_items_input').val(items.length);
            if (items.length == 0)
                $('#submit_order').attr("disabled", "disabled");
            else
                $('#submit_order').removeAttr("disabled");

            $.fn.placeholder();

            

        }

        function update_other_drop_down() {
            $('.aspNetDisabled').val('');
            $("#Functional_Area").val('');

            var IssueeID = $('#CostCenter').find(':selected').attr('id');
            var issuee_name = $('#CostCenter').find(':selected').attr('iname');
            var issuee_code = $('#CostCenter').find(':selected').attr('isssuecode');
            $('#issuee_name').val(issuee_code);

            $('#unit_code').attr("disabled", "disabled");
            $('#unit_code').css({ 'backgroundColor': 'whitesmoke' });

            // GetWBSElement(IssueeID);
            GetShipAddressName(IssueeID);

            $('#functional_area_name').val('');
            $('#Functional_Area').removeAttr("disabled");


        }

        function update_unit_code_drop_down() {

            var IssueeID = $('#unit_code').find(':selected').attr('id');
            var issuee_name = $('#unit_code').find(':selected').attr('iname');
            var issueecode = $('#unit_code').find(':selected').attr('issueecode');
            $('#unit_code_name').val(issueecode);

            $('#CostCenter').attr("disabled", "disabled");
            $('#CostCenter').css({ 'backgroundColor': 'whitesmoke' });

            GetWBSElement(IssueeID);
            GetShipAddressName(IssueeID);


        }


        function isBlank(str) {
            return (!str || /^\s*$/.test(str));
        }

        function submit_order() {


            var validate = true;
            if ($("#CostCenter").val() == "") {
                $("#CostCenter").css("border", "1px solid red");
                // $("#unit_code").css("border", "1px solid red");
                validate = false;
            }


            
            if (isBlank($("#Functional_Area").val())) {
                $("#Functional_Area").css("border", "1px solid red");
                validate = false;
            }

            if ($("#phone_number").val() == "") {
                $("#phone_number").css("border", "1px solid red");
                validate = false;
            }

            if ($("#requestor_name").val() == "") {
                $("#requestor_name").css("border", "1px solid red");
                validate = false;
            }

            if (validate) {

                var items = JSON.parse(sessionStorage.getItem('item_selected'));
                var Items = [];

                for (var i = 0; i < items.length; i++) {

                    Items[i] = new Object;
                    Items[i].ItemsID = items[i].id;
                    Items[i].PersonsID = 0;
                    Items[i].DeliverTo = 0;
                }

                var CostCenter = $("#CostCenter").find(':selected').attr('id');
                var FunctionArea = $("#Functional_Area").find(':selected').attr('id');
                //var UnitCode = $("#unit_code").find(':selected').attr('id');
                var UnitCode = "";
                //var WBSElement = $("#WBS_Element").find(':selected').attr('value');
                var WBSElement = "";

                var ShipAddressID = $("#ship_address_name").find(':selected').attr('value');
                var sales_order_note = $('#sales_order_note').val();
                var phone_number = $('#phone_number').val();


               // var room_number = $('#room_number').val();
                var room_number = "";

                //var delivery_date = $('#delivery_date').val();
                var delivery_date = "";
                var requestor_name = $('#requestor_name').val();
                var IssueOrdersUDText7 = $('#IssueOrdersUDText7').val();



                var Submit_details = new Object();
                Submit_details.Items = new Object();

                Submit_details.Items = Items;

                if (CostCenter != null) {
                    Submit_details.IssueeID = CostCenter;
                }
                else {
                    Submit_details.IssueeID = UnitCode;
                }


                Submit_details.FunctionArea = FunctionArea;
                Submit_details.WBSElement = WBSElement;
                Submit_details.ShipAddressID = ShipAddressID;
                Submit_details.SalesOrderNote = sales_order_note;
                Submit_details.PhoneNumber = phone_number;
                Submit_details.RoomNumber = room_number
                Submit_details.DeliveryDate = delivery_date;
                Submit_details.RequestorName = requestor_name;
                Submit_details.IssueOrdersUDText7 = IssueOrdersUDText7;

                var _Params = JSON.stringify(Submit_details);
               
                InsertToDataBase(_Params);


            }
        }
    </script>

</asp:Content>

