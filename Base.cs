﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LASC_ShoppingCart2
{
    public class Base : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["CustomLogin"] == null)
            {
                if (Session["UserLoggedIn"] != null)
                {
                    if (Convert.ToBoolean(Session["UserLoggedIn"]) != true)
                        Response.Redirect("~/Login.aspx");
                }
                else
                    Response.Redirect("~/Login.aspx");
            }

        }
    }
}