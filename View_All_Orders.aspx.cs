﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LASC_CTS_EquipmentCatalogue
{
    public partial class View_All_Orders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["CustomLogin"] == null)
            {
                if (Session["UserLoggedIn"] != null)
                {
                    if (Convert.ToBoolean(Session["UserLoggedIn"]) != true)
                        Response.Redirect("~/Login.aspx");
                }
                else
                    Response.Redirect("~/Login.aspx");
            }

        
    }
}
    }