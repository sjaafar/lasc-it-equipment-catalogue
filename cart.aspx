﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/LASC.Master" CodeFile="cart.aspx.cs" Inherits="LASC_ShoppingCart2.cart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
          <div id="page_path">home</div>
        <h1 class="header_title_blue"></h1>

        <div id="product_content"  >
            <table style="width: 100%;" id="category_table"   cellpadding="0" cellspacing="0">
                <tr>
                    <td>Product</td>
                    <td>Color</td>
                    <td>Materials</td>
                    <td>Description</td>
                    <td>Asset #</td>
                    <td>Condition</td>
                    <td>Add To Cart</td>
                </tr>
                 
            </table>
      </div>
      
    
     <!-- ---------------- End Content--------- -->

     <!-- ---------------- Button--------- -->
 
     <!-- ---------------- End Button--------- -->
    
      <script type="text/javascript">

          $(document).ready(function () {
              $('#image_loader').hide();
              $('#content').fadeIn();
              var items = JSON.parse(sessionStorage.getItem('item_selected'));
              for (var i = 0 ; i < items.length; i++) {
                  //Condition Start
                  var CondVl = items[i].ConditionCode;
                  var condition_stars = "";
                  if (CondVl == '1' || CondVl == '2' || CondVl == '3' || CondVl == '4') {

                      if (CondVl == '1')
                          condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";

                      if (CondVl == '2')
                          condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";

                      if (CondVl == '3')
                          condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class\"></div></div>";

                      if (CondVl == '4')
                          condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class star_on\"></div></div>";

                  }
                  else
                      condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class\"></div> <div class=\"star_class\"></div> <div class=\"star_class\"></div><div class=\"star_class\"></div></div>";

                  //Conditions End

                  if (items[i].ProductImage != "none") {
                      var imageForProduct = '<img src="data:image/png;base64,' + items[i].ProductImage + '"  >';
                  } else {
                      var imageForProduct = 'Image Not Available';
                  }
                  var structure = "";
                  structure = structure + "<tr class=\"item_tr\">";
                  structure = structure + "<td ><a href=\"product_details.aspx?id=" + items[i].ModelIDAbstract + "\"><div class=\"check_items_image\" >" + imageForProduct + "</div></a></td>";

                  structure = structure + " <td><a href=\"product_details.aspx?id=" + items[i].ModelIDAbstract + "\">" + items[i].ModelName + "</a></td>";
                  structure = structure + "<td> " + condition_stars + " </td>";
                  if (CheckIfItemInSessionStorage(items[i].id))
                      structure = structure + "<td>  <div item_id=\"" + items[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
                  else
                      structure = structure + "<td>  <div item_id=\"" + items[i].id + "\" class=\"add_asset_button\"></div> </td>";
                  structure = structure + "    </tr>";

                  $('#item_selected_table tr:last').after(structure);

              }//end for

          });
  </script>
</asp:Content>
