﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="register_page.aspx.cs" Inherits="LASC_ShoppingCart2.register_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="./css/styles.css" type="text/css">
     
</head>
<body>
    <form id="form1" runat="server">
         <div>
        
    </div>
    </form>

    <!-- -----------------------------------------Start Header-------------------------------- -->
    <div id="header_section">

        <div id="logo_and_name">
            <div id="logo">
               <a href="default.aspx"> <img src="img/logo.png" /></a></div>
            <div>
                <h1>Los Angeles Superior Court</h1>
                <h2>IT Equipment Catalogue</h2>
            </div>

        </div>

    </div>
    <!-- -----------------------------------------End  Header-------------------------------- -->

    
    <!-- ---------------- Start Content--------- -->
     <div id="image_loader" style="display:none;"><img src="img/ajax_loader.gif" /></div>
    <div id="content" class="page_width">
         <h1 style="text-align:center;">Register</h1>
     
        <div id="form_content_all">
         
              <form id="form" method="post">
    <div id="form_content_new">
      <table style="width: 100%;" border="0">
        <tbody>
          <tr>
            <td colspan="2"><h1>Create an account</h1></td>
          </tr>
          <tr>
            <td><p class="lable_input">First Name</p></td>
            <td><p class="lable_input">Last Name</p></td>
          </tr>
          <tr>
            <td><input id="first_name" class="input_new_forms" type="text" name="first_name" /></td>
            <td><input id="last_name" class="input_new_forms" type="text" name="last_name" /></td>
          </tr>
          <tr>
            <td><p class="lable_input">Email</p></td>
            <td><p class="lable_input">Company Name</p></td>
          </tr>
          <tr>
            <td><input id="email_adress" class="input_new_forms" type="text" name="email_adress" /></td>
            <td><input id="company_name" class="input_new_forms" type="text" name="company_name" /></td>
          </tr>
          <tr>
            <td><p class="lable_input">Create password</p></td>
            <td><p class="lable_input">Reenter password</p></td>
          </tr>
          <tr>
            <td><input id="password" class="input_new_forms" type="password" name="Create password" /></td>
            <td><input id="confpassword" class="input_new_forms" type="password" name="Reenter password" /></td>
          </tr>
          
          
          <tr >
            <td style="height: 50px;padding-top: 20px;" colspan="2"><div class="has_background_button" onclick="SendDemoForm(); return false;" style="margin: auto;"> Create  account</div></td>
          </tr>
          <tr>
            <td colspan="2"> </td>
          </tr>
        </tbody>
      </table>
    </div>
  </form>

            </div>
      
    </div>
     <!-- ---------------- End Content--------- -->
 
     <!-- -----------------------------------------Start Footer --------------------------------- -->
    <div id="footer_content">

            <div id="footer_text" >
                <p>A Service of LASC Materials Management Administration
                    <br />
                    (213) 974-9994
                </p>

            </div>

    </div>
    <!-- -----------------------------------------End Footer --------------------------------- -->
      
    <script type='text/javascript' src='js/jquery.min.js'></script>
    <script type='text/javascript' src='js/jscript.js'></script>
      <script type="text/javascript">// <![CDATA[
          $(document).ready(function () {

              $('#form_content_new').first().find("input").keydown(function () {
                  $(this).css('border', '1px solid #dae0e4');
                  $(this).css({ 'color': 'black', 'font-weight': '500' });
              });





          });



          function SendDemoForm() {
              var name = $("#first_name").val();
              var last_name = $("#last_name").val();
              var company = $("#company_name").val();
              var email = $("#email_adress").val();
              var password = $("#password").val();
              var confpassword = $("#confpassword").val();
             
              var errorFlag = false;


              if ($("#first_name").val() == '') {
                  errorFlag = true;
                  $("#first_name").css('border', '1px solid red');
              }
              else {
                  $("#first_name").removeAttr("style");
              }


              if ($("#last_name").val() == '') {
                  errorFlag = true;
                  $("#last_name").css('border', '1px solid red');
              }
              else {
                  $("#last_name").removeAttr("style");
              }

              if (company == '') {
                  errorFlag = true;
                  $("#company_name").css('border', '1px solid red');
              }
              else {
                  $("#company_name").removeAttr("style");
              }

              if ((email == 'Email') || (email == '') || (!validateEmail(email))) {
                  errorFlag = true;
                  $("#email_adress").css('border', '1px solid red');
              }
              else {
                  $("#email_adress").removeAttr("style");
              }

              if (password == '') {
                  errorFlag = true;
                  $("#password").css('border', '1px solid red');
              }
              else {
                  $("#password").removeAttr("style");
              }

              if (confpassword == '') {
                  errorFlag = true;
                  $("#confpassword").css('border', '1px solid red');
              }
              else {
                  $("#confpassword").removeAttr("style");
              }

              if (confpassword != password || confpassword == '') {
                  errorFlag = true;
                  $("#confpassword").css('border', '1px solid red');
                  $("#password").css('border', '1px solid red');
              }
              else {
                  $("#password").removeAttr("style");
                  $("#confpassword").removeAttr("style");
              }


              if (!errorFlag) {
                  $(".submit_button_new_form").attr('disabled', 'disabled').css('opacity', 0.5);
                  $(".submit_button_new_form").removeAttr('onclick');
                  $(".submit_button_new_form").text('please_wait_image');

                  var Submit_details = new Object;
                  Submit_details.name = name;
                  Submit_details.last_name = last_name;
                  Submit_details.company = company;
                  Submit_details.email = email;
                  Submit_details.password = password;

                  var _Params = JSON.stringify(Submit_details);

                 
                  send_form_email(_Params);
              }
          }

          function validateEmail(elementValue) {
              var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

              return emailPattern.test(elementValue);
          }


          //phone funcion validation
          function phonenumber(phoneno) {
              var error = "";
              var stripped = phoneno.replace(/[\(\)\.\-\ ]/g, '');

              if (phoneno == "") {
                  error = "You didn't enter a phone number.\n";

                  return false;
              }
              if (isNaN(parseInt(stripped))) {
                  error = "The phone number contains illegal characters.\n";

                  return false;
              }
              return true;
          }



          // ON LOAD Event
          $(document).ready(function () {
              // hiding left <DIV>
              $(".inPageBoard").css("display", "none");
              $('#phone_number').keypress(function (e) {
                  var a = [];
                  var k = e.which;

                  for (i = 65; i < 123; i++) {
                      if (i != 92)
                          a.push(i);
                  }
                  a.push(36);
                  a.push(38);
                  a.push(33);
                  a.push(34);
                  a.push(39);
                  a.push(61);
                  a.push(63);
                  a.push(42);
                  a.push(44);
                  a.push(58);
                  a.push(59);
                  a.push(64);
                  a.push(126);

                  if ((a.indexOf(k) >= 0))
                      e.preventDefault();


              });
          });
          </script> 

</body>
</html>
