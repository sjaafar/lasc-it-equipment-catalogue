﻿<%@ WebHandler Language="C#" Class="ShowImage" %>

using System;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public class ShowImage : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.QueryString["autoId"] == null) return;
        string q = "SELECT ModelImage From Images WHERE ImageID = @autoId";



        string connStr = ConfigurationManager.AppSettings["STR_CONN"];
        string pictureId = context.Request.QueryString["autoId"];

            
        using (SqlConnection conn = new SqlConnection(connStr))
        {
            using (SqlCommand cmd = new SqlCommand(q, conn))
            {
                cmd.Parameters.Add(new SqlParameter("@autoId", pictureId));
                conn.Open();
                using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    reader.Read();
                    try
                    {
                        Object o = reader[reader.GetOrdinal("ModelImage")];
                        if (o != null)
                        {
                            Byte[] image = (Byte[])o;
                            if (image != null)
                                context.Response.BinaryWrite(image);
                        }
                    }
                    catch
                    {
                    }
                    reader.Close();
                }
            }
        }
    }

    public bool IsReusable
    {
        get
        {
            return true;
        }
    }

}