﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LASC.Master" AutoEventWireup="true" CodeBehind="View_All_Orders.aspx.cs" Inherits="LASC_CTS_EquipmentCatalogue.View_All_Orders" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="SalesOrderLines">
    <div style="text-align:left; width: 100%">
    <ul style="list-style: none; font-size: 15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; line-height: 2; text-decoration:none;">
        <li style="padding-bottom: 0.5em;"><a id="open1" href="View_All_Orders.aspx?Flag=open" style="text-decoration:none;color:rgb(226,88,15);"> Open Sales Orders</a></li>
        <li><a id="close" href="View_All_Orders.aspx?Flag=closed" style="text-decoration:none;color:rgb(226,88,15);"> Closed Sales Orders</a></li>
    </ul>
    </div> 

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


     <script type='text/javascript' src='js/jscript.js'></script>
     <script type="text/javascript">

         var CustomLogin = '<%= Session["UserLogOn"].ToString()%>';
         var flag = '<%= Request.QueryString["Flag"]%>';

    </script>
   
    

    
    <div>
        <table style="width: 100%;" id="item_selected_table" cellpadding="0" cellspacing="0">
            <tr>
                    <td width="10%">Order Number</td>
                    <td width="15%">Order Date</td>
                    <td width="25%" >Cost Center</td>
                    <td width="25%" >Adress Ship</td>
                    <!--<td width="25%">Description</td>-->
                    <%--<td width="10%">Model photo #</td>--%>
                    <td width="5%" >Total Price</td>
                    <td width="15%">Details</td>
            </tr>

        </table>

         <div  class="arrows" style="margin:auto;clear:both;height: 91px;padding-left: 45px;padding-top: 28px;">
        <table style="margin: 30px auto 30px;">
            <tr>
                <td> <div class="prev_button" style="cursor:pointer;float:left;"><img src="img/flesh_left.png" style="width: 70px;" /></div></td>
                <td><div id="nbr_pages"></div></td>
                <td> <div class="next_button" style="cursor:pointer;float:right;"><img src="img/flesh_right.png"  style="width: 70px;" /></div></td>
            </tr>
             
             
        </table>
    </div>
      

             
    <script type="text/javascript">
        $(document).ready(function () {
            if (flag == "open") {
                hrefid = document.getElementById("open1");
                hrefid.style.fontWeight = 'bold';
            }
            else {
                hrefid = document.getElementById("close");
                hrefid.style.fontWeight = 'bold';
            }
            Getorder_neededGeneral(0, CustomLogin,flag);// get the first page
            $('.next_button').click(function () {
                index_page = parseInt($('.selected_page').html());
                Getorder_neededGeneral(index_page, CustomLogin,flag);//Next Page 

            });

            $('.prev_button').click(function () {
                index_page = parseInt($('.selected_page').html());
                if (index_page <= 1) return;
                index_page = parseInt($('.selected_page').html()) - 2;

                Getorder_neededGeneral(index_page, CustomLogin,flag);//Next Page 
            });
            //End HOme PAge Code

        });
        </script>


</asp:Content>
