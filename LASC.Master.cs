﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LASC_ShoppingCart2
{
    public partial class LASC : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                string SQL = "select CategoryID, CategoryCode,CAtegory from Categories "
                    + " where Category='asset' AND CategoryID IN (SELECT CategoryID from Models where ItemTypeID = 1" 
                    + " AND (AssetTypeID = 1 OR AssetTypeID = 2 OR AssetTypeID = 3 OR AssetTypeID = 5 OR AssetTypeID = 6 OR AssetTypeID = 18 OR AssetTypeID = 19 OR AssetTypeID = 20 )AND ModelsUDValueCode2 = 'Surplus')"
                    + " order by CategoryCode asc";
                SqlDataAdapter da = new SqlDataAdapter(SQL, sqlConn);
                DataTable dtOutput = new DataTable();
                da.Fill(dtOutput);
                StringBuilder sb = new StringBuilder();
                foreach (DataRow drOutput in dtOutput.Rows)
                {
                    
                    //Eval("CategoryID").ToString().Equals(getHandler("id")) ? "class=selected_cat" : "";
                    string selected_class="";
                    string selected_li_class="";
                    if(drOutput["CategoryID"].ToString().Equals(getHandler("id")))
                    {   selected_class= "class=selected_cat" ;
                        selected_li_class="class=selected_li_class";
                    }
                    // johnny 6/16/2017
                    sb.Append("<li " + selected_li_class + " ><a href=\"subcategories.aspx?id=" + drOutput["CategoryID"] + "\"  " + selected_class + "  > " + UppercaseFirst(drOutput["CategoryCode"].ToString()) + " </a>");


                    string SQL2 = "select  distinct FURNITURE_TYPE, TF.FURNITURE_TYPE_ID ";
	                          SQL2=SQL2+" from TBL_FURNITURE_TYPE TF ";
	                          SQL2=SQL2+" left join TBL_MODEL_EXTENDED TM on TM.FURNITURE_TYPE_ID = TF.FURNITURE_TYPE_ID ";
	                          SQL2=SQL2+" left join Models M  on M.ModelIDAbstract=TM.Model_ID ";
                              SQL2 = SQL2 + " left join Categories C on C.CategoryID=M.CategoryID ";
                              SQL2 = SQL2 + " where C.categoryID =" + drOutput["CategoryID"];
                              SQL2 = SQL2 + " and  M.ModelsUDValueCode2 like 'Surplus' AND  M.deleted = 0   AND";
                              SQL2 = SQL2 + "  ItemTypeId=1 and ModelIDAbstract = M.ModelIDAbstract  order by FURNITURE_TYPE asc";
                          

                    SqlDataAdapter da2 = new SqlDataAdapter(SQL2, sqlConn);
                    DataTable dtOutput2 = new DataTable();
                    da2.Fill(dtOutput2);

                    if ((int)dtOutput2.Rows.Count != 0)
                    {
                        sb.Append("<ul class=\"sub_sub_menu\">");
                        foreach (DataRow drOutput2 in dtOutput2.Rows)
                        {
                            selected_class = "";
                            selected_li_class="";
                            if (drOutput2["FURNITURE_TYPE_ID"].ToString().Equals(getHandler("fid")))
                            {       selected_class = "class=selected_cat";
                                    selected_li_class = "class=selected_li_class";
                            }
                            sb.Append("<li " + selected_li_class + " ><a href=\"categories.aspx?id=" + drOutput["CategoryID"] + "&fid=" + drOutput2["FURNITURE_TYPE_ID"] + "\"  " + selected_class + " > " + UppercaseFirst(drOutput2["FURNITURE_TYPE"].ToString()) + " </a></li>");
                        }
                        sb.Append("</ul>");
                    }

                    sb.Append("</li>");
                }

                menu_categories.InnerHtml = sb.ToString();

            }
            catch (Exception ex)
            {

            }
 

        }


        //Transfer first case to upper
        public string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            string lower = s.ToLower();
            // Return char and concat substring.
            return char.ToUpper(lower[0]) + lower.Substring(1);
        }

        public string getHandler(string var)
        {
            return Server.UrlEncode(Request.QueryString[var]);
        }
    }
}