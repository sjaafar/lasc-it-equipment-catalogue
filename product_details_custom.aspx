﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/LASC.Master" CodeFile="product_details_custom.aspx.cs" Inherits="LASC_ShoppingCart2.product_details_custom" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .auto-style1 {
            height: 22px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="page_path">
    <span id="page_pathserver" runat="server" >
  
    </span>
		<h3 id="ModelnumberCode" style="font-size: 22px;margin-bottom: 2px;">Asset Number: </h3>
        </div>

    <!-- ------------------------------ Left Side------------------------------------>
    <div class="left_div_description">
        <!-- ------------------------------ Slider------------------------------------>
        <div id="slider_div">

            <div id="image_bg">
                <img   class="big_image" /></div>

            <div id="dots_div">
            </div>

            <div id="small_images">
            </div>
        </div>
        <!-- ------------------------------ End Slider------------------------------------>
        <div id="info_product">
            <table style="width: 100%;">
                <tr style="padding-top: 10px">
                    <td valign="top">
                        <img src="img/list_style_square_img.png" />
                        <strong>CONDITIONS:</strong></td>
                    <td valign="top" class="item">

                        <div class="conditions_place"></div>


                        <div class="tooltip_description" style="display: none">


                            <div class="tool_tip_div_style">
                                <div class="rates">
                                    <div class="star_class star_on"></div>
                                    <div class="star_class star_on"></div>
                                    <div class="star_class star_on"></div>
                                    <div class="star_class star_on"></div>

                                    <div id="Div6">EXCELLENT - New with no nicks gouges or scratches </div>
                                </div>

                                <div class="rates">
                                    <div class="star_class star_on"></div>
                                    <div class="star_class star_on"></div>
                                    <div class="star_class star_on"></div>
                                    <div class="star_class "></div>

                                    <div id="Div1">VERY GOOD - Like new with superficial surface scratches and blemishes</div>
                                </div>



                                <div class="rates">
                                    <div class="star_class star_on"></div>
                                    <div class="star_class star_on"></div>
                                    <div class="star_class "></div>
                                    <div class="star_class "></div>

                                    <div id="Div2">GOOD - Used with minimal signs of age and wear and no major defects</div>
                                </div>


                                <div class="rates">
                                    <div class="star_class star_on"></div>
                                    <div class="star_class  "></div>
                                    <div class="star_class "></div>
                                    <div class="star_class "></div>

                                    <div id="Div3">FAIR - Used with moderate signs of age and wear</div>
                                </div>


                                <p>
                                    All ratings indicate the furniture is in working condition and do not have any 
                             <br />
                                    structural or operational problems. "Age" and "wear" refer only to cosmetic defects
                             <br />
                                    found in the item(s).
                                </p>
                            </div>
                        </div>
                    </td>

                </tr>
                <tr style="padding-top: 10px">
                    <td valign="top" style="white-space: nowrap;">
                        <img src="img/list_style_square_img.png" />
                        <strong>MATCHED ITEMS:</strong></td>
                    <td valign="top" class="matched_items_text_div">
                        <div class="arrow_matched_items"></div>
                         <span class="text_clickable_matched_item" style="margin-left: 10px;cursor:pointer;">YES (see matched items)</span>

                        <div id="matched_items_div" style="display: none;">
                           
 
                        </div>
                    </td>

                </tr>
                <tr>
                    <td class="auto-style1 AddToCartContent" valign="top">
                        <img src="img/list_style_square_img.png" />
                        <img src="img/add_to_cart_button_disabled.png" style="vertical-align: middle;">
                    </td>
                    <td class="auto-style1" valign="top">
                       <a href="javascript:history.go(-1)"> <img src="img/return_to_catalog.png" style="vertical-align: middle;" class="return_to_catalog" /></a></div>
                    </td>

                </tr>
                
            </table>

        </div> 

    </div>
    <!-- ------------------------------End Left Side------------------------------------>

    <!-- ------------------------------ Right Side------------------------------------>
    <div class="right_div_description">
        <!-- ------------------------------Tabs------------------------------------>
        <div id="tabs" style="position: relative; top: 1px;">
            <div class="tab_style selected_tab">Description</div>
            <div class="tab_style">Specifications</div>
            <div class="tab_style">Additional Info</div>
        </div>
        <!-- --- ----- -->
        <div class="tabs_div">
            <!-- -------Descriptions------------>
            <div class="tab_content" style="display: block;">
                <h2 class="item_name"></h2>

                <p class="type_item">
                    <img src="img/list_style_square_img.png">
                    Type:</p>
                <p class="type_item_details" id="type_desc"></p>


                <p class="type_item">
                    <img src="img/list_style_square_img.png">
                    Color:</p>
                <p class="type_item_details" id="color_desc"></p>


                <p class="type_item">
                    <img src="img/list_style_square_img.png">
                    Features:</p>
                <ul id="features_desc_list">
                </ul>


                <p class="type_item">
                    <img src="img/list_style_square_img.png">
                    Materials:</p>
                <p class="type_item_details" id="materials_desc"></p>


                <p class="type_item">
                    <img src="img/list_style_square_img.png">
                    Special Features:</p>
                <ul id="special_features_desc_list">
                </ul>


                <p class="type_item">
                <img src="img/list_style_square_img.png">
                Dimensions:</p>
            <p class="type_item_details dimensions_desc"  ></p>
            </div>
        </div>
        <!-- -------End Descriptions------------>
        <div class="tab_content">
            <h2 class="item_name">HERMAN Miller AERON CHAIR</h2>
            <p class="type_item_details" id="ModelnumberCode2">Model #: </p>
           
        </div>
        <!-- -------Addi Info------------>
        <div class="tab_content">
            <h2 class="item_name">HERMAN Miller AERON CHAIR</h2>
            <p class="type_item_details" id="Additional_info_desc"></p>


            <h2 style="margin-top: 60px;">OUR RATING SYSTEM:</h2>
            <div class="rates_right_box">
                <div class="star_class star_on"></div>
                <div class="star_class star_on"></div>
                <div class="star_class star_on"></div>
                <div class="star_class star_on"></div>

                <div id="Div4" class="rates_text_right_box">EXCELLENT - New with no nicks gouges or scratches </div>
            </div>

            <div class="rates_right_box">
                <div class="star_class star_on"></div>
                <div class="star_class star_on"></div>
                <div class="star_class star_on"></div>
                <div class="star_class "></div>

                <div id="Div5" class="rates_text_right_box">VERY GOOD - Like new with superficial surface scratches and blemishes</div>
            </div>



            <div class="rates_right_box">
                <div class="star_class star_on"></div>
                <div class="star_class star_on"></div>
                <div class="star_class "></div>
                <div class="star_class "></div>

                <div id="Div7" class="rates_text_right_box">GOOD - Used with minimal signs of age and wear and no major defects</div>
            </div>


            <div class="rates_right_box">
                <div class="star_class star_on"></div>
                <div class="star_class  "></div>
                <div class="star_class "></div>
                <div class="star_class "></div>

                <div id="Div8" class="rates_text_right_box">FAIR - Used with moderate signs of age and wear</div>
            </div>


            <p style="font-size: 14px; margin-top: 30px;">
                All ratings indicate the furniture is in working condition and do not have any 
                             structural or operational problems. "Age" and "wear" refer only to cosmetic defects
                             found in the item(s).
            </p>

        </div>
        <!-- ------ End-Addi Info------------>

    </div>
    <!-- ------------------------------End Tabs------------------------------------>
    </div>
    <!-- ------------------------------End Right Side------------------------------------>

    <input type="hidden" name="modelnumber" value="" class="modelnumbervalue"  />
    <div id="pop_up_box" >
        <div id="poped_up_content" class="style_scroll_bar ">
          <div id="image_loader_pop" style="display: block;"><img src="img/ajax_loader.gif"></div>
           
        </div>

    </div>


    <script type="text/javascript">

        $(document).ready(function () {
            //Product Details Page Start
            var id_item = getURLParameter('id');
            //   GetProductDetails(id_item);

            //Slider 
            $(document).on("click", ".dot_class", function () {
                var index = $(".dot_class").index(this);
                $('.dot_class').removeClass('selected');
                $(this).addClass('selected');

                var attr_src = $(".small_image_class:eq(" + index + ") img").attr('src');
                $('.big_image').attr('src', attr_src);
            });



            $(document).on("click", ".small_image_class", function () {
                var index = $(".small_image_class").index(this);
                $(".dot_class").removeClass('selected');
                $(".dot_class:eq(" + index + ")").addClass('selected');

                var attr_src = $(this).find("img").attr('src');
                $('.big_image').attr('src', attr_src);
            });


            //End Slider

            //Tabs
            $(".tab_style").click(function () {
                $("#ModelnumberCode2").html('Asset Number: ' + $(".modelnumbervalue").val());
                var index = $(".tab_style").index(this);
                $('.tab_style').removeClass('selected_tab');
                $(this).addClass('selected_tab');

                $('.tab_content').hide();
                $('.tab_content:eq(' + index + ')').show();

            });

            //Matched Items:
            $('.arrow_matched_items , .text_clickable_matched_item').click(function () {

                var Open = $('.arrow_matched_items').hasClass("arrow_matched_items_open");

                if (Open) {
                    $('.arrow_matched_items').removeClass("arrow_matched_items_open");
                    $('#matched_items_div').fadeOut();
                }
                else {
                    $('.arrow_matched_items').addClass("arrow_matched_items_open");
                    $('#matched_items_div').fadeIn();
                }

            });


            //Conditions
            $('.conditions_place').hover(function () {
                $('.tooltip_description').stop(true, true).fadeIn();
            }, function () {
                $('.tooltip_description').stop(true, true).fadeOut();
            });


            // get data from server 
            ModelId = getURLParameter('id');
            GetProductDetails(ModelId);



            //pop up code
            $('.order_from_button').click(function () {
                $('#pop_up_box').fadeIn();

                $.ajax({
                    type: "GET",
                    url: "asset_order_form.aspx",
                    data: {
                        'id': ModelId
                    },



                    success: function (data) {

                        $("#poped_up_content").html(data);
                        $('#ModelName_pop').append($('.item_name:eq(0)').html());
                        $('#ModelName_pop').attr("val", $('.item_name:eq(0)').html());
                        //$('#ModelColor_pop').append($('#color_desc').html());
                        //$('#ModelMaterials_pop').append($('#materials_desc').html());
                        var attr_src = $(".small_image_class:eq(0) img").attr('src');
                        $('.header_popup_image_module').attr('src', attr_src);
                    },
                    error: ServiceFailed

                });


            });



            $(document).keyup(function (e) {

                if (e.keyCode == 27) { $('#pop_up_box').fadeOut(); }   // esc
            });

            $(document).mouseup(function (e) {
                if (e.target.getAttribute('id') == "pop_up_box")
                    $('#pop_up_box').fadeOut();
            });
            //End pop up code


        });






    </script>



</asp:Content>
