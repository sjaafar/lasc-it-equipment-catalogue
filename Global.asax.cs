﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace LASC_ShoppingCart2
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
            if (Session["ItemInCart"] != null)
            {
                SqlConnection sqlConn = new SqlConnection(ConfigurationManager.AppSettings["STR_CONN"]);
                List<int> oItemList = (List<int>)Session["ItemInCart"];
                SqlCommand SQL_CMD = new SqlCommand("", sqlConn);

                SQL_CMD.Connection.Open();

                foreach (int oItem in oItemList)
                {

                    string sql3 = "UPDATE Models SET ModelsUDValueCode2 = 'Surplus'  WHERE ModelID = @ModelID AND ModelsUDValueCode2='InCart' ";
                    SQL_CMD.CommandText = sql3;
                    List<int> ItemInCart = new List<int>();
                    SQL_CMD.CommandType = CommandType.Text;
                    SQL_CMD.Parameters.Clear();
                    SQL_CMD.Parameters.Add(new SqlParameter("ModelID", oItem));

                    SQL_CMD.ExecuteNonQuery();
                }
                SQL_CMD.Connection.Close();
               
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}