// JavaScript Document
//server side 
//var url = "/LASC_SurplusShoppingCartTest/WcfDataService1.svc";

//My localhost
//var url = "http://localhost:8081/LASC_StockShoppingCart2/WcfDataService1.svc";
var url = window.location.href.substr(0, location.href.lastIndexOf("/") + 1) + "WcfDataService1.svc";


$.ajaxSetup({ cache: false });


/* window.onbeforeunload = function (event) {
    EmptyServerSession();

 };

$(function () {
    $("*").click(function () {
        window.onbeforeunload = null;
    });
});
*/
// Login Page 
function send_IsAuthenticated_User(username, password) {
    var _Params_IsAuthenticated = new Object();

    _Params_IsAuthenticated.UserId = 0;
    _Params_IsAuthenticated.UserLogOn = username;
    _Params_IsAuthenticated.UserPassword = password;

    var _Params = JSON.stringify(_Params_IsAuthenticated);

    $('#image_loader').fadeIn();
    $('#content').hide();
    $.ajax({
        type: "GET",
        url: url + "/IsAuthenticated_User",
        data: "Input=" + encodeURIComponent(_Params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {

            if (i_Srv_Response.UserId == 0) {
                //alert('Invalid username or Password');
                $('.error_message').html('Invalid username or Password');
                $('#image_loader').hide();
                $('#content').fadeIn();

            }
            else window.location.href = 'Default.aspx';

        },
        error: ServiceFailed

    });
}
//End Login Page


//Search Input


function SearchProduct(index_start, search_text) {

    $('#image_loader').show();
    $('#content').html('');

    var _Params_PageAndCategorie = new Object();
    _Params_PageAndCategorie.IndexPage = index_start;
    _Params_PageAndCategorie.search_input = search_text;


    var _Params = JSON.stringify(_Params_PageAndCategorie);

    $.ajax({
        type: "GET",
        url: url + "/SearchProduct",
        data: "Input=" + _Params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {

            SearchProduct_Completed(i_Srv_Response, index_start, search_text)
        },
        error: ServiceFailed

    });
}

function SearchProduct_Completed(oResult, index_start, search_text) {

    var search_body = "";
    var condition_stars = "";
    $('#image_loader').hide();
    $('#content').fadeIn();

    if (oResult.ListProduct.length == 0)
        search_body = search_body + "<h1 class=\"header_title_blue\">Sorry, nothing match your search</h1>";
    else
        search_body = search_body + "<h1 class=\"header_title_blue\">Search Results</h1>";

    search_body = search_body + " <div id=\"product_content\" style=\"margin-bottom: 100px;\">" +
        "    <style>#category_table tr{font-size:12px!important;</style>" +
        "    <table style=\"width: 100%;\" id=\"category_table\"   cellpadding=\"0\" cellspacing=\"0\">" +
         "       <tr>" +
          "          <td>Product<a href=\"#\"></a></td>" +

           "        <td>Subcategory</td> " +
           "        <td>Model</td> " +
           "         <td>Asset #</td>" +
           "         <td>Condition</td>" +

            "        <td>Add To Cart</td> " +
            "    </tr>";


    if (oResult.nbrPages == "0") {
        console.log(oResult);
        for (var i = 0 ; i < oResult.ListProduct.length; i++) {


            if (oResult.ListProduct[i].ProductImage != "none") {
                var structure = "";
                var structure = structure + "<tr class=\"item_tr\">";
                var structure = structure + "<td class=\"GoToProduct\"><a href=\"product_details.aspx?id=" + oResult.ListProduct[i].id + "\"><div  class=\"categories_items_image\" ><img src=\"data:image/png;base64," + oResult.ListProduct[i].ProductImage + "\"  ></div></a></td>";

                var structure = structure + " <td class=\"GoToProduct\">" + oResult.ListProduct[i].FURNITURE_TYPE + "</td>";
                var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListProduct[i].Model + " </td>";
                var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListProduct[i].ModelCode + " </td>";
                structure += "<td>  " + oResult.ListProduct[i].ConditionCode + " </td>";

                if (CheckIfItemInSessionStorage(oResult.ListProduct[i].id))
                    structure = structure + "<td >  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
                else
                    structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button\"></div> </td>";
                var structure = structure + "    </tr>"
                var structure = structure + "    </tr>";
                search_body = search_body + structure;
            }
            else {
                var structure = "";
                var structure = structure + "<tr class=\"item_tr\">";
                var structure = structure + "<td><a href=\"product_details.aspx?id=" + oResult.ListProduct[i].id + "\">Image Not Available</a></td>";
                var structure = structure + " <td class=\"GoToProduct\">" + oResult.ListProduct[i].FURNITURE_TYPE + "</td>";
                var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListProduct[i].Model + " </td>";
                var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListProduct[i].ModelCode + " </td>";
                structure += "<td class=\"GoToProduct\">  " + oResult.ListProduct[i].ConditionCode + " </td>";
                if (CheckIfItemInSessionStorage(oResult.ListProduct[i].id))
                    structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
                else
                    structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button\"></div> </td>";
                var structure = structure + "    </tr>"
                var structure = structure + "    </tr>";
                search_body = search_body + structure;

            }

        }
        $('.arrows').hide();

        search_body = search_body + "</table>" +
    "</div> ";

        $('#content').html(search_body);

        //Make tr clickable
        $('#category_table').delegate('.GoToProduct', 'click', function () {
            window.location.href = $(this).parent().find('a').attr('href');

        });

        $('.add_asset_button').click(function () {
            if ($(this).hasClass('add_asset_checked')) {
                $(this).removeClass('add_asset_checked');
                RemoveItemFromSessionStorage($(this).attr('item_id'));
                // var items = JSON.parse(sessionStorage.getItem('item_selected'));
                var items = JSON.parse(sessionStorage.getItem('item_selected'));
                $('#total_items_input').val(items.length);
                if (items.length == 0) {
                    $('#header_title_checkout').html("NO Item Selected");
                    $('#submit_order').attr("disabled", "disabled");
                }
                else {
                    $('#header_title_checkout').html("Checkout");
                    $('#submit_order').removeAttr("disabled");
                }


            }
            else {
                $(this).addClass('add_asset_checked');
                AddAsset_item($(this).attr('item_id'));
            }


        });

        return;
    }


    //if (index_start == oResult.nbrPages) return;

    $(".item_tr").remove();
    $("#nbr_pages").html('');
    for (var i = 0 ; i < oResult.ListProduct.length; i++) {


        if (oResult.ListProduct[i].ProductImage != "none") {
            var structure = "";
            var structure = structure + "<tr class=\"item_tr\">";
            var structure = structure + "<td ><a href=\"product_details.aspx?id=" + oResult.ListProduct[i].id + "\"><div  class=\"categories_items_image\" ><img src=\"data:image/png;base64," + oResult.ListProduct[i].ProductImage + "\" ></div></a></td>";
            var structure = structure + " <td class=\"GoToProduct\">" + oResult.ListProduct[i].FURNITURE_TYPE + "</td>";
            var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListProduct[i].Model + " </td>";
            var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListProduct[i].ModelCode + " </td>";
            structure += "<td class=\"GoToProduct\">  " + oResult.ListProduct[i].ConditionCode + " </td>";
            if (CheckIfItemInSessionStorage(oResult.ListProduct[i].id))
                structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
            else
                structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button\"></div> </td>";
            var structure = structure + "    </tr>"
            var structure = structure + "    </tr>";
            search_body = search_body + structure;
        }
        else {
            var structure = "";
            var structure = structure + "<tr class=\"item_tr\">";
            var structure = structure + "<td><a href=\"product_details.aspx?id=" + oResult.ListProduct[i].id + "\">Image Not Available</a></td>";
            var structure = structure + " <td class=\"GoToProduct\">" + oResult.ListProduct[i].FURNITURE_TYPE + "</td>";
            var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListProduct[i].Model + " </td>";
            var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListProduct[i].ModelCode + " </td>";
            structure += "<td>  " + oResult.ListProduct[i].ConditionCode + " </td>";
            if (CheckIfItemInSessionStorage(oResult.ListProduct[i].id))
                structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
            else
                structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button\"></div> </td>";
            var structure = structure + "    </tr>"
            var structure = structure + "    </tr>";
            search_body = search_body + structure;
        }
    }

    search_body = search_body + " <div class=\"arrows_search\" style=\"margin: auto; clear: both; height: 91px; padding-left: 45px; padding-top: 28px; display: none;\">";
    search_body = search_body + " <table style=\"margin: 30px auto 30px;\">";
    search_body = search_body + "     <tbody><tr>";
    search_body = search_body + "       <td> <div class=\"prev_button_search\" style=\"cursor:pointer;float:left;\"><img src=\"img/flesh_left.png\" style=\"width: 70px;\"></div></td>";
    search_body = search_body + "       <td><div id=\"nbr_pages\"></div></td>";
    search_body = search_body + "      <td> <div class=\"next_button_search\" style=\"cursor:pointer;float:right;\"><img src=\"img/flesh_right.png\" style=\"width: 70px;\"></div></td>";
    search_body = search_body + "   </tr>     ";
    search_body = search_body + " </tbody></table>     ";
    search_body = search_body + "  </div>";

    $('#content').html(search_body);





    var index_page = 0;
    var search_text_pagination = search_text

    $('.next_button_search').click(function () {
        if (index_start == oResult.nbrPages) return;
        index_page = parseInt($('.selected_page_search').html());
        SearchProduct(index_page, search_text);//Next Page 

    });

    $('.add_asset_button').click(function () {
        if ($(this).hasClass('add_asset_checked')) {
            $(this).removeClass('add_asset_checked');
            RemoveItemFromSessionStorage($(this).attr('item_id'));
            // var items = JSON.parse(sessionStorage.getItem('item_selected'));
            var items = JSON.parse(sessionStorage.getItem('item_selected'));
            $('#total_items_input').val(items.length);
            if (items.length == 0) {
                $('#header_title_checkout').html("NO Item Selected");
                $('#submit_order').attr("disabled", "disabled");
            }
            else {
                $('#header_title_checkout').html("Checkout");
                $('#submit_order').removeAttr("disabled");
            }


        }
        else {
            $(this).addClass('add_asset_checked');
            AddAsset_item($(this).attr('item_id'));
        }


    });

    //Make tr clickable
    $('#category_table').delegate('.GoToProduct', 'click', function () {
        window.location.href = $(this).parent().find('a').attr('href');

    });


    $('.prev_button_search').click(function () {
        console.log(index_page);
        index_page = parseInt($('.selected_page_search').html());
        if (index_page <= 1) return;
        index_page = parseInt($('.selected_page_search').html()) - 2;

        SearchProduct(index_page, search_text);//Next Page 
    });





    var i = index_start - 5;
    // if ( index_start < (oResult.nbrPages - 1) && (index_start ) > (oResult.nbrPages - 10 ) ) i = index_start -5;
    var res = 0;
    while (i < oResult.nbrPages && res < 10) {
        res++;
        i++;
        if ((i < 0)) i = 0;
        if (i != index_start) {
            var DivContent = "<div  onClick=SearchProduct(" + i + ",'" + search_text + "')>" + (i + 1) + "</div>"
            $("#nbr_pages").append(DivContent);
        }
        else {
            $("#nbr_pages").append("<div class='selected_page_search'>" + (i + 1) + "</div>");

        }
    }


}
//End Search 


//Product Detail Page
function GetProductDetails(ModelID) {
    $.ajax({
        type: "GET",
        url: url + "/GetProductDetails",
        data: "Input=" + ModelID,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {

            GetProductDetails_Completed(i_Srv_Response)
        },
        error: ServiceFailed

    });
}

function GetProductDetails_Completed(oResult) {

    // Images 
    if (oResult.ProductImage.length != 0) {
        $('#dots_div').html('');
        $('#small_images').html('');
        for (var i = 0; i < oResult.ProductImage.length ; i++) {
            $('#small_images').append("<div  class=\"small_image_class\"><img src=\"ShowImage.ashx?autoid=" + oResult.ProductImage[i] + "\"></div>");
            $('#dots_div').append("<div class=\"dot_class\"></div>");
        }

        var BigImage = oResult.ProductImage[0];
        $('.big_image').attr("src", "ShowImage.ashx?autoid=" + BigImage);
        $(".dot_class:eq(0)").addClass('selected');

    }
    // End Images
    //------------------------------------------------------------
    var Condition = oResult.Conditions;
    condition_stars = "<div class=\"rates\">  <div class=\"star_class\"></div> <div class=\"star_class\"></div> <div class=\"star_class\"></div><div class=\"star_class\"></div><div class=\"clearThis\"></div></div>";
    if (Condition == '1') {
        condition_stars = "<div class=\"rates\">  <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div><div class=\"clearThis\"></div></div>";
    }
    if (Condition == '2') {
        condition_stars = "<div class=\"rates\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div><div class=\"clearThis\"></div></div>";
    }
    if (Condition == '3') {
        condition_stars = "<div class=\"rates\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class\"></div><div class=\"clearThis\"></div></div>";
    }
    if (Condition == '4') {
        condition_stars = "<div class=\"rates\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class star_on\"></div><div class=\"clearThis\"></div></div>";
    }

    $('.conditions_place').append(condition_stars);
    //End Conditions Section
    //------------------------------------------------------------------
    //Right Box 
    $('#qty_item').html(oResult.Quantity);
    $('.item_name').html(oResult.ModelName);
    if (oResult.ModelName == 'COMPUTER') {
        $("#ModelnumberCode2").html('<img src="img/list_style_square_img.png" class=""> <strong>CPU: </strong>' + oResult.CPU + '<br>' +
                                    '<img src="img/list_style_square_img.png" class=""> <strong>CPU Speed: </strong>' + oResult.CPUspeed + '<br>' +
                                    '<img src="img/list_style_square_img.png" class=""> <strong>HD Size: </strong>' + oResult.HDSize + '<br>' +
                                    '<img src="img/list_style_square_img.png" class=""> <strong>IP Address: </strong>' + oResult.IP + '<br>' +
                                    '<img src="img/list_style_square_img.png" class=""> <strong>BIOS: </strong>' + oResult.BIOS + '<br>' +
                                    '<img src="img/list_style_square_img.png" class=""> <strong>RAM: </strong>' + oResult.RAM + '<br>' +
                                    '<img src="img/list_style_square_img.png" class=""> <strong>Video Card: </strong>' + oResult.VideoCard + '<br>' +
                                    '<img src="img/list_style_square_img.png" class=""> <strong>MAC Address: </strong>' + oResult.MAC + '<br>');
    }

    if (oResult.ModelName == 'MONITOR') {
        $("#ModelnumberCode2").html('<img src="img/list_style_square_img.png" class=""> <strong>Resolution: </strong>' + oResult.resolution + '<br>' +
                                    '<img src="img/list_style_square_img.png" class=""> <strong>Screen Size: </strong>' + oResult.ScreenSize + '<br>');
    }

    if (oResult.ModelName == 'SOFTWARE') {
        $("#ModelnumberCode2").html('<img src="img/list_style_square_img.png" class=""> <strong>Version: </strong>' + oResult.SoftwareVersion + '<br>' +
                                    '<img src="img/list_style_square_img.png" class=""> <strong>Licenses: </strong>' + oResult.SoftwareLicenses + '<br>');
    }

    $('.item_notes').append(oResult.Notes);
    $('#type_desc').html(oResult.ModelCode);
    $('#color_desc').html(oResult.assetType);
    $('#SerialNum').html(oResult.SerialNum);
    $('#features_desc_list').html(oResult.CategoryCode);
    $('#materials_desc').html(oResult.FURNITURE_TYPE);
    $('#special_features_desc_list').html(oResult.Model);
    $('.dimensions_desc').html(oResult.SPECIFICATIONS);
    $('#Additional_info_desc').html(oResult.ADDITIONAL_INFO);
    $('.modelnumbervalue').val(oResult.MainModelCode);
    $('#ModelnumberCode').append(oResult.MainModelCode);

    //End Right Box 
    //---------------------------------------------------------------------

    //Matched Items
    //$('#matched_items_div').html('');
    if (oResult.MatchedModel != null && oResult.MatchedModel.length != 0 && oResult.MatchedModel[0].MatchedModelModelID != 0) {
        for (var i = 0; i < oResult.MatchedModel.length ; i++) {
            var body_matched_items = "<div class=\"matche_item\">";

            if (oResult.MatchedModel[i].ModelImage != null)
                body_matched_items = body_matched_items + "<a href=\"product_details.aspx?id=" + oResult.MatchedModel[i].MatchedModelModelID + "\"><div class=\"image_box_mathced\"><img src=\"data:image/png;base64," + oResult.MatchedModel[i].ModelImage + "\"  /></div></a>";
            else body_matched_items = body_matched_items + "<a href=\"product_details.aspx?id=" + oResult.MatchedModel[i].MatchedModelModelID + "\"><div style=\"margin-top: 36px;margin-left: 5px;margin-bottom: 30px;\">Not Available</div></a>";

            body_matched_items = body_matched_items + "<div  class=\"matched_item_popup\">";

            var title_matched = oResult.MatchedModel[i].MatchedModelName;
            if (title_matched == null)
                title_matched = "No Title";

            body_matched_items = body_matched_items + "<div  class=\"tool_tip_div_style matched_title\" >" + title_matched + "</div>" +
                                   "</div>" +
                               "</div>";
            $('#matched_items_div').append(body_matched_items);

        }

        var BigImage = oResult.ProductImage[0];
        $('.big_image').attr("src", "data:image/png;base64," + BigImage);
        $(".dot_class:eq(0)").addClass('selected');

    }
    else {
        $('.matched_items_text_div').html("No Matched Items.");
    }

    //Hover effects
    $('.matche_item').hover(function () {
        $(this).find('.matched_item_popup').stop(true, true).fadeIn();
        $('.matche_item').css('opacity', "0.7");
        $(this).css('opacity', "1");
    }, function () {
        $(this).find('.matched_item_popup').stop(true, true).fadeOut();
        $('.matche_item').css('opacity', "1");
    });

    //End Matched items
    //-------------------------------------------------------------------------------
    $('#image_loader').hide();
    $('#content').fadeIn();

}
//End Product Detail Page

//Categories Page 
function GetProducts_needed(index_start, CategorieId, furnitureID) {
    $('#image_loader').show();
    $('#content').hide();

    var _Params_PageAndCategorie = new Object();
    _Params_PageAndCategorie.IndexPage = index_start;
    _Params_PageAndCategorie.CategorieId = CategorieId;
    _Params_PageAndCategorie.furnitureID = furnitureID;

    var _Params = JSON.stringify(_Params_PageAndCategorie);

    $.ajax({
        type: "GET",
        url: url + "/GetProducts_needed",
        data: "Input=" + _Params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {

            GetProducts_needed_Completed(i_Srv_Response, index_start, CategorieId, furnitureID);
            $('.add_asset_button').click(function () {

                if ($(this).hasClass('add_asset_checked')) {
                    $(this).removeClass('add_asset_checked');
                    RemoveItemFromSessionStorage($(this).attr('item_id'));
                }
                else {
                    $(this).addClass('add_asset_checked');
                    AddAsset_item($(this).attr('item_id'));
                }
            });
        },
        error: ServiceFailed
    });
}
// johnny 6/13/2017
 function Getorder_neededGeneral(index, CustomLogin,flag)
{
    $('#image_loader').show();
    $('#content').hide();
    $('#Div1').hide();
    var _Params_PageAndOrders = new Object();
    _Params_PageAndOrders.index = index;
    _Params_PageAndOrders.CustomLogin = CustomLogin;
    _Params_PageAndOrders.flag=flag
    var _Params = JSON.stringify(_Params_PageAndOrders);
     $.ajax({
        type: "GET",
        url: url + "/Getorder_neededGeneral",
        data: "Input=" + _Params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {
            Getorder_neededGeneral_completed(i_Srv_Response, index, CustomLogin);

            
        },
        error: ServiceFailed
    });
   
}
 function Getorder_neededGeneral_completed(oResult, index_start, CustomLogin)
{
    if (index_start > oResult.nbrPages || index_start < 0) {
        $('#image_loader').hide();
        $('#content').fadeIn();
        $('#Div1').fadeIn();
            return;
    }
    $('#image_loader').hide();
    $('#content').fadeIn();
    $('#Div1').fadeIn();
    if (oResult.ListOrder.length==0) {

        $('.header_title_blue').html("Sorry you have no orders to display.");
    }
    var i = index_start - 5;
    $(".item_tr").remove();
    $("#nbr_pages").html('');
    for (var i = 0 ; i < oResult.ListOrder.length; i++) {
        var structure = "";
        var structure = structure + "<tr class=\"item_tr\">";
        if (oResult.ListOrder[i].orderNumber !=null)
            var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListOrder[i].orderNumber + " </td>";
        else
            var structure = structure + "<td class=\"GoToProduct\">  Not Avilable </td>";
        if (oResult.ListOrder[i].OrderDate != null) {
            var jasondate = oResult.ListOrder[i].OrderDate.toString();
            var date = new Date(parseInt(jasondate.replace('/Date(', '')));
            var month = date.getMonth()+1;
            var day = date.getDate();
            var year = date.getFullYear();
            var structure = structure + "<td class=\"GoToProduct\"> " + month +"/"+day+"/"+year+"</td>";
        }
        else
            var structure = structure + "<td class=\"GoToProduct\"> Not Available </td>";
        if (oResult.ListOrder[i].issuecode != null && oResult.ListOrder[i].issuename != null)
            var structure = structure + "<td class=\"GoToProduct alignLeft\"> " + oResult.ListOrder[i].issuename + "(" + oResult.ListOrder[i].issuecode + ")</td>";
        else
            var structure = structure + "<td class=\"GoToProduct alignLeft\"> Not Available </td>";
        if (oResult.ListOrder[i].AddressToShip != null)
            var structure = structure + "<td class=\"GoToProduct alignLeft\"> " + oResult.ListOrder[i].AddressToShip + "</td>";
        else
            var structure = structure + "<td class=\"GoToProduct alignLeft\"> Not Available </td>";
        if (oResult.ListOrder[i].TotalPrice != 0)
        {
            var num=oResult.ListOrder[i].TotalPrice.toFixed(2);
            var structure = structure + "<td class=\"GoToProduct alignRight\">" + num + "</td>";
        }
        else
            var structure = structure + "<td class=\"GoToProduct alignRight\"> Not Available </td>";
        var structure = structure + "<td class=\"GoToProduct\"><a href=\"Orders.aspx?orderid=" + oResult.ListOrder[i].OrderId + "\"> View Details</a></td>"
        var structure = structure + " </tr>";
        $('#item_selected_table tr:last').after(structure);
    }
    if (oResult.nbrPages == 0) {
        $('.arrows').hide();
    }
    for (i = 0; i <= oResult.nbrPages; i++) {
        if (i != index_start)
            $("#nbr_pages").append("<div  onClick=Getorder_neededGeneral(" + i + "," + CustomLogin + ")>" + (i + 1) + "</div>");
        else
            $("#nbr_pages").append("<div class='selected_page'  onClick=Getorder_neededGeneral(" + i + "," + CustomLogin + ")>" + (i + 1) + "</div>");
    }

}
function GeTorder_needed_(index_start_,CustomLogin_,orderid_)
 {
    var bool = true;
    if (bool) {
        $('#image_loader').show();
        $('#content').hide();
        $('#SalesOrderLines').hide();
        var _Params_PageAndOrdersGeneral = new Object();
        _Params_PageAndOrdersGeneral.index = index_start_;
        _Params_PageAndOrdersGeneral.customLogin = CustomLogin_;
        _Params_PageAndOrdersGeneral.orderid = orderid_;
        var _Params = JSON.stringify(_Params_PageAndOrdersGeneral);
        bool = false;
        $.ajax({
            type: "GET",
            url: url + "/GeTorder_needed_",
            data: "Input=" + _Params,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processdata: false,
            async: false,
            success: function (i_Srv_Response) {
                Getorder_needed_completed(i_Srv_Response, index_start_, CustomLogin_, orderid_);
            },
            error: ServiceFailed
        });
    }
   
 
}
function Getorder_needed_completed(oResult, index_start, CustomLogin,orderid) {
    if (index_start > oResult.nbrPages || index_start < 0) {
        $('#image_loader').hide();
        $('#content').fadeIn();
        $('#SalesOrderLines').fadeIn();
        return;
    }
        $('#image_loader').hide();
        $('#content').fadeIn();
        $('#SalesOrderLines').fadeIn();
        if (oResult.ListOrder.length==0) {

            $('.header_title_blue').html("Sorry you have no orders to display.");
        }
        var i = index_start - 5;
        $(".item_tr").remove();
        $("#nbr_pages").html('');
        for (var i = 0 ; i < oResult.ListOrder.length; i++) {
            var structure = "";
            var structure = structure + "<tr class=\"item_tr\">";
            if (oResult.ListOrder[i].orderNumber != null)
                var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListOrder[i].orderNumber + " </td>";
            else
                var structure = structure + "<td class=\"GoToProduct\">  Not Avilable </td>";
            if (oResult.ListOrder[i].ModelNumber != null)
                var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListOrder[i].ModelNumber + " </td>";
            else
                var structure = structure + "<td class=\"GoToProduct\"> Not Available </td>";
            if (oResult.ListOrder[i].ModelName != null)
                var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListOrder[i].ModelName + " </td>";
            else
                var structure = structure + "<td class=\"GoToProduct\"> Not Available </td>";
            if (oResult.ListOrder[i].price != 0) {
                var num = oResult.ListOrder[i].price.toFixed(2);
                var structure = structure + "<td class=\"GoToProduct\"> " + num + " </td>";
            }
            else
                var structure = structure + "<td class=\"GoToProduct\"> Not Available </td>";
            var structure = structure + " </tr>";
            $('#item_selected_table tr:last').after(structure);
        }
        if (oResult.nbrPages == 0) {
            $('.arrows').hide();
        }
        for (i = 0; i <= oResult.nbrPages; i++) {
            if (i != index_start)
                $("#nbr_pages").append("<div  onClick=GeTorder_needed_(" + i + "," + CustomLogin + ", " + orderid + ")>" + (i + 1) + "</div>");
            else
                $("#nbr_pages").append("<div class='selected_page'  onClick=GeTorder_needed_(" + i + "," + CustomLogin + " , "+orderid+")>" + (i + 1) + "</div>");
        }


}

function GetProducts_needed_Completed(oResult, index_start, CategorieId, furnitureID) {

    if (index_start > oResult.nbrPages || index_start < 0) {
        $('#image_loader').hide();
        $('#content').fadeIn();
        return;
    }
    $('#image_loader').hide();
    $('#content').fadeIn();

    if (oResult.ListProduct.length == 0) {

        $('.header_title_blue').html("Sorry NO Item In This Category");
    }

    var i = index_start - 5;
    $(".item_tr").remove();
    $("#nbr_pages").html('')
    for (var i = 0 ; i < oResult.ListProduct.length; i++) {
        if (oResult.ListProduct[i].ProductImage != "") {
            var ProductImage = "<div  class=\"categories_items_image\" ><img src=\"ShowImage.ashx?autoid=" + oResult.ListProduct[i].ProductImage + "\"  ></div>"
        } else {
            var ProductImage = "Image Not Available";
        }
        var Condition = oResult.ListProduct[i].ConditionCode;
        condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class\"></div> <div class=\"star_class\"></div> <div class=\"star_class\"></div><div class=\"star_class\"></div></div>";
        if (Condition == '1') {
            condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";
        }
        if (Condition == '2') {
            condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";
        }
        if (Condition == '3') {
            condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class\"></div></div>";
        }
        if (Condition == '4') {
            condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class star_on\"></div></div>";
        }

        var structure = "";
        var structure = structure + "<tr class=\"item_tr\">";
        var structure = structure + "<td class=\"GoToProduct\"><a href=\"product_details.aspx?id=" + oResult.ListProduct[i].id + "\">" + ProductImage + "</a></td>";
        var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListProduct[i].Subcategory + " </td>";
        var structure = structure + "<td class=\"GoToProduct\"> " + oResult.ListProduct[i].Model + " </td>";
        //var structure = structure + " <td class=\"GoToProduct\">" + oResult.ListProduct[i].ModelName + "</td>";
        var structure = structure + " <td class=\"GoToProduct\">" + oResult.ListProduct[i].ModelNumber + "</td>";
        var structure = structure + " <td class=\"GoToProduct\">" + condition_stars + "</td>";
        if (CheckIfItemInSessionStorage(oResult.ListProduct[i].id)) {
            if (typeof (CustomLogin) != "undefined" && CustomLogin != "") {
                if (CustomLogin == "True")
                    structure = structure + "<td style=\"display:none\">  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
                else
                    structure = structure + "<td >  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
            }
            else

                structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
        }
        else {

            if (typeof (CustomLogin) != "undefined" && CustomLogin != "") {
                if (CustomLogin == "True")
                    structure = structure + "<td style=\"display:none\">  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
                else
                    structure = structure + "<td >  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
            }
            else
                structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button\"></div> </td>";
        }
        var structure = structure + "    </tr>";
        $('#category_table tr:last').after(structure);
    }
    if (oResult.nbrPages == 0) {
        $('.arrows').hide();
    }
    for (i = 0; i <= oResult.nbrPages; i++) {
        if (i != index_start)
            $("#nbr_pages").append("<div  onClick=GetProducts_needed(" + i + "," + CategorieId + "," + furnitureID + ")>" + (i + 1) + "</div>");
        else
            $("#nbr_pages").append("<div class='selected_page'  onClick=GetProducts_needed(" + i + "," + CategorieId + "," + furnitureID + ")>" + (i + 1) + "</div>");
    }


}
//End Categories Page 


//Sub Categories Page 
function GetSubCategories_needed(CategorieId) {

    var _Params_CategorieAndFurniture = new Object();
    _Params_CategorieAndFurniture.CategorieId = CategorieId;


    var _Params = JSON.stringify(_Params_CategorieAndFurniture);

    $.ajax({
        type: "GET",
        url: url + "/GetSubCategories_needed",
        data: "Input=" + _Params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {


            GetSubCategories_needed_Completed(i_Srv_Response, CategorieId)
        },
        error: ServiceFailed

    });
}

function GetSubCategories_needed_Completed(oResult, CategorieId) {


    $('#image_loader').hide();
    $('#content').fadeIn();

    if (oResult.ListProduct.length == 0) {

        $('.header_title_blue:eq(0)').html("Sorry NO Item In This Category");
    }

    for (var i = 0 ; i < oResult.ListProduct.length; i++) {
        if (oResult.ListProduct[i].ProductImage != "none") {
            var structure = "";
            structure = structure + "<div class=\"sub_cat_modle\"  >";
            structure = structure + "<div class=\"img_div\"><a href=\"categories.aspx?id=" + CategorieId + "&fid=" + oResult.ListProduct[i].id + "\"><div  class=\"subcategories_items_image\" ><img src=\"ShowImage.ashx?autoid=" + oResult.ListProduct[i].ProductImage + "\"  ></div></a></div>";

            structure = structure + " <div class=\"text_div\"><h1 class=\"header_title_blue\" ><a href=\"categories.aspx?id=" + CategorieId + "&fid=" + oResult.ListProduct[i].id + "\"> " + oResult.ListProduct[i].ModelName + "</a></h1>";
            structure = structure + " </div>  </div>";
            $('#sub_cat_div_content').after(structure);
        }
        else {
            var structure = "";
            structure = structure + "<div class=\"sub_cat_modle\"  >";
            structure = structure + "<div class=\"img_div\"><a href=\"categories.aspx?id=" + CategorieId + "&fid=" + oResult.ListProduct[i].id + "\"><div  class=\"subcategories_items_image\" ><img src=\"img/subcat/" + oResult.ListProduct[i].id + ".jpg\"  ></div></a></div>";
            structure = structure + " <div class=\"text_div\"><h1 class=\"header_title_blue\" ><a href=\"categories.aspx?id=" + CategorieId + "&fid=" + oResult.ListProduct[i].id + "\"> " + oResult.ListProduct[i].ModelName + "</a></h1>";
            structure = structure + " </div>  </div>";
            $('#sub_cat_div_content').after(structure);
        }

    }

}
//End Sub Categories Page 



//assets Pop up window
function Getasstes_needed(index_start_page, CategorieId) {



    $('#image_loader_pop').show();
    $('.popup_table').hide();

    var _Params_PageAndCategorie = new Object();
    _Params_PageAndCategorie.IndexPage = index_start_page;
    _Params_PageAndCategorie.CategorieId = CategorieId + "";

    var _Params = JSON.stringify(_Params_PageAndCategorie);

    $.ajax({
        type: "GET",
        url: url + "/Getasstes_needed",
        data: "Input=" + _Params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {

            Getasstes_needed_Completed(i_Srv_Response, index_start_page, CategorieId);

            //-----------------------------------------------------
            //add button
            $('.add_asset_button').click(function () {

                if ($(this).hasClass('add_asset_checked')) {
                    $(this).removeClass('add_asset_checked');
                    RemoveItemFromSessionStorage($(this).attr('item_id'));
                    // var items = JSON.parse(sessionStorage.getItem('item_selected'));


                }
                else {

                    $(this).addClass('add_asset_checked');
                    AddAsset_item($(this).attr('item_id'));
                }



            });
            //-------------------------------------------------------
        },
        error: ServiceFailed

    });
}

function Getasstes_needed_Completed(oResult, index_start, CategorieId) {

    if (index_start > oResult.nbrPages || index_start < 0) {
        $('#image_loader_pop').hide();
        $('.popup_table').fadeIn();
        return;
    }


    var attr_src = $(".small_image_class:eq(0) img").attr('src');
    var desc_items = $("#ModelName_pop").attr("val");


    $('#image_loader_pop').hide();
    $('.popup_table').fadeIn();

    if (oResult.ListProduct.length == 0) {

        $('#ModelName_pop').html("Sorry NO Item In This Model");
    }


    if (oResult.nbrPages == "0") {

        for (var i = 0 ; i < oResult.ListProduct.length; i++) {

            var CondVl = oResult.ListProduct[i].ConditionCode;
            var condition_stars = "";
            if (CondVl == '1' || CondVl == '2' || CondVl == '3' || CondVl == '4') {

                if (CondVl == '1')
                    condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";

                if (CondVl == '2')
                    condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";

                if (CondVl == '3')
                    condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class\"></div></div>";

                if (CondVl == '4')
                    condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class star_on\"></div></div>";

            }
            else
                condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class\"></div> <div class=\"star_class\"></div> <div class=\"star_class\"></div><div class=\"star_class\"></div></div>";



            if (oResult.ListProduct[i].ProductImage != "none") {
                var structure = "";
                structure = structure + "<tr class=\"item_tr\">";
                structure = structure + "<td><div class=\"asset_items_image\"><img class=\"popup_image_module\" src=\"" + attr_src + "\" width=\"100\" /></div> </td>";
                structure = structure + "<td> " + desc_items + " </td>";

                structure = structure + "<td>  " + condition_stars + " </td>";

                if (CheckIfItemInSessionStorage(oResult.ListProduct[i].id))
                    structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
                else
                    structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button\"></div> </td>";
                structure = structure + "    </tr>";
                $('#asset_table tr:last').after(structure);
            }
            else {
                var structure = "";
                structure = structure + "<tr class=\"item_tr\">";
                structure = structure + "<td><div class=\"asset_items_image\"><img class=\"popup_image_module\" src=\"" + attr_src + "\" width=\"100\" /></div> </td>";
                structure = structure + "<td> " + desc_items + " </td>";

                structure = structure + "<td>  " + condition_stars + " </td>";

                if (CheckIfItemInSessionStorage(oResult.ListProduct[i].id))
                    structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
                else
                    structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button\"></div> </td>";
                structure = structure + "    </tr>";
                $('#asset_table tr:last').after(structure);
            }

        }
        $('.arrows').hide();

        return;
    }


    // if (index_start == oResult.nbrPages) return;

    $(".item_tr").remove();
    $("#nbr_pages").html('');

    for (var i = 0 ; i < oResult.ListProduct.length; i++) {

        var CondVl = oResult.ListProduct[i].ConditionCode;
        var condition_stars = "";
        if (CondVl == '1' || CondVl == '2' || CondVl == '3' || CondVl == '4') {

            if (CondVl == '1')
                condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";

            if (CondVl == '2')
                condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";

            if (CondVl == '3')
                condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class\"></div></div>";

            if (CondVl == '4')
                condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class star_on\"></div></div>";

        }
        else
            condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class\"></div> <div class=\"star_class\"></div> <div class=\"star_class\"></div><div class=\"star_class\"></div></div>";






        if (oResult.ListProduct[i].ProductImage != "none") {
            var structure = "";
            structure = structure + "<tr class=\"item_tr\">";
            structure = structure + "<td><div class=\"asset_items_image\"><img class=\"popup_image_module\" src=\"" + attr_src + "\" width=\"100\" /></div> </td>";
            structure = structure + "<td> " + desc_items + " </td>";

            structure = structure + "<td>  " + condition_stars + " </td>";


            if (CheckIfItemInSessionStorage(oResult.ListProduct[i].id))
                structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
            else
                structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button\"></div> </td>";

            structure = structure + "    </tr>";
            $('#asset_table tr:last').after(structure);
        }
        else {
            var structure = "";
            structure = structure + "<tr class=\"item_tr\">";
            structure = structure + "<td><div class=\"asset_items_image\"><img class=\"popup_image_module\" src=\"" + attr_src + "\" width=\"100\" /></div> </td>";
            structure = structure + "<td> " + desc_items + " </td>";

            structure = structure + "<td>  " + condition_stars + " </td>";

            if (CheckIfItemInSessionStorage(oResult.ListProduct[i].id))
                structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
            else
                structure = structure + "<td>  <div item_id=\"" + oResult.ListProduct[i].id + "\" class=\"add_asset_button\"></div> </td>";
            structure = structure + " </tr>";
            $('#asset_table tr:last').after(structure);
        }
    }


    var i = index_start - 5;

    var res = 0;
    while (i < oResult.nbrPages && res < 10) {
        res++;
        i++;
        if ((i < 0)) i = 0;
        if (i != index_start)
            $("#nbr_pages").append("<div  onClick=Getasstes_needed(" + i + ",'" + CategorieId + "')>" + (i + 1) + "</div>");
        else
            $("#nbr_pages").append("<div class='selected_page'  onClick=Getasstes_needed(" + i + ",'" + CategorieId + "')>" + (i + 1) + "</div>");
    }



}
//End Assets  


//get url parameters
function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
    );
}

//get one product and put it in session storage
function AddAsset_item(item_id) {

    //image loader

    $('#image_loader_update').show();
    //$('#update_button').hide();

    //get product info
    if (!CheckIfItemInSessionStorage(item_id)) {

        $.ajax({
            type: "GET",
            url: url + "/GetOneProductInfo",
            data: "Input=" + item_id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processdata: false,
            async: true,
            success: function (i_Srv_Response) {

                //AddAsset_item_Completed();


                if (sessionStorage.length == 0) {
                    var items = [];
                    items.push(i_Srv_Response);
                    sessionStorage.setItem('item_selected', JSON.stringify(items));


                }
                else {


                    var items = JSON.parse(sessionStorage.getItem('item_selected'));
                    items.push(i_Srv_Response);
                    sessionStorage.setItem('item_selected', JSON.stringify(items));
                }

                var items = JSON.parse(sessionStorage.getItem('item_selected'));
                $('#total_items_input').val(items.length);
                if (items.length == 0) {
                    $('#header_title_checkout').html("NO Item Selected");
                    $('#submit_order').attr("disabled", "disabled");
                }
                else {
                    $('#header_title_checkout').html("Checkout");
                    $('#submit_order').removeAttr("disabled");
                }


                Build_items_selected_box();//rebuild box

                //image loader
                $('#image_loader_update').hide();
                $('#update_button').show();

            },
            error: ServiceFailed

        });


    }
}


function RemoveItemFromSessionStorage(item_id) {
    if (sessionStorage.length == 0) {

    }
    else {
        var items = JSON.parse(sessionStorage.getItem('item_selected'));
        RemoveItemFromCart(item_id);
        for (var i = 0; i < items.length; i++) {

            if (items[i].id == item_id) {

                items.splice(i, 1);
                sessionStorage.setItem('item_selected', JSON.stringify(items));

            }
        }
        Build_items_selected_box();//rebuild box  

    }
}

//return true the item in session   returen false is item not in session sotrage 
function CheckIfItemInSessionStorage(item_id) {
    if (sessionStorage.length == 0) {
        return false;
    }
    else {
        var items = JSON.parse(sessionStorage.getItem('item_selected'));

        for (var i = 0; i < items.length; i++) {

            if (items[i].id == item_id)
                return true;
        }
        return false;

    }

}


//Start checkout page 

function GetWBSElement(IssueeID) {
    $.ajax({
        type: "GET",
        url: url + "/GetWBSElement",
        data: "Input=" + IssueeID,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {

            GetWBSElement_Completed(i_Srv_Response)
        },
        error: ServiceFailed

    });
}

function GetWBSElement_Completed(oResult) {

    if (oResult == null || oResult.length == 1) {
        $('#WBS_Element').attr("disabled", "disabled");
        $('#WBS_Element').css({ 'backgroundColor': 'whitesmoke' });
    }
    else {

        $('#WBS_Element').removeAttr("disabled");
        $('#WBS_Element').css("backgroundColor", "white");
    }
    var body_select = "";
    for (var i = 0 ; i < oResult.length ; i++) {
        body_select = body_select + "<option related_item=\"" + oResult[i].RelatedItem + "\" fname=\"" + oResult[i].Name + "\" value=\"" + oResult[i].stringID + "\">" + oResult[i].Name + " + " + oResult[i].stringID + "</option>";
    }
    $('#WBS_Element').html('');
    $('#WBS_Element').append(body_select);
    //$('#wbs_element_name').val($('#WBS_Element').find(':selected').attr('fname'));

    //Select drop Down
    var spacesToAdd = 3;
    var biggestLength = 0;
    $(".addsapcesWbs option").each(function () {
        var len = $(this).text().length;
        if (len > biggestLength) {
            biggestLength = len;
        }
    });
    var padLength = biggestLength + spacesToAdd;
    $(".addsapcesWbs option").each(function () {
        var parts = $(this).text().split('+');
        var strLength = parts[0].length;
        for (var x = 0; x < (padLength - strLength) ; x++) {
            parts[0] = parts[0] + ' ';
        }
        if (parts[1] || parts[1] == "") {
            $(this).text(parts[0].replace(/ /g, '\u00a0') + '' + parts[1]).text;
        }

    });

    //End Select Drop Down

}


function GetShipAddressName(IssueeID) {


    $.ajax({
        type: "GET",
        url: url + "/GetShipAddressName",
        data: "Input=" + IssueeID,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {

            GetShipAddressName_Completed(i_Srv_Response)
        },
        error: ServiceFailed

    });
}

function GetShipAddressName_Completed(oResult) {

    var body_select = "";
    for (var i = 0 ; i < oResult.length ; i++) {
        body_select = body_select + "<option fname=" + oResult[i].Name + " value=\"" + oResult[i].ID + "\"> " + oResult[i].Name + "</option>";
    }

    $('#ship_address_name').html('');
    $('#ship_address_name').append(body_select);
    $('#ship_address_name').val($('#ship_address_name').find(':selected').attr('fname'));
}


//check persons in database from Code 

function CheckRecipientEmployee(objthis, PersonCode, PersonsLastName) {

    var _Params_RecipientEmp = new Object();
    _Params_RecipientEmp.PersonCode = PersonCode;
    _Params_RecipientEmp.PersonsLastName = PersonsLastName;

    var _Params = JSON.stringify(_Params_RecipientEmp);

    $.ajax({
        type: "GET",
        url: url + "/CheckRecipientEmployee",
        data: "Input=" + _Params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {
            CheckRecipientEmployee_Completed(objthis, i_Srv_Response)
        },
        error: ServiceFailed

    });
}

function CheckRecipientEmployee_Completed(objthis, oResult) {

    if (oResult.PersonsFound == 1) {
        $(objthis).closest('tr').find('.rec_emp_feild').attr('id_persons', oResult.PersonID);
        $(objthis).closest('tr').find('.empl_not_found_text').hide();
    }
    else {
        $(objthis).closest('tr').find('.empl_not_found_text').show();
        $(objthis).closest('tr').find('.rec_emp_feild').attr("id_persons", "");
    }
    CheckIfFeildTrue(objthis);
}


//Check feilds if it's ok or not and show X if wrong 
function CheckIfFeildTrue(objthis) {
    var item_validate = true;
    var id_personss = $(objthis).closest('tr').find('.rec_emp_feild').attr('id_persons');
    var rec_emp_feild = $(objthis).closest('tr').find('.rec_emp_feild').val();
    //var r_first_name = $(objthis).closest('tr').find('.r_first_name').val();
    var r_last_name = $(objthis).closest('tr').find('.r_last_name').val();
    var deliver_to = $(objthis).closest('tr').find('.deliver_to').val();


    if (rec_emp_feild == "" || rec_emp_feild == "ID NO." || r_last_name == "" || r_last_name == "LAST NAME" || deliver_to == "" || deliver_to == "ROOM NO." || id_personss == "") {
        item_validate = false;
        $(objthis).closest('tr').find('.check_filed_false ').removeClass("check_filed_true");
    }
    else $(objthis).closest('tr').find('.check_filed_false ').addClass("check_filed_true");
}

//GetEmailUser 

function GetEmailUser() {
    $.ajax({
        type: "GET",
        url: url + "/GetEmailUser",
        data: "Input=0",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {

            GetEmailUser_Completed(i_Srv_Response)
        },
        error: ServiceFailed

    });
}

function GetEmailUser_Completed(oResult) {

    if (oResult.PersonsFound == 1) {
        $("#email").val(oResult.PersonsEmail);
        $("#confemail").val(oResult.PersonsEmail);
    }


}

//GetEmailUser 

function SendEmailOrder(email, body) {
    $('#image_loader').show();
    $('#content').hide();

    var _Params_PageAndCategorie = new Object();
    _Params_PageAndCategorie.Email = email;
    _Params_PageAndCategorie.BodyEmail = body;

    var _Params = JSON.stringify(_Params_PageAndCategorie);

    $.ajax({
        type: "GET",
        url: url + "/SendEmailOrder",
        data: "Input=" + _Params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {
            //console.log(i_Srv_Response);
            SendEmailOrder_Completed(i_Srv_Response)
        },
        error: ServiceFailed

    });
}

function SendEmailOrder_Completed(oResult) {

    $('#image_loader').hide();
    $('#content').fadeIn();

    if (oResult == "Send") {
        $('#content').html("<h1 class=\"header_title_blue\">Thank You!.  </1><br /><a href=\"default.aspx\">Back To Home Page</a>");
    }
    else {
        $('#content').html("<h1 class=\"header_title_blue\">Something Wrong Sorry!! </a>");

    }


}

//End checkout page

//InsertToDataBase
function InsertToDataBase(_Params) {

    $('#image_loader').show();
    $('#content').hide();

    $.ajax({
        type: "POST",
        url: url + "/InsertToDataBase",
        data: JSON.stringify(_Params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {

            InsertToDataBase_Completed(i_Srv_Response)
        },
        error: ServiceFailed

    });
}
//End InsertToDataBase
function InsertToDataBase_Completed(oResult) {

    $('#content').fadeIn();
    $('#image_loader').hide();
    $('.result_msg').show();

    if (oResult.ErrorString) {
        //  $('.error_ul_list').append("<li>" + oResult.ErrorString + "</li>");
        $(".popup_error_msg").show();

        for (var i = 0 ; i < oResult.ItemsIDNotAvailable.length; i++) {
            var items = JSON.parse(sessionStorage.getItem('item_selected'));
            for (var j = 0 ; j < items.length; j++) {
                console.log(oResult.ItemsIDNotAvailable[i] + ":::" + items[j].id);
                if (oResult.ItemsIDNotAvailable[i] == items[j].id) {
                    $('.error_ul_list').append("<li>" + items[j].ModelName + " " + items[j].ModelNumber + " not available</li>");
                }
            }
            RemoveItemFromSessionStorage(oResult.ItemsIDNotAvailable[i]);
            Build_checkout_table_detlais();
        }
    }

    else {
        var bodytext = "";
        bodytext = bodytext + "    <h1 class=\"header_title_blue\">Thank you!<br /> your order# is " + oResult.IssueOrderCode + ".</h1>";
        bodytext = bodytext + "   <div style=\"margin-left:55px;\">";
        bodytext = bodytext + "    <table>";
        bodytext = bodytext + "       <tr style=\"height: 30px;\">";
        bodytext = bodytext + "         <td>Email your order to :</td>";
        bodytext = bodytext + "         <td></td>";
        bodytext = bodytext + "     </tr>";
        bodytext = bodytext + "     <tr>";
        bodytext = bodytext + "         <td>Email address:</td>";
        bodytext = bodytext + "        <td><input type=\"text\" name=\"email\" id=\"email\"  style=\"width: 220px;\"  /></td>";
        bodytext = bodytext + "     </tr>";
        bodytext = bodytext + "     <tr>";
        bodytext = bodytext + "         <td>Re-enter email address:</td>";
        bodytext = bodytext + "        <td><input type=\"text\" name=\"confemail\" id=\"confemail\"  style=\"width: 220px;\" /></td>";
        bodytext = bodytext + "     </tr>";
        bodytext = bodytext + "     <tr>";
        bodytext = bodytext + "        <td></td>";
        bodytext = bodytext + "       <td><input type=\"button\" id=\"submit_mail_button\" value=\"Send\" style=\"width: 220px;\"  /></td>";
        bodytext = bodytext + "   </tr>";
        bodytext = bodytext + "   </table>";
        bodytext = bodytext + "   </div>";
        $('.result_msg').append(bodytext);
        $('#checkout_form').hide();
        GetEmailUser();

        //Email 
        var bodyemail = "The order{number} is " + oResult.IssueOrderCode + ".<br /> ";

        var items = JSON.parse(sessionStorage.getItem('item_selected'));
        bodyemail = bodyemail + "Asset{number}, Subcategory, Model <br />";
        for (var i = 0; i < items.length; i++) {

		 if (items[i].Model.length == 0) {
                items[i].Model = "";
            }
            bodyemail = bodyemail + items[i].ModelNumber + ", " + items[i].Subcategory + ", " + items[i].Model + ".<br />";

        }
        bodyemail = bodyemail + "  You requested this email from the Los Angeles Superior Court CTS Equipment Catalogue. For further information please contact LASC Contracts and Materials Management Administration (213) 830-0846.";

        $("#submit_mail_button").click(function () {
            var email = $('#email').val();
            var confemail = $('#confemail').val();
            var res = true;
            if (confemail != email || confemail == '' || (!validateEmail(email))) {
                $('#email').css("border", "1px solid red");
                $('#confemail').css("border", "1px solid red");
                res = false;
            }
            if (res) { SendEmailOrder(email, bodyemail); }


        });



        //clear Items Session
        var items = [];
        sessionStorage.setItem('item_selected', JSON.stringify(items));
        Build_items_selected_box();
    }

    //Copy Paste features
    $('.copy_button').click(function () {

        rec_emp_feild = $(this).closest('tr').find('.rec_emp_feild').val();
        auto_name = $(this).closest('tr').find('.auto_name').val();
        deliver_to = $(this).closest('tr').find('.deliver_to').val();

        $('.past_button').css("opacity", "1");
        $(this).closest('tr').find('.past_button').css("opacity", "0.4");

    });

    $('.past_button').click(function () {

        $(this).closest('tr').find('.rec_emp_feild').val(rec_emp_feild);
        $(this).closest('tr').find('.auto_name').val(auto_name);
        $(this).closest('tr').find('.deliver_to').val(deliver_to);
        $('.class_feild').trigger('change');

    });

    $('.rec_emp_feild').change(function () {
        var persons_name = $(this).find(':selected').attr("persons_name");
        $(this).closest('tr').find('.auto_name').val(persons_name);
    });

    $('.class_feild').on("keyup change", function () {
        var item_validate = true;
        rec_emp_feild = $(this).closest('tr').find('.rec_emp_feild').val();
        auto_name = $(this).closest('tr').find('.auto_name').val();
        deliver_to = $(this).closest('tr').find('.deliver_to').val();

        if (rec_emp_feild == "" || auto_name == "" || deliver_to == "") {
            item_validate = false;
            $(this).closest('tr').find('.check_filed_false ').removeClass("check_filed_true");
        }
        else $(this).closest('tr').find('.check_filed_false ').addClass("check_filed_true");
    });

    //End Copy Paste features

}


//Right Box Item Selected Function Build
function Build_items_selected_box() {


    var items = JSON.parse(sessionStorage.getItem('item_selected'));

    if (items == null) {
        $('#item_selected_box').show();

    }
    else {
        $('#item_selected_box').show();
        $('#number_of_product_selected').html(items.length);



    }
}


function ServiceFailed(xhr) {

    try {
        alert("Error");

    }
    catch (e) {
        if (e.Message != undefined) {
            alert("ServiceFailed: " + e.Message);

        }
        return false;
    }

}



//Send Form register page 
function send_form_email(_Params) {

    $('#image_loader').show();
    $('#content').hide();

    $.ajax({
        type: "GET",
        url: url + "/SendFormEmail",
        data: "Input=" + _Params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {


            send_form_email_Completed(i_Srv_Response)
        },
        error: ServiceFailed

    });
}

function EmptyServerSession() {
    $.ajax({
        type: "GET",
        url: url + "/EmptyServerSession",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {
        },
        error: ServiceFailed

    });
}

function RemoveItemFromCart(ModelID) {
    $.ajax({
        type: "GET",
        url: url + "/RemoveItemFromCart",
        data: "Input=" + ModelID,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: false,
        async: true,
        success: function (i_Srv_Response) {
        },
        error: ServiceFailed

    });
}



function send_form_email_Completed(_Params) {
    $('#content').fadeIn();
    $('#image_loader').hide();
    $('#content').html("<h1 class=\"header_title_blue\"> You&#39;ll be notified via email with your registration status.  </1><br /><a href=\"default.aspx\">Back To Login Page</a>");
}
//End Send Form register page 


//place holder
(function ($) {
    $.fn.placeholder = function () {
        if (typeof document.createElement("input").placeholder == 'undefined') {
            $('[placeholder]').focus(function () {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                    input.removeClass('placeholder');
                }
            }).blur(function () {
                var input = $(this);
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.addClass('placeholder');
                    input.val(input.attr('placeholder'));
                }
            }).blur().parents('form').submit(function () {
                $(this).find('[placeholder]').each(function () {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                })
            });
        }
    }
})(jQuery);


//validate email 
function validateEmail(elementValue) {
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

    return emailPattern.test(elementValue);
}