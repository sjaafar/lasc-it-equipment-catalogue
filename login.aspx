﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="LASC_ShoppingCart2.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="./css/styles.css" type="text/css">

    	<script>

    	    function get_browser() {
    	        var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    	        if (/trident/i.test(M[1])) {
    	            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
    	            return 'IE ' + (tem[1] || '');
    	        }
    	        if (M[1] === 'Chrome') {
    	            tem = ua.match(/\bOPR\/(\d+)/)
    	            if (tem != null) { return 'Opera ' + tem[1]; }
    	        }
    	        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    	        if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
    	        return M[0];
    	    }

    	    function get_browser_version() {
    	        var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    	        if (/trident/i.test(M[1])) {
    	            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
    	            return 'IE ' + (tem[1] || '');
    	        }
    	        if (M[1] === 'Chrome') {
    	            tem = ua.match(/\bOPR\/(\d+)/)
    	            if (tem != null) { return 'Opera ' + tem[1]; }
    	        }
    	        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    	        if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
    	        return M[1];
    	    }
    	    var errormsg = 'You are using an unsupported browser.\nThis website is best viewed using:\n - IE9+ \n - FireFox 23.0+ \n - Google Chrome 28.0+ \n - Safari 5.1.7+'
    	    var browser = get_browser();
    	    var browser_version = get_browser_version();
    	    if (browser == 'MSIE') {
    	        if (browser_version <= 8) {
    	            alert(errormsg);
    	        }
    	    }
    	    if (browser == 'Chrome') {
    	        if (browser_version <= 28) {
    	            alert(errormsg);
    	        }
    	    }
    	    if (browser == 'safari') {
    	        if (browser_version <= 5) {
    	            alert(errormsg);
    	        }
    	    }
    	    if (browser == 'firefox') {
    	        if (browser_version <= 23) {
    	            alert(errormsg);
    	        }
    	    }
	</script>

</head>
<body>
    <form id="form1" runat="server">
         <div>
        
    </div>
    </form>

    <!-- -----------------------------------------Start Header-------------------------------- -->
    <div id="header_section">

        <div id="logo_and_name">
            <div id="logo">
               <a href="default.aspx"> <img src="img/logo.png" /></a></div>
            <div>
                <h1>Los Angeles Superior Court</h1>
                <h2>IT Equipment Catalogue</h2>
            </div>

        </div>

    </div>
    <!-- -----------------------------------------End  Header-------------------------------- -->

    
    <!-- ---------------- Start Content--------- -->
     <div id="image_loader" style="display:none;"><img src="img/ajax_loader.gif" /></div>
    <div id="content" class="page_width">
         <h1 style="text-align:center;">Login</h1>


           <div id="login_div">
            <div class="error_message"></div>
            
            <table style="width: 100%;margin-left: -38px;">
                <tr>
                    <td>Username: </td>
                    <td><input type="text" class="input_style" id="username" value="" /></td>
                   
                </tr>
                <tr>
                    <td>Password: </td>
                    <td><input type="password" class="input_style" id="password"  /></td>
                   
                </tr>
                
            </table>

			 <div style="float:left;width:100px;margin:auto;margin-left: 107px;margin-top: 15px;"> <input type="button" id="submit_button" value="Login" class="login_button"/></div>             
             <div style="float:left;width:100px;margin:auto;margin-left: 107px;margin-top: 15px;"> <a href="register_page.aspx" style="text-decoration: none;" > <div class="login_button">Register</div></a></div> 
             <div style="float:left;width:100px;margin:auto;margin-left: 107px;margin-top: 15px;"> <a href="help_page.aspx" style="text-decoration: none;" ><div class="login_button">Login Help</div></a></div> 
        </div>
      
        <p class="timeoutmsg">For security reasons you’ve been logged out due to inactivity.</p>

    </div>
     <!-- ---------------- End Content--------- -->
 
     <!-- -----------------------------------------Start Footer --------------------------------- -->
    <!-- <div style="position: fixed;bottom: 60px;width:100%;">
       <div id="m1" class="marquee"><span > &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; JUDICIAL OFFICERS PREVIEW DAYS: SEPTEMBER 26 THRU OCTOBER 20 2014!!!</span></div>
      </div>  -->
    <div id="footer_content">

            <div id="footer_text" >
                <p>A Service of LASC Materials Management Administration
                    <br />
                    (213) 830-0846
                </p>

            </div>

    </div>
    <!-- -----------------------------------------End Footer --------------------------------- -->
      
    <script type='text/javascript' src='js/jquery.min.js'></script>
    <script type='text/javascript' src='js/jscript.js'></script>
    
    <script type="text/javascript">
       


        $(document).ready(function () {
            var loggingIn;
            sessionStorage.clear();

            $('#submit_button').click(function () {
                submit_form();
                loggingIn = true;
            });
 
             


            $('.input_style').on('keypress', function (e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13 ) {
                    e.preventDefault();
                     
                    submit_form();

                }
            });


            $('.input_style').focus(function () {
                $(this).css({
                    "border-color": "#dcdcdc",
                    "border-width": "1px",
                    "border-style": "solid"
                });

            });


            if (getURLParameter("t") == "out")
                $('.timeoutmsg').show();


        });

       
       
        function submit_form(){
           
                    
                var username = $('#username').val();
                var password = $('#password').val();
            
                var  res=true;

                if(username=='')
                {
                    $('#username').css({
                        "border-color": "red",
                        "border-width": "1px",
                        "border-style": "solid"
                    });
                    res=false;
                }

                if(password=='')
                {
                    $('#password').css({
                        "border-color": "red",
                        "border-width": "1px",
                        "border-style": "solid"
                    });

                    res=false;
                }

                
                if (res) {
                    send_IsAuthenticated_User(username, password);
                     
                }
        }
        

        
    </script>

</body>
</html>
