﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LASC.Master" AutoEventWireup="true" CodeFile="view_cart.aspx.cs" Inherits="LASC_ShoppingCart2.view_cart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="page_path">home</div>
    <h1 class="header_title_blue"></h1>

    <div id="item_selected_table">
        <table style="width: 100%;" id="item_selected_table" cellpadding="0" cellspacing="0">
            <tr>
                    <td width="10%">Product</td>
                    <td width="15%">Subcategory</td>
                    <td width="25%">Model</td>
                    <!--<td width="25%">Description</td>-->
                    <td width="10%">Asset #</td>
                    <td width="5%">Condition</td>
                    <td width="10%">Add To Cart</td>
            </tr>

        </table>
        <div class="ButtonsForViewCart">
            <div class="IMGButtonsForViewCart" id="UpdateButtonForViewCart">
                <img src="img/update_list_button.png" /></div>
            <div class="IMGButtonsForViewCart"><a href="checkout.aspx">
                <img src="img/check_out_button.png" /></a></div>
            <div class="IMGButtonsForViewCart"><a href="default.aspx">
                <img src="img/return_to_catalog.png" /></a></div>
        </div>
    </div>


    <!-- ---------------- End Content--------- -->

    <!-- ---------------- Button--------- -->

    <script type="text/javascript">

        $(document).ready(function () {
            $('#image_loader').hide();
            $('#content').fadeIn();
            Build_view_cart_details();
            $('.add_asset_button').click(function () {

                if ($(this).hasClass('add_asset_checked')) {
                    $(this).removeClass('add_asset_checked');
                    RemoveItemFromSessionStorage($(this).attr('item_id'));
                    // var items = JSON.parse(sessionStorage.getItem('item_selected'));
                    var items = JSON.parse(sessionStorage.getItem('item_selected'));
                    $('#total_items_input').val(items.length);
                    if (items.length == 0) {
                        $('#header_title_checkout').html("NO Item Selected");
                        $('#submit_order').attr("disabled", "disabled");
                    }
                    else {
                        $('#header_title_checkout').html("Checkout");
                        $('#submit_order').removeAttr("disabled");
                    }


                }
                else {

                    $(this).addClass('add_asset_checked');
                    AddAsset_item($(this).attr('item_id'));
                }


            });

            $('#UpdateButtonForViewCart').click(function () {
                $('#item_selected_table  tr:not(:first)').each(function () {
                    if (!$(this).find(".add_asset_button").hasClass('add_asset_checked'))
                    { $(this).remove(); }
                });

            });

            function Build_view_cart_details() {
                var items = JSON.parse(sessionStorage.getItem('item_selected'));
                console.log(items);
                for (var i = 0 ; i < items.length; i++) {
                    //Condition Start
                    var CondVl = items[i].ConditionCode;
                    var condition_stars = "";
                    if (CondVl == '1' || CondVl == '2' || CondVl == '3' || CondVl == '4') {

                        if (CondVl == '1')
                            condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";

                        if (CondVl == '2')
                            condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class \"></div><div class=\"star_class\"></div></div>";

                        if (CondVl == '3')
                            condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class\"></div></div>";

                        if (CondVl == '4')
                            condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div> <div class=\"star_class star_on\"></div><div class=\"star_class star_on\"></div></div>";
                    }
                    else
                        condition_stars = "<div class=\"rates\" style=\"width: 80px;margin: auto;\">  <div class=\"star_class\"></div> <div class=\"star_class\"></div> <div class=\"star_class\"></div><div class=\"star_class\"></div></div>";
                    //Conditions End
                    //Conditions End
                    if (items[i].ProductImage != "0") {
                        var imageForProduct = '<img src="ShowImage.ashx?autoid=' + items[i].ProductImage + '"  >';
                    } else {
                        var imageForProduct = 'Image Not Available';
                    }
                    var structure = "";
                    var structure = structure + "<tr class=\"item_tr\">";
                    var structure = structure + "<td class=\"GoToProduct\"><a href=\"product_details.aspx?id=" + items[i].id + "\"><div class='categories_items_image '>" + imageForProduct + "</div></a></td>";
                    var structure = structure + "<td class=\"GoToProduct\"> " + items[i].Subcategory + " </td>";
                    var structure = structure + "<td class=\"GoToProduct\"> " + items[i].Model + " </td>";
                    var structure = structure + " <td class=\"GoToProduct\">" + items[i].ModelNumber + "</td>";
                    var structure = structure + " <td class=\"GoToProduct\">" + condition_stars + "</td>";
                    if (CheckIfItemInSessionStorage(items[i].id))
                        structure = structure + "<td>  <div item_id=\"" + items[i].id + "\" class=\"add_asset_button add_asset_checked\"></div> </td>";
                    else
                        structure = structure + "<td>  <div item_id=\"" + items[i].id + "\" class=\"add_asset_button\"></div> </td>";
                    var structure = structure + "    </tr>";

                    $('#item_selected_table tr:last').after(structure);
                }//end for
            }
        });
    </script>
</asp:Content>
