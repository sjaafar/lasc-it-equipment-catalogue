﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LASC.Master" AutoEventWireup="true" CodeFile="faq.aspx.cs" Inherits="LASC_ShoppingCart2.faq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="header_title_blue">Frequently Asked Questions</h1>
    <div style="margin-left: 55px;">
        <p class="q_style">1: What is the purpose of having an IT Equipment Catalogue?</p>
        <p class="r_style">1: The purpose is to provide you with an easy and convenient online shopping experience with descriptions, images of available IT equipment, and simple selecting and ordering procedures.</p>

        <p class="q_style">2: Why is there so much surplus IT Equipment available?</p>
        <p class="r_style">2: Due to the closure of several courthouses throughout the Los Angeles County, there is a surplus of IT Equipment from these locations available for distribution.</p>


        <p class="q_style">3: Is all of the IT Equipment on the Website used, or are there new pieces?</p>
        <p class="r_style">3: Although the majority of the IT equipment is new, there are also used items available.</p>


        <p class="q_style">4: What is the quality of the IT equipment listed?</p>
        <p class="r_style">
            4: The IT equipment has been rated the following:
            <br />  <br />
            <strong style="margin-left: 60px;margin-right: 11px;">Excellent:</strong>  New with no nicks, gouges, or scratches.<br />
            <strong style="margin-left: 60px;">Very Good: </strong>Like new with superficial surface and blemishes.<br />
            <strong style="margin-left: 60px;margin-right: 40px;">Good: </strong>Used with minimal signs of age and wear and no major defects.<br />
            <strong style="margin-left: 60px;margin-right: 53px;">Fair:</strong>  Used with moderate signs of age and wear.
        </p>


        <p class="q_style">5: Will the IT equipment I receive be clean and safe to use?</p>
        <p class="r_style">
            5:Yes, your health and safety is very important to us. Prior to delivery from the warehouse, your IT equipment will be thoroughly prepped. It will cleaned and sanitized based on the item. 
    <br />
All IT Equipment are carefully inspected to ensure that nothing is broken, loose, or torn. Any such items will be sent to salvage and will not be placed on the IT Equipment Catalogue.

        </p>

        <p class="q_style">6: Who is responsible for the CTS Asset Sales Orders? </p>
        <p class="r_style">6: The CTS District Supervisors for your respective districts are responsible for placing IT orders and ensuring that staff has the appropriate IT Equipment to perform their duties.</p>

        <p class="q_style">7: How often may I order IT Equipment from the catalogue?</p>
        <p class="r_style">7: The IT Equipment in the catalogue is intended to replace IT items that are damaged or outdated. It is also for necessary additional IT Equipment needed for your location. Therefore, the frequency of placing an order can be determined by this guideline.  </p>

        <p class="q_style">8: How do I register to use the website?  </p>
        <p class="r_style">8: You will be notified if you have been pre-registered. If you have not been notified, please read the instructions provided on the first page of the Furniture and Resource Catalogue to register.</p>

        <p class="q_style">9: Once I place my order, how long will it take to receive my IT Equipment delivery?</p>
        <p class="r_style">9: The routine time for delivery is between 5-7 business days.  Please keep in mind, however, that at the onset of the IT Equipment Catalogue we may experience an overwhelming demand for orders and may take an additional day or two.</p>

         <p class="q_style">10: What about my existing IT Equipment? How do I make arrangements to have it removed?  </p>
        <p class="r_style">10: At the time of placing your order, please indicate if you require IT Equipment removal.  When your IT Equipment is delivered, the existing IT devices will be removed.</p>

         <p class="q_style">11: What happens to my old IT equipment? </p>
        <p class="r_style">11: Your old IT equipment is taken to the warehouse where it is evaluated and re-entered into the IT Equipment Catalogue or offered to one of the donation programs such as Teen Court where it will do much good by benefitting the community. If it is deemed unusable, it will be sent to salvage/e-waste.</p>

         <p class="q_style">12: Who do I contact if I do not receive my IT equipment or have additional questions regarding my transaction?   </p>
        <p class="r_style">12: We pride ourselves in being very customer oriented and will contact you if there is any change in your delivery date, however, in the unforeseen case that this happens, you may contact the Monrovia Warehouse at (626) 301-1400. We will promptly handle the situation.</p>

         <p class="q_style">13: I cannot see the page, who do I contact? </p>
        <p class="r_style">13: This may be a temporary computer glitch. Please contact the Business Operations and Resource Management Division in Materials Management Administration at  <a href="mailto:resources@lacourt.org">resources@lacourt.org</a>  or call (213) 633-4938 where knowledgeable staff is available to assist you and handle any crisis.</p>

         <p class="q_style">14: How do I report a bad link?  </p>
        <p class="r_style">14: Again, the knowledgeable staff in the Business Operation and Resource Management Division of Materials Management Administration is there to assist you at <a href="mailto:resources@lacourt.org">resources@lacourt.org</a> or (213) 633-4938.</p>

         <p class="q_style">15: Will I get a receipt for the IT equipment I order? </p>
        <p class="r_style">15: Yes, you will receive a digital packing slip with a detailed list of your order.</p>

         <p class="q_style">16: If I leave my ordering page open, how long will the items I am planning to order remain in my cart? </p>
        <p class="r_style">16: Your cart will remain active for 2 hours. So, if you decide to go to lunch or take a break, this avails you of some time to leave it dormant until it disappears.</p>

         <p class="q_style">17: Can I change my password?  If so, how? </p>
        <p class="r_style">17: Yes. Please contact Business Operations and Resource Management Division in Materials Management Administration at (213) 633-4938.</p>

         <p class="q_style">18: How will I know if additional IT equipment becomes available on the Surplus Catalogue? </p>
        <p class="r_style">18: The acquisition of IT Equipment from various courthouses will be an ongoing process until all of it has been removed. As IT equipment becomes available, it will be placed in the Furniture and Resource Catalogue. You may want to view the website weekly for new additions. </p>

    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#image_loader').hide();
            $('#content').fadeIn();

            $('.q_style').click(function () {
                $(this).next('.r_style').slideToggle()
            });

        });


    </script>
</asp:Content>
