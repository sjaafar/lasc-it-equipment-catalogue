﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LASC.Master" AutoEventWireup="true" CodeFile="how_to_use.aspx.cs" Inherits="LASC_ShoppingCart2.how_to_use" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2 class="header_2_title_blue" style="width: 875px;">Welcome to the Los Angeles Superior Court IT Equipment Catalogue. 
         
        The purpose of this manual is to help with the navigation of this site and assist in submitting an order for the desired items chosen.  </h2>

    <h1 class="header_title_blue" >Getting Started</h1>

    <div style="margin-left: 35px;">
        <ul  style="line-height: 26px;font-size:14px;width: 875px;">
            <li>Once logged in, the <strong><em>Home  Page</em></strong> will be in view. </li>
            <li>There are two ways to begin  the process of selection, <strong><em>Search </em></strong>and <strong><em>Category Selection</em></strong></li>
            <ul>
                <li>To use <strong><em>Search</em></strong>, enter a category, or IT product to arrive at the IT product select screen. </li>
                <li>To use <strong><em>Category Selection, </em></strong>click  on any of the available categories provided on the <strong><em>Home </em></strong>screen. This will  enable access to the subcategories in order to narrow the search preferences. </li>
                <ul>
                    <li>Clicking on the sub-category icon will provide a description and information of the selected item.</li>
                    <li>For more detail about the  item, click on the icon once again and a new screen will show additional  pictures, item condition, special features. </li>
                    <li>To select item for  ordering, click on the <strong><em>Order Form</em></strong> button and an inset will display  the desired articles of IT Assets. </li>
                    <li>Click on the <strong><em>Add</em></strong> icon and the item will be added to the <strong><em>Shopping Cart</em></strong></li>
                    <li>A check mark in the box  will confirm the item has been selected</li>
                    <li>To continue shopping,  select the <strong><em>Return to Catalogue</em></strong> button and you will be returned to the <strong><em>Home  Page</em></strong></li>
                </ul>
            </ul>
        </ul>

    </div>

    <h1 class="header_title_blue" style="margin-top: 35px;">Checkout</h1>
    <div style="margin-left: 35px;">
        <ul style="line-height: 26px;font-size:14px;width: 875px;">
            <li>There are two ways to  checkout once you are finished shopping on the site:</li>
            <ul>
                <li>On the <strong><em>Order Form</em></strong>, select <strong><em>Checkout</em></strong> to take you to the <strong><em>Checkout</em></strong> screen.  </li>
                <li>A <strong><em>Shopping cart</em></strong> icon will  also be available in the upper right corner of the screen once an item has been  selected. </li>
                <ul>
                    <li>Clicking on the icon will  also take you to the <strong><em>Checkout </em></strong>screen</li>
                </ul>
            </ul>
            <li>Once on the <strong><em>Checkout </em></strong>screen a list of the items selected will be provided as well as the  total number</li>
            <ul>
                <li>If there are any changes to  be made to the number of products chosen, please click on the check box at the  right of the item to remove/add from the <strong><em>Shopping Cart</em></strong></li>
                <li>Once this has been done,  click <strong><em>Update</em></strong> and the change(s) will be finalized</li>
            </ul>
            <li>Please fill in the recipient&rsquo;s  employee # and their room # for each item in the lines provided. </li>
            <ul>
                <li>If there are multiple  recipients, please enter each individual</li>
                <ul>
                    <li>If unknown, enter the  Contact info to take receipt of the delivered items.</li>
                </ul>
                <li>If the recipient is the  same for multiple items ordered, a <strong><em>Copy </em></strong>and <strong><em>Paste </em></strong>feature is  available.</li>
                <ul>
                    <li>Click on the <strong><em>Copy</em></strong> button for the line that is to be copied.</li>
                    <li>On the following line,  select <strong><em>Paste</em></strong> to have the previous information entered automatically.</li>
                </ul>
            </ul>
            <li>Please fill in the complete <strong><em>Contact</em></strong> info in order to provide accurate delivery and pick up instructions</li>
            <ul>
                <li>The <strong><em>Contact,</em></strong> <strong><em>Phone  number</em></strong>, and <strong><em>Room Number </em></strong>will be needed for point of delivery custodial  responsibilities </li>
                <li><strong><em>Cost Center </em></strong>and <strong><em>Functional  Area</em></strong> will be needed for Asset report accuracy.</li>
                <li><strong><em>Ship To Address</em></strong> will be  the location of the court receiving the items ordered.</li>
                <li><strong><em>Special Instructions </em></strong>are  to be provided in case of any special period, special tools, or obstacles that  will need to be addressed at the time of delivery.</li>
                <li>If there are any articles  of furniture that need to be removed at the time of delivery, please check the <strong><em>Yes </em></strong>box  provided. </li>
                <ul>
                    <li>In the text box provided,  please list the items to be removed and any Asset barcode tags that are  attached.</li>
                </ul>
            </ul>
            <li>After completion of all  info requested, click the <strong><em>Submit Order</em></strong> button to finalize the  order.</li>
            <ul>
                <li>An order number will be  provided as reference </li>
            </ul>
            <li>If an <strong><em>&ldquo;Error While Processing Your  Order&rdquo;</em></strong> message is received after clicking the <strong><em>Submit Order,</em></strong> then an  item that is currently in the <strong><em>Shopping Cart</em></strong> has been ordered  before you were able to finalize your request.</li>
            <ul>
                <li>An item description will be  located in the message box to inform you of the specific selection that is  unavailable.</li>
                <li>Press the <strong><em>OK</em></strong> button and you will be returned to the <strong><em>Checkout</em></strong> screen.  </li>
                <li>The unavailable IT item will automatically be omitted from your order.</li>
                <li>You may once again click  the <strong><em>Submit  Order</em></strong> button to submit your request.</li>
            </ul>
        </ul>

    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#image_loader').hide();
            $('#content').fadeIn();


        });


    </script>
</asp:Content>
