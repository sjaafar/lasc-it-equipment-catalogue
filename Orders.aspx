﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LASC.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="LASC_CTS_EquipmentCatalogue.Orders" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   

     <script type="text/javascript">
         
         var CustomLogin = '<%= Session["UserLogOn"].ToString()%>';
         var Orderid = '<%= Convert.ToInt32(Request.QueryString["orderid"])%>'

    </script>

    <div>
        <table style="width: 100%;" id="item_selected_table" cellpadding="0" cellspacing="0">
            <tr>
                    <td width="10%">Order Number</td>
                    <td width="15%">Asset #</td>
                    <td width="25%">Subcategory</td>
                    <!--<td width="25%">Description</td>-->
                    <%--<td width="10%">Model photo #</td>--%>
                    <td width="5%">Price</td>
            </tr>

        </table>
        </div>

         <div  class="arrows" style="margin:auto;clear:both;height: 91px;padding-left: 45px;padding-top: 28px;">
        <table style="margin: 30px auto 30px;">
            <tr>
                <td> <div class="prev_button" style="cursor:pointer;float:left;"><img src="img/flesh_left.png" style="width: 70px;" /></div></td>
                <td><div id="nbr_pages"></div></td>
                <td> <div class="next_button" style="cursor:pointer;float:right;"><img src="img/flesh_right.png"  style="width: 70px;" /></div></td>
            </tr>
             
             
        </table>
    </div>

        
    <script type="text/javascript">
        $(document).ready(function () {
            GeTorder_needed_(0, CustomLogin, Orderid);// get the first page
            $('.next_button').click(function () {
                index_page = parseInt($('.selected_page').html());
                GeTorder_needed_(index_page, CustomLogin, Orderid);//Next Page 


            });

            $('.prev_button').click(function () {
                index_page = parseInt($('.selected_page').html());
                if (index_page <= 1) return;
                index_page = parseInt($('.selected_page').html()) - 2;
                GeTorder_needed_(index_page, CustomLogin,Orderid);//Next Page 
            });
            //End HOme PAge Code

        });
        </script>

</asp:Content>
