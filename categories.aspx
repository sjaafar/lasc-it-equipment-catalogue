﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/LASC.Master" CodeFile="categories.aspx.cs" Inherits="LASC_ShoppingCart2.categories" %>
 


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        var CustomLogin = '<%= Session["CustomLogin"] %>';

    </script>
          <div id="page_path">home</div>
        <h1 class="header_title_blue"></h1>

        <div id="product_content"  >
            <table style="width: 100%;" id="category_table"   cellpadding="0" cellspacing="0">
                <tr>
                    <td width="10%">Product</td>
                    <td width="15%">Subcategory</td>
                    <td width="25%">Model</td>
                    <!--<td width="25%">Description</td>-->
                    <td width="10%">Asset #</td>
                    <td width="5%">Condition</td>
                    <td width="10%" valign="middle" align="center" style="display:<%= Convert.ToBoolean(Session["CustomLogin"]) == true ? "none" : "" %>">Add To Cart</td>
                </tr>
                 
            </table>
      </div>
      
    
     <!-- ---------------- End Content--------- -->

     <!-- ---------------- Button--------- -->
    <div  class="arrows" style="margin:auto;clear:both;height: 91px;padding-left: 45px;padding-top: 28px;">
        <table style="margin: 30px auto 30px;">
            <tr>
                <td> <div class="prev_button" style="cursor:pointer;float:left;"><img src="img/flesh_left.png" style="width: 70px;" /></div></td>
                <td><div id="nbr_pages"></div></td>
                <td> <div class="next_button" style="cursor:pointer;float:right;"><img src="img/flesh_right.png"  style="width: 70px;" /></div></td>
            </tr>
             
             
        </table>
      
        
       
             
    </div>
     <!-- ---------------- End Button--------- -->
    
      <script type="text/javascript">

          $(document).ready(function () {
              //Home pAge Code
              categorieId = getURLParameter('id');
              furnitureID = getURLParameter('fid');
              

              GetProducts_needed(0, categorieId, furnitureID);//First Page 
              var index_page = 0;
              $('.next_button').click(function () {
                  index_page = parseInt($('.selected_page').html());
                  GetProducts_needed(index_page, categorieId, furnitureID);//Next Page 

              });

              $('.prev_button').click(function () {
                  index_page = parseInt($('.selected_page').html());
                  if (index_page <= 1) return;
                  index_page = parseInt($('.selected_page').html()) - 2;
                 
                  GetProducts_needed(index_page, categorieId, furnitureID);//Next Page 
              });
              //End HOme PAge Code


              //Make tr clickable
              $('#category_table').delegate('.GoToProduct', 'click', function () {
                  window.location.href = $(this).parent().find('a').attr('href');
                   
              });

              var categorie_name = $('.subMenu .selected_cat').html();
              var categorie_href = $('.subMenu .selected_cat').attr("href");
              var furniture_type_name = $('.sub_sub_menu .selected_cat').html();
              $('#page_path').html("<a href=\"default.aspx\">Home</a> &#8674; <a href='" + categorie_href +"'>"+ categorie_name + "</a>&#8674; " + furniture_type_name);
              
          });



  </script>




</asp:Content>
