﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/LASC.Master" CodeFile="Default.aspx.cs" Inherits="LASC_ShoppingCart2.Default" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="page_path" ></div>
    <h1 class="header_title_blue"></h1>

    <div id="product_content">
        <!-- ---------------- product desc---------  
        <div id="product_div">
            <div class="product_div">
                <div>Image path</div>
                <h3>Title</h3>
            </div>
        </div>
         ---------------- product desc--------- -->

        <asp:Repeater ID="rpcategories" runat="server">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                <div id="product_div">
                    <div class="product_div">
                        <a href="subcategories.aspx?id=<%# UppercaseFirst(Eval("CategoryID").ToString()) %>">
                            <div>
                                <img src="img/<%# UppercaseFirst(Eval("CategoryID").ToString()) %>.jpg" />
                            </div>
                            <h3><%# UppercaseFirst(Eval("CategoryCode").ToString()) %> </h3>
                        </a>
                    </div>
                </div>
            </ItemTemplate>
            <FooterTemplate>
            </FooterTemplate>
        </asp:Repeater>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#image_loader').hide();
            $('#content').fadeIn();
           
            //This is just for now 
            $('.product_div a[href^="subcategories.aspx?id=49"]  h3').html("Storage & Filing");
         	$('.product_div a[href^="subcategories.aspx?id=62"]  h3').html("Fixed Furniture");
            $('.product_div a[href^="subcategories.aspx?id=65"]  h3').html("Systems Furniture");
        });
       
    </script>
    <!-- ---------------- End Content--------- -->





</asp:Content>
