﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LASC.Master" AutoEventWireup="true" CodeFile="contact_us.aspx.cs" Inherits="LASC_ShoppingCart2.contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div style="margin-left: 55px;float:left;width:325px;">
       <img src="img/logo_left.jpg" />
    </div>
    <div style="float:left;width:630px;"> 

        <h1 class="header_title_blue" style="margin-left: 0;margin-bottom: 0;font-size: 23px;">LOS ANGELES SUPERIOR COURT</h1>
        <h1 style="color: rgb(120,120,120);font-size: 54px;font-weight:bold;margin: 0;">CONTRACTS</h1>
        <h1 style="color: rgb(120,120,120);font-size: 36px;margin: 0;font-weight: 500;">and</h1>
        <h1 style="color: rgb(120,120,120);font-size: 54px;font-weight:bold;margin: 0;">MATERIALS</h1>
        <h1 style="color: rgb(120,120,120);font-size: 54px;font-weight:bold;margin: 0;">MANAGEMENT</h1>
        <h1 style="color: rgb(120,120,120);font-size: 54px;font-weight:bold;margin: 0;">ADMINISTRATION</h1>
        <h1 class="header_title_blue" style="margin-left: 0;">Contact:</h1>
        <p style="margin: 0;">Address:</p>
        <p style="margin: 0;">Stanley Mosk Courthouse</p>
        <p style="margin: 0;">111 N. Hill St. Rm. 526</p>
        <p style="margin: 0;">Los Angeles, CA 90012</p>

        <p>Phone: (213) 830-0846</p>

        <p>Email: <a href="mailto:resources@lacourt.org">resources@lacourt.org</a></p>

        <p style="text-decoration: underline;">Customer Service Hours of Operation: </p>

        <p style="margin: 0;">Monday – Friday</p>
        <p style="margin: 0;">8am – 5pm </p>
        </div>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#image_loader').hide();
            $('#content').fadeIn();
        });
    </script>
</asp:Content>
