﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LASC.Master" AutoEventWireup="true" CodeFile="policy.aspx.cs" Inherits="LASC_ShoppingCart2.policy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="header_title_blue">PURPOSE</h1>
    <p style="margin-left:55px;">Establish guidelines for the use of the Los Angeles Superior Court’s (LASC) IT Equipment Catalogue. This policy applies to all LASC personnel. <br /><br />  </p>
    

    <h1 class="header_title_blue">POLICY</h1>
    <p style="margin-left:55px;">The LASC IT Equipment Catalogue is designed to allow CTS District Supervisors the ability to order IT equipment and peripherals-related items that have been designated as surplus, such as computers, monitors, printers, and other miscellaneous items.
         <br /><br /> 
         LASC has created the IT Equipment Catalogue as a useful resource for CTS Court personnel. The Court retains the ownership rights to the Catalogue and reserves the right to monitor, restrict, or discontinue access if used inappropriately.
         <br /><br />
         The Court limits access to personal information about the IT Equipment Catalogue’s users to those LASC employees for whom the Court believes such access to be appropriate in light of the functions and responsibilities of their duties.
        <br /><br />
    </p>


     <h1 class="header_title_blue">CONFIDENTIALITY AND DATA SECURITY</h1>
    <p style="margin-left:55px;">
        To access the IT Equipment Catalogue, users are required to register and create a user account. Users must provide information such as a User ID, Password, and Name. After the account is created, the user information will be utilized only for verification purposes when accessing the Catalogue. 
        <br /><br />    
        The Court has physical, electronic and procedural measures to protect your personal information from unauthorized access. It is crucial that as a user, safeguards must be taken to protect against unauthorized access to password/account, such as logging off before exiting the Catalogue.
         <br /><br />
    </p>
    

      
     <h1 class="header_title_blue">CONTACT INFORMATION</h1>
    <p style="margin-left:55px;">

       If you have any questions regarding this policy, you may contact:  <br /><br />  
Business Operations/Resource Management <br />  
 
111 N. Hill St., Room 526 <br />
Los Angeles, CA 90012<br />   <br />  

(213) 633-4938 or <a href="mailto:resources@lacourt.org">resources@lacourt.org</a>
         <br /><br />  

        <p style="font-size:10px;margin-left:55px;">Revised: January 21, 2016</p>


    </p>
    
    
       <script type="text/javascript">
            $(document).ready(function () {
                $('#image_loader').hide();
                $('#content').fadeIn();


            });


    </script>
</asp:Content>
