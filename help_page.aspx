﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="help_page.aspx.cs" Inherits="LASC_ShoppingCart2.help_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="./css/styles.css" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
    </form>

    <!-- -----------------------------------------Start Header-------------------------------- -->
    <div id="header_section">

        <div id="logo_and_name">
            <div id="logo">
                <a href="default.aspx">
                    <img src="img/logo.png" /></a>
            </div>
            <div>
                <h1>Los Angeles Superior Court</h1>
                <h2>IT Equipment Catalogue</h2>
            </div>

        </div>

    </div>
    <!-- -----------------------------------------End  Header-------------------------------- -->


    <!-- ---------------- Start Content--------- -->
    <div id="image_loader" style="display: none;">
        <img src="img/ajax_loader.gif" /></div>
    <div id="content" class="page_width">
        <h1 class="header_title_blue">Help</h1>

        <div style="margin-left: 55px;">
            <p>
                To Login please refer to the email sent to you with registration information attached.<br />
                In the event you experience any problems with the Los Angeles County Court website contact:
            </p>
            <p>
                LASC Materials Management Administration
                <br />
                (213) 830-0846<br />
                <a href="mailto:resources@lacourt.org">resources@lacourt.org</a>
            </p>
            <a href="Login.aspx">Go back to login page</a>

        </div>

    </div>
    <!-- ---------------- End Content--------- -->

    <!-- -----------------------------------------Start Footer --------------------------------- -->
    <div id="footer_content">

        <div id="footer_text">
            <p>
                A Service of LASC Materials Management Administration
                    <br />
                (213) 830-0846
            </p>

        </div>

    </div>
    <!-- -----------------------------------------End Footer --------------------------------- -->

    <script type='text/javascript' src='js/jquery.min.js'></script>
    <script type='text/javascript' src='js/jscript.js'></script>


</body>
</html>
